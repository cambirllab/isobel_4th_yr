## README

Code and data for the autonomous dishwasher loading robot developed as part of Isobel Voysey's Master's thesis (AY 2019/2020).

ur5_controller contains the protocol for communicating with the UR5 arm (setup explained in the README) and experiments run from the commands.py file.
To replicate the end-to-end setup including object detection, darknet (LINK) must be installed within the ur5_controller/Generic_ur5_controller folder.

Data includes images of real-world examples of trays collected from a canteen and corresponding ground truth labels in YOLO format (in canteen_experiments) and images of the stages for each test in the lab experiments (in lab_experiments).