# import json
#
# data = []
# with open('detectfile.txt') as f:
#     for line in f:
#         data.append(json.loads(line))
#
# for obj in data:
#     print(obj['type'], obj['prob'], obj['box']['x'])
#
# if data[-1]['type'] == 'cup':
#     print('hooray!')
import numpy as np

import yolofuncs as yf
import os

print(np.deg2rad([-47, -105, -94, 200, -314, 43]))


def get_objects_in_img(img_name, img_path, txt_path, tilt=5, darknet_path="./darknet/build/darknet/x64/"):
    os.chdir(darknet_path)
    print('Changed dir to: ', os.getcwd())
    # print(run_yolo('testimg', 'my_imgs/testimg.jpg'))
    objects = yf.get_lr_tilt(img_name, img_path, txt_path, tilt=tilt)
    os.chdir("../../../..")
    print('Returned to: ', os.getcwd())
    return objects

img_name = 'test1'
img_path = 'my_imgs/' + img_name + '.jpg'
txt_path = 'my_imgs/' + img_name + '.txt'

objects = get_objects_in_img(img_name, img_path, txt_path)
# objects = [{'type': 'fork', 'prob': 97.861984, 'box': {'x': 0.339039276972597, 'y': 0.5687020835382426, 'w': 0.083105, 'h': 0.449587, 'tilt': 'l'}}, {'type': 'cup', 'prob': 42.222736, 'box': {'x': 0.6629098055397011, 'y': 0.2991742723727442, 'w': 0.234383, 'h': 0.257614, 'tilt': 'l'}}, {'type': 'spoon', 'prob': 38.692932, 'box': {'x': 0.6602931855112948, 'y': 0.2863505276144085, 'w': 0.249012, 'h': 0.245682, 'tilt': 'u'}}, {'type': 'fork', 'prob': 37.90506, 'box': {'x': 0.37138328757581107, 'y': 0.5428969208358069, 'w': 0.069439, 'h': 0.483598, 'tilt': 'l'}}, {'type': 'plate', 'prob': 70.0, 'box': {'x': 0.375, 'y': 0.5666666666666667, 'w': 0.5, 'h': 0.6666666666666666, 'tilt': 'u'}}]
# objects = [{'type': 'spoon', 'prob': 97.138092, 'box': {'x': 0.46716006414790967, 'y': 0.652658099157007, 'w': 0.26438, 'h': 0.328484, 'tilt': 'u'}}, {'type': 'bowl', 'prob': 64.255684, 'box': {'x': 0.2118285078441216, 'y': 0.34682693066451104, 'w': 0.179887, 'h': 0.305732, 'tilt': 'u'}}, {'type': 'plate', 'prob': 70.0, 'box': {'x': 0.484375, 'y': 0.5375, 'w': 0.4625, 'h': 0.6166666666666667, 'tilt': 'u'}}]

# plates = yf.get_plates('testimg', 'testimg.jpg')
# print(plates)
picking = yf.get_pick_order(objects)

path_down = "./darknet/build/darknet/x64/" + img_path

yf.draw_cv_rect(path_down, objects, pick_order=picking)
#print(yf.get_lr_tilt('testimg', 'testimg.jpg', 'testimg.txt'))