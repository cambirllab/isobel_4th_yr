import os
import yolofuncs as yf
import numpy as np
import cv2 as cv

my_darknet_path = "./darknet/build/darknet/x64/"


def get_objects_in_img(img_name, img_path, txt_path, tilt=5, darknet_path="./darknet/build/darknet/x64/"):
    os.chdir(darknet_path)
    print('Changed dir to: ', os.getcwd())
    # print(run_yolo('testimg', 'my_imgs/testimg.jpg'))
    objects = yf.get_lr_tilt(img_name, img_path, txt_path, tilt=tilt)
    os.chdir("../../../..")
    print('Returned to: ', os.getcwd())
    return objects


# objects_w_tilt = get_objects_in_img('testimg', 'my_imgs/testimg.jpg', 'my_imgs/testimg.txt', tilt=2)
# print(objects_w_tilt)

my_objs = yf.get_objs_from_txt('./overlaptest.txt')
img = cv.imread('./overlaptest.jpg')

print(my_objs)


overlaps = yf.get_overlap_matrix(my_objs)

n = np.shape(overlaps)[0]

high_thresh = 0.8
low_thresh = 0.1


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image




relations = np.zeros(shape=(n, n))

for i in range(n):
    for j in range(n):
        fract = overlaps[i,j]
        if i != j:
            if fract >= high_thresh:
                print(my_objs[i]['type'], 'is on top of', my_objs[j]['type'])
                relations[i, j] = 1
            elif fract > low_thresh and fract < high_thresh:
                print(my_objs[i]['type'], 'is underneath', my_objs[j]['type'])
                relations[i, j] = -1
            elif fract >= 0:
                print(my_objs[i]['type'], 'and', my_objs[j]['type'], 'do not overlap')
                relations[i, j] = 0
    print('--')
    draw_cv_rect(img, my_objs[i], isGround=False)


print(relations)

picking = [0]

for i in range(1, n):
    k = 0
    while k < len(picking):
        j = picking[k]
        # print(i, j, k)

        rel = relations[i, j]
        # print(rel)

        if rel == 1:
            picking.insert(k, i)
            print('on top of!')
            break
        k += 1
        if k == len(picking):
            picking.append(i)
            print('end of list')
            break
    print(picking)

order = [my_objs[n]['type'] for n in picking]
print('Pick in the following order: ', order)

cv.imshow('Image', img)
cv.waitKey(0)
cv.destroyAllWindows()


