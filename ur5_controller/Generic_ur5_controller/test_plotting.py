import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
from tqdm import tqdm

import matplotlib.pyplot as plt

width = 0.3
labels = ['fork', 'spoon', 'knife', 'cup', 'plate']
x = np.arange(len(labels))

totals = [27, 25, 10, 25, 24]
not_id1 = [1, 2, 5, 4, 0]
not_id = [100*a/b for a, b in zip(not_id1, totals)]
# id_not_pick = [9, 4, 2, 1, 6]
id_not_pick1 = [3, 1, 1, 3, 1]
id_not_pick = [100*a/b for a, b in zip(id_not_pick1, totals)]
id_and_pick1 = [23, 22, 4, 18, 23]
id_and_pick = [100*a/b for a, b in zip(id_and_pick1, totals)]
all_id = [sum(i) for i in zip(id_not_pick, id_and_pick)]
# all_id = [100*a/b for a, b in zip(all_id1, totals)]
print(all_id)
print(not_id)
print(id_not_pick, id_and_pick)

fig1, ax3 = plt.subplots(figsize=(4,3))

p3 = ax3.bar(x, not_id, width*2, bottom=all_id, capsize=4, label='Not identified', color=[0.6350, 0.0780, 0.1840])
p2 = ax3.bar(x, id_not_pick, width*2, bottom=id_and_pick, capsize=4, label='Identified but not picked successfully', color=[0, 0.4470, 0.7410])
p1 = ax3.bar(x, id_and_pick, width*2, capsize=4, label='Identified and picked successfully', color=[0.4660, 0.6740, 0.1880])


ax3.set_ylim([0,100])
ax3.set_xticks(x)
ax3.set_xticklabels(labels)
ax3.set_ylabel('Percentage of tests')
ax3.legend()
ax3.legend(fontsize=8, loc='lower center', bbox_to_anchor=(0.5, 1))
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
#ax2.set_title('Precision and Recall', fontsize=10)
fig1.tight_layout()
# plt.show()

# # Experiment 1
# width = 0.3
# labels = ['fork', 'spoon', 'knife', 'cup', 'plate']
# x = np.arange(len(labels))
#
# totals = [25, 24, 8, 25, 27]
# not_id1 = [0, 0, 3, 5, 2]
# not_id = [100*a/b for a, b in zip(not_id1, totals)]
# id_not_pick1 = [9, 8, 2, 3, 13]
# id_not_pick = [100*a/b for a, b in zip(id_not_pick1, totals)]
# id_and_pick1 = [16, 16, 3, 17, 12]
# id_and_pick = [100*a/b for a, b in zip(id_and_pick1, totals)]
# all_id = [sum(i) for i in zip(id_not_pick, id_and_pick)]
# # all_id = [100*a/b for a, b in zip(all_id1, totals)]
# print(all_id)
# print(not_id)
# print(id_not_pick, id_and_pick)
#
# fig1, ax3 = plt.subplots(figsize=(4,3))
#
# p3 = ax3.bar(x, not_id, width*2, bottom=all_id, capsize=4, label='Not identified', color=[0.6350, 0.0780, 0.1840])
# p2 = ax3.bar(x, id_not_pick, width*2, bottom=id_and_pick, capsize=4, label='Identified but not picked successfully', color=[0, 0.4470, 0.7410])
# p1 = ax3.bar(x, id_and_pick, width*2, capsize=4, label='Identified and picked successfully', color=[0.4660, 0.6740, 0.1880])
#
#
# ax3.set_ylim([0,100])
# ax3.set_xticks(x)
# ax3.set_xticklabels(labels)
# ax3.set_ylabel('Percentage of tests')
# ax3.legend()
# ax3.legend(fontsize=8, loc='lower center', bbox_to_anchor=(0.5, 1))
# ax3.spines['right'].set_visible(False)
# ax3.spines['top'].set_visible(False)
# #ax2.set_title('Precision and Recall', fontsize=10)
# fig1.tight_layout()
# plt.show()

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder


# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
n_across = 5

exp1_names = ['2020-03-09-1041.jpg', '2020-03-09-1046.jpg', '2020-03-09-1051.jpg', '2020-03-09-1056.jpg', '2020-03-09-1102.jpg', '2020-03-09-1107.jpg', '2020-03-09-1120.jpg', '2020-03-09-1126.jpg', '2020-03-09-1130.jpg', '2020-03-09-1135.jpg', '2020-03-09-1201.jpg', '2020-03-09-1210.jpg', '2020-03-09-1215.jpg', '2020-03-09-1218.jpg', '2020-03-09-1222.jpg', '2020-03-11-1055.jpg', '2020-03-11-1059.jpg', '2020-03-11-1105.jpg', '2020-03-11-1112.jpg', '2020-03-11-1118.jpg', '2020-03-11-1124.jpg', '2020-03-11-1137.jpg', '2020-03-11-1145.jpg', '2020-03-11-1149.jpg', '2020-03-11-1153.jpg']

y_m = '2020-03-'
start_day_time = ['09-1239', '09-1247', '09-1253', '09-1258', '11-1203', '11-1212', '11-1220', '11-1228', '11-1234',
                  '11-1241', '11-1740', '11-1747', '11-1757', '11-1804', '11-1811', '11-1822', '11-1828', '11-1834',
                  '11-1840', '11-1850', '11-1857', '11-1904', '11-1913', '11-1918', '11-1927']
exp2_times = [y_m + day_time for day_time in start_day_time]

# exp1_final_names = []
# os.chdir(darknet_path + 'test_imgs/openloop/final/')
# for image in glob.glob('*.jpg'):
#     exp1_final_names.append(image)
# print(exp1_final_names)
# input()
addendum = ['-a', '-b', '-c', '-d', '-e', '-f', '-g', '-h', '-i', '-j', '-k', '-l']
# exp2_end_names = []
# for i in range(len(exp2_times)):
#     final_name = ''
#     for j in range(len(addendum)):
#         addend = addendum[j]
#         name = exp2_times[i] + addend
#         filename = name + '.jpg'
#         # try to open that file
#         if os.path.isfile(darknet_path + 'test_imgs/closedloop/yolo/' + filename):
#             final_name = name
#         else:
#             break
#     exp2_end_names.append(final_name)
#
# print(exp2_end_names)
# input('heeya')
#
# for i in tqdm(range(len(exp2_end_names))):
#     filename = exp2_end_names[i] + '.jpg'
#
#     image_path = darknet_path + 'test_imgs/closedloop/yolo/' + filename
#     print(filename)
#     img = cv.imread(image_path)
#     new_w = int(640 / 2)
#     new_h = int(480 / 2)
#     resized = cv.resize(img, (new_w, new_h))
#     og_h = 1080
#     og_w = 1920
#
#     if i % n_across == 0:  # finished with row, start new one
#         if k > 0:
#             rows.append(cur_row)
#
#         cur_row = resized
#         k += 1
#     else:  # continue stacking images to current row
#         cur_img = resized
#         cur_row = np.hstack([cur_row, cur_img])
#
# rows.append(cur_row)
# # n_filler = n_across - (len(exp1_names) % n_across)
# # print(n_filler)
# #
# # if n_filler > 0:
# #     fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
# #     fill_img.fill(255)
# #     cur_row = np.hstack([cur_row, fill_img])
# #     rows.append(cur_row)
#
# # vertically stacking rows to create final collage.
# collage = rows[0]
#
# for k in range(1, len(rows)):
#     collage = np.vstack([collage, rows[k]])
#
# print(collage.shape)
# # collage = cv.resize(collage, (2160, 1080))
# cv.imwrite(darknet_path + 'test_imgs/closedloop/' + 'exp2endcollage.jpg', collage)
# cv.imshow('collage', collage)
# cv.waitKey(0)
# cv.destroyAllWindows()
# input()



for i in tqdm(range(6)):
    filename = '2020-03-11-1804' + addendum[i] + '.jpg'

    image_path = darknet_path + 'test_imgs/closedloop/yolo/' + filename
    print(filename)
    img = cv.imread(image_path)
    new_w = int(640 / 2)
    new_h = int(480 / 2)
    resized = cv.resize(img, (new_w, new_h))
    # draw lines on here
    # vertical
    cv.line(resized, (int((1.0/3.0)*new_w), 0), (int((1.0/3.0)*new_w), new_h), (255, 255, 255), thickness=1)
    cv.line(resized, (int((2.0/3.0)*new_w), 0), (int((2.0/3.0)*new_w), new_h), (255, 255, 255), thickness=1)
    cv.line(resized, (0, int((1.0/3.0)*new_h)), (new_w, int((1.0/3.0)*new_h)), (255, 255, 255), thickness=1)
    cv.line(resized, (0, int((2.0/3.0)*new_h)), (new_w, int((2.0/3.0)*new_h)), (255, 255, 255), thickness=1)

    og_h = 1080
    og_w = 1920

    if i % 7 == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

rows.append(cur_row)
# n_filler = n_across - (len(exp1_names) % n_across)
# print(n_filler)
#
# if n_filler > 0:
#     fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
#     fill_img.fill(255)
#     cur_row = np.hstack([cur_row, fill_img])
#     rows.append(cur_row)

# vertically stacking rows to create final collage.
collage = rows[0]

# for k in range(1, len(rows)):
#     collage = np.vstack([collage, rows[k]])

print(collage.shape)
# collage = cv.resize(collage, (2160, 1080))
cv.imwrite(darknet_path + 'test_imgs/closedloop/' + 'exp2-1804-collage-grid.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()
input()


for i in tqdm(range(len(exp2_times))):
    filename = exp2_times[i] + '-a.jpg'

    image_path = darknet_path + 'test_imgs/closedloop/yolo/' + filename
    print(filename)
    img = cv.imread(image_path)
    new_w = int(640 / 2)
    new_h = int(480 / 2)
    resized = cv.resize(img, (new_w, new_h))
    og_h = 1080
    og_w = 1920

    if i % n_across == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

rows.append(cur_row)
# n_filler = n_across - (len(exp1_names) % n_across)
# print(n_filler)
#
# if n_filler > 0:
#     fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
#     fill_img.fill(255)
#     cur_row = np.hstack([cur_row, fill_img])
#     rows.append(cur_row)

# vertically stacking rows to create final collage.
collage = rows[0]

for k in range(1, len(rows)):
    collage = np.vstack([collage, rows[k]])

print(collage.shape)
# collage = cv.resize(collage, (2160, 1080))
cv.imwrite(darknet_path + 'test_imgs/closedloop/' + 'exp2startcollage.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()
input()

# for i in tqdm(range(len(exp1_names))):
#     filename = exp1_names[i][:-4] + '-final.jpg'
#
#     image_path = darknet_path + 'test_imgs/openloop/final/' + filename
#
#     img = cv.imread(image_path)
#     new_w = int(640 / 2)
#     new_h = int(480 / 2)
#     resized = cv.resize(img, (new_w, new_h))
#     og_h = 1080
#     og_w = 1920
#
#     if i % n_across == 0:  # finished with row, start new one
#         if k > 0:
#             rows.append(cur_row)
#
#         cur_row = resized
#         k += 1
#     else:  # continue stacking images to current row
#         cur_img = resized
#         cur_row = np.hstack([cur_row, cur_img])
#
# rows.append(cur_row)
# # n_filler = n_across - (len(exp1_names) % n_across)
# # print(n_filler)
# #
# # if n_filler > 0:
# #     fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
# #     fill_img.fill(255)
# #     cur_row = np.hstack([cur_row, fill_img])
# #     rows.append(cur_row)
#
# # vertically stacking rows to create final collage.
# collage = rows[0]
#
# for k in range(1, len(rows)):
#     collage = np.vstack([collage, rows[k]])
#
# print(collage.shape)
# # collage = cv.resize(collage, (2160, 1080))
# cv.imwrite(darknet_path + 'test_imgs/openloop/' + 'exp1endcollage.jpg', collage)
# cv.imshow('collage', collage)
# cv.waitKey(0)
# cv.destroyAllWindows()
# input()


