import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []


# for image in glob.glob("*.jpg"):
#     # image gives me name
#     print(image)
#     if image[-5] == 'c':
#         img_list.append(image)
#         img = cv.imread(image)
#         cv.imwrite('../gtruth/' + image, img)


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def get_experiment_plates(image_path, show_img=True):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # for small plastic
    p1 = 80
    p2 = 50
    min_r = 120
    max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    # print(plate_objects)
    return plate_objects


def get_objects_from_2_txts(image_txt_path, tilted_txt_path, tilt=5, show_imgs=True):
    rad_tilt = np.deg2rad(tilt)

    objects = yf.get_objs_from_txt(image_txt_path)
    tilted_objects = yf.get_objs_from_txt(tilted_txt_path)

    # print('obj:', objects)
    # print('tbj:', tilted_objects)

    # TODO: Comparison of objects and tilted objects
    # need to search in region

    # will do transformation of object centroids as if rotated, then search for nearest match
    rotated_objects = []
    for obj in objects:
        rotate_obj = obj
        curr_x = obj['box']['x'] - 0.5
        curr_y = 0.5 - obj['box']['y']

        rot_x = (np.cos(rad_tilt) * curr_x - np.sin(rad_tilt) * curr_y) + 0.5
        rot_y = 0.5 - (np.sin(rad_tilt) * curr_x + np.cos(rad_tilt) * curr_y)

        rotate_obj['box']['x'] = rot_x
        rotate_obj['box']['y'] = rot_y

        rotated_objects.append(rotate_obj)

    # print('rbj:', rotated_objects)

    # comparison...
    def centroid_sep(obj1, obj2):
        x1 = obj1['box']['x']
        x2 = obj2['box']['x']
        y1 = obj1['box']['y']
        y2 = obj2['box']['y']

        sep_sq = (x1 - x2) ** 2 + (y1 - y2) ** 2
        sep = np.sqrt(sep_sq)
        return sep

    # Code for working out tilt
    search_radius = 0.05
    for i in range(len(rotated_objects)):
        r_obj = rotated_objects[i]
        obj = objects[i]
        dists = [centroid_sep(r_obj, t_obj) for t_obj in tilted_objects]

        if len(dists) == 0:
            obj['box']['tilt'] = None
        else:
            if np.min(dists) < search_radius:
                closest_obj = tilted_objects[np.argmin(dists)]
                c_obj = closest_obj  # TODO: Add some conditions here, eg type needs to be similar

                if c_obj['box']['w'] > obj['box']['w']:
                    if c_obj['box']['h'] < obj['box']['h']:
                        obj['box']['tilt'] = 'l'
                    else:
                        obj['box']['tilt'] = 'u'  # TODO: Some way to say is uncertain
                else:
                    if c_obj['box']['h'] > obj['box']['h']:
                        obj['box']['tilt'] = 'r'
                    else:
                        obj['box']['tilt'] = 'u'

                print(r_obj, dists, 'closest: ', closest_obj)
            else:
                obj['box']['tilt'] = 'u'
                # obj['box']['tilt'] = None

    # print(objects)
    # TODO: Change this back to without plates
    # plates = get_plates(image_name, image_path, show_img=show_imgs)
    # objects = objects + plates
    print(objects)

    return objects


# for txtfile in glob.glob("*.txt"):
#     if txtfile[0] == "t":
#         print(txtfile)
#         print(get_objs_from_true_txt(txtfile))
#         input('next file?')

# # code to get list of images, useful now they're not called tray1, 2, etc
# os.chdir(darknet_path + 'test_imgs/openloop/gtruth/')
# for image in glob.glob("*.jpg"):
#     img_list.append(image)
# print(img_list)
# print(len(img_list))
#
# np.save('exp1_img_list.npy', img_list)
# input()

# # code to get true object files from text files
# os.chdir(darknet_path + 'test_imgs/openloop/')
# img_list = np.load('exp1_img_list.npy')
# for i in range(len(img_list)):
#     image_name = img_list[i]
#     text_name = image_name[:-4] + '.txt'
#     objects = get_objs_from_true_txt('gtruth/' + text_name)
#     print(objects)
#     np.save('gtruth/' + image_name[:-4] + '-tobjs.npy', objects)
# input()

# #
# os.chdir(darknet_path + 'test_imgs/openloop/')
# img_list = np.load('exp1_img_list.npy')
# for i in range(len(img_list)):
#     image_name = img_list[i]
#     name = image_name[:-4]
#     objects = get_objects_from_2_txts('yolo/' + name + '.txt', 'yolo/' + name + '_tilt5.txt', show_imgs=False)
#
#     # do plate (big + small)? and bowl detection through circles, probably in yolofuncs, and append to text document
#     plates = get_experiment_plates('yolo/' + image_name, show_img=False)
#     all_objects = np.concatenate((objects, plates))
#     print(all_objects)
#     np.save('yolo/' + name + '-yobjs.npy', all_objects)
#
# print('Been through all the images!')
# input()

# code to find plan output
os.chdir(darknet_path + 'test_imgs/openloop/')
img_list = np.load('exp1_img_list.npy')
for i in range(len(img_list)):
    print(i+1)
    image_name = img_list[i]
    name = image_name[:-4]
    output_path = 'output/' + name + '-output.txt'

    pick_str = 'Pick in'
    time_str = 'TIME! Cycle'
    with open(output_path) as file:
        for line in file:
            if line.startswith(pick_str) or line.startswith(time_str):
                print(line)


input('stop')
# setup counts
predict_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
iou_lists = {'fork': [], 'spoon': [], 'knife': [], 'cup': [], 'plate': [], 'bowl': [], 'cutlery': []}


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        # print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image


os.chdir(darknet_path + 'test_imgs/openloop/')
img_list = np.load('exp1_img_list.npy')
for i in range(len(img_list)):
    image_name = img_list[i]
    filename = image_name[:-4]

    print(i+1, filename)
    try:
        objects = np.load('yolo/' + filename + '-yobjs.npy', allow_pickle=True)

        true_objects = np.load('gtruth/' + filename + '-tobjs.npy', allow_pickle=True)

        old_ground_counts = ground_counts
        old_predict_counts = predict_counts
        old_tp_counts = tp_counts

        temp_ground = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
        temp_predict = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
        temp_tp = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
        temp_close_enough = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}

        for true_obj in true_objects:
            obj_type = true_obj['type']
            # print(obj_type)

            for obj in objects:
                if obj['type'] == true_obj['type']:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    # print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)

                    iou_thresh = 0.5
                    if iou >= iou_thresh:
                        tp_counts[true_obj['type']] = tp_counts[true_obj['type']] + 1
                        to_app_list = iou_lists[true_obj['type']]
                        iou_lists[true_obj['type']] = np.concatenate((to_app_list, [iou]))

                        temp_tp[true_obj['type']] = temp_tp[true_obj['type']] + 1
                    # else:
                    #     print('didnt pass: ', true_obj['type'])

                # adding to cutlery true count if fork and fork or fork and spoon or fork and cutlery etc
                cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']
                if obj['type'] in cutlery_list and true_obj['type'] in cutlery_list:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    # print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    # print([inter, union, iou])

                    iou_thresh = 0.5
                    if iou >= iou_thresh:
                        tp_counts['cutlery'] = tp_counts['cutlery'] + 1
                        to_app_list = iou_lists['cutlery']
                        iou_lists['cutlery'] = np.concatenate((to_app_list, [iou]))
                    # else:
                    #     print('didnt pass: ', true_obj['type'])

                    # print('done single comp')

        # TODO: Fix this, consider refactoring you have a list of cutlery objects predicted, try all out at once, look if value greater than thresh exists in list
        cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']
        cutlery_predictions = []
        for obj in objects:
            if obj['type'] in cutlery_list:
                cutlery_predictions.append(obj)
        # print(true_objects)
        # print(objects)
        # print(cutlery_predictions)
        if len(cutlery_predictions) > 0:
            for true_obj in true_objects:
                # print('here1', true_obj['type'])
                if true_obj['type'] in cutlery_list:
                    empty_ious = np.zeros(len(cutlery_predictions))
                    # print('here2')
                    # find one that
                    for c in range(len(cutlery_predictions)):
                        cut_obj = cutlery_predictions[c]
                        # print('here3')

                        x1 = cut_obj['box']['x']
                        y1 = cut_obj['box']['y']
                        w1 = cut_obj['box']['w']
                        h1 = cut_obj['box']['h']

                        x2 = true_obj['box']['x']
                        y2 = true_obj['box']['y']
                        w2 = true_obj['box']['w']
                        h2 = true_obj['box']['h']

                        # print(x1, y1, w1, h1, x2, y2, w2, h2)

                        inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                        # print([inter, union, iou])
                        empty_ious[c] = iou
                    # print(empty_ious)
                    for iou in empty_ious:
                        if iou >= 0.5:
                            temp_close_enough[true_obj['type']] += 1
                            # break  # (to only get if there is one or not)
        # else:
        #     print('no cutlery predicted')

        # add to the counts and draw rectangles
        for true_obj in true_objects:
            # if true_obj['type'] == 'knife':
            #     print('knife in', str(filename))
            ground_counts[true_obj['type']] = ground_counts[true_obj['type']] + 1
            temp_ground[true_obj['type']] = temp_ground[true_obj['type']] + 1
            # draw rectangle
            # draw_cv_rect(img, true_obj, isGround=True)
        for obj in objects:
            try:
                predict_counts[obj['type']] = predict_counts[obj['type']] + 1
            except KeyError:
                print(obj['type'], 'not one of object set')

            try:
                temp_predict[obj['type']] = temp_predict[obj['type']] + 1
            except KeyError:
                print(obj['type'], 'not one of object set')

            # draw rectangle
            # draw_cv_rect(img, obj, isGround=False)
        if i in [7, 8, 16, 19, 20, 21, 22, 23, 24]:
            print('grnd:', temp_ground.values())
            print('pred:', temp_predict.values())
            print('trup:', temp_tp.values())
            print('enuf:', temp_close_enough.values())
            print('                 *****                   ')

        # print(old_ground_counts, old_predict_counts, old_tp_counts)
        # print(ground_counts, predict_counts, tp_counts, iou_lists)

        # NUMPY SAVE SOME SHIT
        np.save('ground_counts.npy', ground_counts)
        np.save('predict_counts.npy', predict_counts)
        np.save('tp_counts.npy', tp_counts)
        np.save('iou_lists.npy', iou_lists)

        # overlaps1 = yf.get_overlap_matrix(true_objects)
        # overlaps2 = yf.get_overlap_matrix(objects)
        # print(yf.get_pick_order(true_objects, overlaps1))
        # print(yf.get_pick_order(objects, overlaps2))

    except IOError:
        print('File not accessible')

print(ground_counts)
print(predict_counts)
print(tp_counts)

print('DONE')