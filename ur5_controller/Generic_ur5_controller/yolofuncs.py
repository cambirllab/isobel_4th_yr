import numpy as np
import cv2
import json
from datetime import datetime
import imutils
import os
import subprocess

darknet_path = "./darknet/build/darknet/x64/"

def run_yolo(image_name, image_path, show_img=True):
    # use Python command to run YOLO from here
    # within YOLO create text file of same name

    # change directory to darknet\build\darknet\x64 to run YOLO


    # command = "darknet.exe detect cfg/yolov3.cfg yolov3.weights data/dog.jpg"
    command = "darknet.exe detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights " + image_path
    print(command)

    if show_img:
        command = command
    else:
        command = command + " -dont_show 1"

    # command = "darknet.exe detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights " + image_path + " -dont_show " + dont_show
    # run command
    subprocess.call(command)
    # Prints in a super weird way
    # TODO: Get it to automatically exit the image, perhaps not display temporarily?


    image_txt_path = image_path[:-3] + 'txt'
    return image_txt_path


def get_tilted_bboxs(image_name, image_path, tilt=5, show_img=True):
    rad_tilt = np.deg2rad(tilt)
    # make a tilted image, anticlockwise
    img = cv2.imread(image_path)
    tilted_img = imutils.rotate(img, tilt)
    # might want to do rotate_bound()

    #cv2.imshow('tilt', tilted_img)
    #cv2.waitKey(0)

    # save tilted image to path
    print(image_name, ' image name in tilted')
    tilted_name = image_name + '_tilt' + str(tilt)
    tilted_path = image_path[:-4] + '_tilt' + str(tilt) + '.jpg'
    # print(tilted_path)
    cv2.imwrite(tilted_path, tilted_img)

    # get textfile from yolo
    tilted_txt_path = run_yolo(tilted_name, tilted_path, show_img=show_img)

    return tilted_txt_path


def get_lr_tilt(image_name, image_path, image_txt_path, tilt=5, show_imgs=True):
    rad_tilt = np.deg2rad(tilt)

    img_txt_path = run_yolo(image_name, image_path, show_img=show_imgs)  # Getting text file for non rotated image

    tilted_txt_path = get_tilted_bboxs(image_name, image_path, tilt=tilt, show_img=show_imgs)

    objects = get_objs_from_txt(img_txt_path)
    tilted_objects = get_objs_from_txt(tilted_txt_path)

    # print('obj:', objects)
    # print('tbj:', tilted_objects)

    # TODO: Comparison of objects and tilted objects
    # need to search in region

    # will do transformation of object centroids as if rotated, then search for nearest match
    rotated_objects = []
    for obj in objects:
        rotate_obj = obj
        curr_x = obj['box']['x'] - 0.5
        curr_y = 0.5 - obj['box']['y']

        rot_x = (np.cos(rad_tilt) * curr_x - np.sin(rad_tilt) * curr_y) + 0.5
        rot_y = 0.5 - (np.sin(rad_tilt) * curr_x + np.cos(rad_tilt) * curr_y)

        rotate_obj['box']['x'] = rot_x
        rotate_obj['box']['y'] = rot_y

        rotated_objects.append(rotate_obj)

    # print('rbj:', rotated_objects)

    # comparison...
    def centroid_sep(obj1, obj2):
        x1 = obj1['box']['x']
        x2 = obj2['box']['x']
        y1 = obj1['box']['y']
        y2 = obj2['box']['y']

        sep_sq = (x1 - x2) ** 2 + (y1 - y2) ** 2
        sep = np.sqrt(sep_sq)
        return sep

    # Code for working out tilt
    search_radius = 0.05
    for i in range(len(rotated_objects)):
        r_obj = rotated_objects[i]
        obj = objects[i]
        dists = [centroid_sep(r_obj, t_obj) for t_obj in tilted_objects]

        if len(dists) == 0:
            obj['box']['tilt'] = None
        else:
            if np.min(dists) < search_radius:
                closest_obj = tilted_objects[np.argmin(dists)]
                c_obj = closest_obj  # TODO: Add some conditions here, eg type needs to be similar

                if c_obj['box']['w'] > obj['box']['w']:
                    if c_obj['box']['h'] < obj['box']['h']:
                        obj['box']['tilt'] = 'l'
                    else:
                        obj['box']['tilt'] = 'u'  # TODO: Some way to say is uncertain
                else:
                    if c_obj['box']['h'] > obj['box']['h']:
                        obj['box']['tilt'] = 'r'
                    else:
                        obj['box']['tilt'] = 'u'

                print(r_obj, dists, 'closest: ', closest_obj)
            else:
                obj['box']['tilt'] = 'u'
                # obj['box']['tilt'] = None

    # print(objects)
    # TODO: Change this back to without plates
    # plates = get_plates(image_name, image_path, show_img=show_imgs)
    # objects = objects + plates
    print(objects)

    return objects


def get_plates(image_name, image_path, show_img=True):
    plate_objects = []
    img = cv2.imread(image_path, 0)
    cimg = cv2.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # for small plastic
    p1 = 80
    p2 = 50
    min_r = 120
    max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    if circles is None:
        print('nothing')
        return []
    else:
        circles = np.uint16(np.around(circles))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

        if show_img:
            cv2.imshow('image', img)
            cv2.imshow('detected circles', cimg)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        print(plate_objects)
        return plate_objects


def draw_cv_rect(image_path, objects, isGround=False, show=True, pick_order=None, draw_percents=False):
    image = cv2.imread(image_path)
    height = image.shape[0]
    width = image.shape[1]
    print(width, height)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)
    for obj_dict in objects:
        x = obj_dict['box']['x']
        y = obj_dict['box']['y']
        w = obj_dict['box']['w']
        h = obj_dict['box']['h']

        top_left_x = int((x - (w / 2)) * width)
        top_left_y = int((y - (h / 2)) * height)
        bot_right_x = int((x + (w / 2)) * width)
        bot_right_y = int((y + (h / 2)) * height)

        pt1 = (top_left_x, top_left_y)
        pt2 = (bot_right_x, bot_right_y)

        cv2.rectangle(image, pt1, pt2, color, 2)
    if pick_order != None:
        i = 1
        for j in range(len(pick_order)):
            obj_dict = objects[j]
            x = int(obj_dict['box']['x']* width)
            y = int(obj_dict['box']['y']* height)
            label = str(i) + ': ' + obj_dict['type']
            cv2.putText(image, label, (x-len(label)*4,y), cv2.FONT_HERSHEY_SIMPLEX, .5,(0,0,0),1,cv2.LINE_AA)
            i += 1
    if draw_percents:
        for obj_dict in objects:
            x = int(obj_dict['box']['x'] * width)
            y = int((obj_dict['box']['y'] - 0.5 * obj_dict['box']['h']) * height - 5)
            prob = str(np.round(obj_dict['prob'])) + '%'
            label = obj_dict['type'] + ': ' + prob
            cv2.putText(image, label, (x - len(label) * 4, y), cv2.FONT_HERSHEY_SIMPLEX, .5, (0, 0, 0), 1, cv2.LINE_AA)
        pass

    if show:
        cv2.imshow('image', image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    return image


def get_objs_from_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            objects.append(json.loads(line))

    # reversing so highest probability at start
    objects = sorted(objects, key=lambda i: i['prob'], reverse=True)

    return objects


def get_intersect_area(obj1, obj2):
    # return intersection
    x1 = obj1['box']['x']
    y1 = obj1['box']['y']
    w1 = obj1['box']['w']
    h1 = obj1['box']['h']

    x2 = obj2['box']['x']
    y2 = obj2['box']['y']
    w2 = obj2['box']['w']
    h2 = obj2['box']['h']

    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        # print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        return intersection
    else:
        intersection = b * l
        return intersection


def get_overlap_matrix(objects):
    # take a list of dictionaries about objects in the image of length N
    # create an N x N matrix, A, with values about bounding box overlap
    # A[i,j] = area of intersection of i and j / area of i
    # A[i,i] = 1
    # A[i,j] = 0 if no overlap
    # interpretation: if A[i,j] is large, implies i is on top of j
    n = len(objects)
    a = np.zeros(shape=(n, n))

    for i in range(n):
        box_area = objects[i]['box']['w'] * objects[i]['box']['h']
        for j in range(n):
            if i == j:
                a[i, j] = 1
            else:
                int_area = get_intersect_area(objects[i], objects[j])
                a[i, j] = int_area / box_area

    print(a)

    return a


def get_pick_order(objects, high_thresh=0.8, low_thresh=0.1):
    # list returned is list of indices for objects in the order they should be picked
    # e.g. [2, 1, 3, 0] means pick objects[2], then objects[1], etc
    overlaps = get_overlap_matrix(objects)

    n = np.shape(overlaps)[0]
    relations = np.zeros(shape=(n, n))

    my_objs = objects

    for i in range(n):
        for j in range(n):
            fract = overlaps[i, j]
            if i != j:
                if fract >= high_thresh:
                    # print(my_objs[i]['type'], 'is on top of', my_objs[j]['type'])
                    relations[i, j] = 1
                elif high_thresh > fract > low_thresh:
                    # print(my_objs[i]['type'], 'is underneath', my_objs[j]['type'])
                    relations[i, j] = -1
                elif fract >= 0:
                    # print(my_objs[i]['type'], 'and', my_objs[j]['type'], 'do not overlap')
                    relations[i, j] = 0
        # print('--')

    # print(relations)

    picking = [0]

    for i in range(1, n):
        k = 0
        while k < len(picking):
            j = picking[k]
            # print(i, j, k)

            rel = relations[i, j]
            rel_opp = relations[j, i]
            # print(rel)

            if rel == 1 and rel_opp == 1:
                picking.insert(k+1, i)
                # insert after j
                # check this
                # print('on top of!')
                break
            elif rel == 1:
                picking.insert(k, i)
                # insert before j
                # print('on top of!')
                break
            k += 1
            if k == len(picking):
                picking.append(i)
                # insert at end of list
                # print('end of list')
                break
        # print(picking)

    order = [my_objs[n]['type'] for n in picking]
    print('Pick in the following order: ', order)

    return picking
