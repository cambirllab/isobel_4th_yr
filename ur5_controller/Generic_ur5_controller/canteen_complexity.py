import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
import matplotlib.pyplot as plt

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def get_canteen_plates(image_path, show_img=True):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # # for small plastic
    # p1 = 80
    # p2 = 50
    # min_r = 120
    # max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    # for big canteen plates
    p1 = 80
    p2 = 60
    min_r = 355
    max_r = 370

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    # for small canteen plates
    p1 = 80
    p2 = 50
    min_r = 230
    max_r = 240

    circles_s = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    print(circles_b)
    print(circles_s)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if circles_s is not None:
        circles = np.uint16(np.around(circles_s))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0,
                                  "box": {"x": i[0] / wid, "y": i[1] / hei, "w": 2 * i[2] / wid,
                                          "h": 2 * i[2] / hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    print(plate_objects)
    return plate_objects


# setup counts
predict_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
iou_lists = {'fork': [], 'spoon': [], 'knife': [], 'cup': [], 'plate': [], 'bowl': [], 'cutlery': []}
temp_close_enough = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
temp_tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image


os.chdir(canteen_path)
close_enough = True
cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']

overall_true_ious = []
overall_ce_ious = []

ground_cups = []
true_predicted_cups = []

for i in range(1,70):
    filename = 'tray' + str(i) + '_c'

    print(filename)
    try:
        tray_true_ious = []
        tray_ce_ious = []

        objects = np.load('yolo/' + filename + '_objs.npy', allow_pickle=True)
        plates = get_canteen_plates('yolo/' + filename + '.jpg', show_img=False)
        objects = np.concatenate((objects, plates))
        print(objects)
        # RUN GET PLATES

        true_objects = get_objs_from_true_txt('gtruth/' + filename + '.txt')
        print(true_objects)

        old_ground_counts = ground_counts
        old_predict_counts = predict_counts
        old_tp_counts = tp_counts

        for true_obj in true_objects:
            object_true_ious = [0]
            object_ce_ious = [0]
            obj_type = true_obj['type']
            print(obj_type)
            if true_obj['type'] == 'cup':
                ground_cups.append((true_obj['box']['x'], true_obj['box']['y']))

            for obj in objects:
                if (obj['type'] == true_obj['type']) or (obj['type'] in cutlery_list and true_obj['type'] in cutlery_list):
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    object_ce_ious.append(iou)
                if obj['type'] == true_obj['type']:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    object_true_ious.append(iou)

                    if obj['type'] == 'cup' and iou >= 0.5:
                        true_predicted_cups.append((true_obj['box']['x'], true_obj['box']['y']))

            # find max
            max_ce_iou = max(object_ce_ious)
            max_true_iou = max(object_true_ious)
            print(max_ce_iou, max_true_iou)

            tray_ce_ious.append(max_ce_iou)
            tray_true_ious.append(max_true_iou)

        print(len(true_objects), len(tray_ce_ious), len(tray_true_ious))

        avg_ce_iou = sum(tray_ce_ious) / len(tray_ce_ious)
        avg_true_iou = sum(tray_true_ious) / len(tray_true_ious)

        overall_true_ious.append(avg_true_iou)
        overall_ce_ious.append(avg_ce_iou)

    except IOError:
        print('File not accessible')

# cups
print(ground_cups)
print(true_predicted_cups)
ground_cup_xs = [x[0] for x in ground_cups]
ground_cup_ys = [1-x[1] for x in ground_cups]
ground_dist_to_cent = [((a-0.5)**2 + (b-0.5)**2)**0.5 for a, b in zip(ground_cup_xs, ground_cup_ys)]
predicted_xs = [x[0] for x in true_predicted_cups]
predicted_ys = [1-x[1] for x in true_predicted_cups]
predicted_dist_to_cent = [((a-0.5)**2 + (b-0.5)**2)**0.5 for a, b in zip(predicted_xs, predicted_ys)]
print(np.mean(ground_dist_to_cent), np.mean(predicted_dist_to_cent))
print((np.sum(ground_dist_to_cent) - np.sum(predicted_dist_to_cent)) / (len(ground_dist_to_cent) - len(predicted_dist_to_cent)))
plt.figure(figsize=(5.76, 3.24))
plt.scatter(ground_cup_xs, ground_cup_ys, c='red')
plt.scatter(predicted_xs, predicted_ys, c='green', marker='.')
plt.xlim([0,1])
plt.ylim([0,1])
plt.show()


print(overall_true_ious)
print(overall_ce_ious)

print(sorted(overall_ce_ious, reverse=True))

img_list = ['tray' + str(i) + '_c' for i in range(1, 70)]
print(img_list)

sorted_true_img_list = [x for _, x in sorted(zip(overall_true_ious, img_list), reverse=True)]
sorted_ce_img_list = [x for _, x in sorted(zip(overall_ce_ious, img_list), reverse=True)]
print(sorted_true_img_list)
print(sorted_ce_img_list)

averaged_ious = [(a + b) / 2 for a, b in zip(overall_true_ious, overall_ce_ious)]
print('avg', averaged_ious)

corresponding_ce_ious = [overall_ce_ious[int(x[4:-2]) - 1] for x in sorted_true_img_list]
print(corresponding_ce_ious)

level1_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if a >= 0.6]
level1_ious = [a for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if a >= 0.6]
print(level1_imgs)
print(sorted(overall_true_ious, reverse=True)[:len(level1_imgs)])
print(len(level1_imgs))
level2_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.6 > a >= 0.4]
level2_ious = [a for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.6 > a >= 0.4]
level3_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.4 > a]
level3_ious = [a for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.4 > a]
print(len(level1_imgs), len(level2_imgs), len(level3_imgs))
print(level1_imgs[7], level2_imgs[4], level2_imgs[13], level3_imgs[1])

plt.figure()
plt.plot(np.arange(1, 70), sorted(overall_true_ious, reverse=True))
# plt.plot(np.arange(0, 69), corresponding_ce_ious)
# plt.plot(np.arange(0, 69), sorted(overall_ce_ious, reverse=True))
# plt.plot(np.arange(0, 69), sorted(averaged_ious, reverse=True))
plt.xlim([0,70])
plt.ylim([0, 1])
plt.xlabel('ranking')
plt.ylabel('average iou across objects on tray')
# plt.show()
# input('do not continue')

print(sorted(overall_true_ious)[0])
print(sorted(overall_true_ious)[-1])

best_iou = sorted_true_img_list[0]
image_path = gtruth_folder + best_iou + '.jpg'
best = cv.imread(image_path)
cv.imwrite('canteen-best.jpg', best[:, 200:-200])
# cv.imshow('best', best[:, 200:-200])
# cv.waitKey(0)
# cv.destroyAllWindows()


level1_inds = [0, 7, 9, 11]  # [7, 9, 13, 11]
level2_inds = [1, 4, 13, 21]
level3_inds = [1, 6, 10, 12]

l1_4_imgs = [level1_imgs[ind] for ind in level1_inds]
l2_4_imgs = [level2_imgs[ind] for ind in level2_inds]
l3_4_imgs = [level3_imgs[ind] for ind in level3_inds]
l1_4_ious = [level1_ious[ind] for ind in level1_inds]
l2_4_ious = [level2_ious[ind] for ind in level2_inds]
l3_4_ious = [level3_ious[ind] for ind in level3_inds]

# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
for i in range(len(l1_4_imgs)):
    filename = l1_4_imgs[i]

    image_path = gtruth_folder + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(1920 / 4)
    new_h = int(1080 / 4)
    resized = cv.resize(img, (new_w, new_h))
    resized = resized[:, 50:-50]
    og_h = 1080
    og_w = 1920

    # setup text
    font = cv.FONT_HERSHEY_SIMPLEX
    text = str(round(l1_4_ious[i],3))
    print('iou', text)
    size = 1.5
    thickness = 3

    # get boundary of this text
    textsize = cv.getTextSize(text, font, size, thickness)[0]
    print(textsize)

    pad = resized.shape[0] // 20
    # get coords based on boundary
    textX = (resized.shape[1] - textsize[0]) // 2
    textY = (textsize[1]) + pad

    # add text centered on image
    cv.putText(resized, text, (textX, textY), font, size, (0, 0, 0), thickness + 2)
    cv.putText(resized, text, (textX, textY), font, size, (255, 255, 255), thickness)

    n_across = 2

    if i % n_across == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

# vertically stacking rows to create final collage.
collage = rows[0]
collage = np.vstack([collage, cur_row])


print(collage.shape)
# collage = cv.resize(collage, (int(collage.shape[1] / 2), int(collage.shape[0] / 2)))
cv.imwrite('canteen-level1-text.jpg', collage)
# cv.imshow('halp', collage)
# cv.waitKey(0)
# cv.destroyAllWindows()
# input('waiiit')

# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
for i in range(len(l2_4_imgs)):
    filename = l2_4_imgs[i]

    image_path = gtruth_folder + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(1920 / 4)
    new_h = int(1080 / 4)
    resized = cv.resize(img, (new_w, new_h))
    resized = resized[:, 50:-50]
    og_h = 1080
    og_w = 1920

    # setup text
    font = cv.FONT_HERSHEY_SIMPLEX
    text = str(round(l2_4_ious[i], 3))
    if len(text) < 5:
        text += '0'
    print('iou', text)
    size = 1.5
    thickness = 3

    # get boundary of this text
    textsize = cv.getTextSize(text, font, size, thickness)[0]
    print(textsize)

    pad = resized.shape[0] // 20
    # get coords based on boundary
    textX = resized.shape[1] - (textsize[0]) - pad
    textX = (resized.shape[1] - textsize[0]) // 2
    textY = (textsize[1]) + pad

    # add text centered on image
    cv.putText(resized, text, (textX, textY), font, size, (0, 0, 0), thickness + 2)
    cv.putText(resized, text, (textX, textY), font, size, (255, 255, 255), thickness)

    n_across = 2

    if i % n_across == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

# vertically stacking rows to create final collage.
collage = rows[0]
collage = np.vstack([collage, cur_row])


print(collage.shape)
# collage = cv.resize(collage, (int(collage.shape[1] / 2), int(collage.shape[0] / 2)))
cv.imwrite('canteen-level2-text.jpg', collage)

# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
for i in range(len(l3_4_imgs)):
    filename = l3_4_imgs[i]

    image_path = gtruth_folder + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(1920 / 4)
    new_h = int(1080 / 4)
    resized = cv.resize(img, (new_w, new_h))
    resized = resized[:, 50:-50]
    og_h = 1080
    og_w = 1920

    # setup text
    font = cv.FONT_HERSHEY_SIMPLEX
    text = str(round(l3_4_ious[i], 3))
    print('iou', text)
    size = 1.5
    thickness = 3

    # get boundary of this text
    textsize = cv.getTextSize(text, font, size, thickness)[0]
    print(textsize)

    pad = resized.shape[0] // 20
    # get coords based on boundary
    textX = resized.shape[1] - (textsize[0]) - pad
    textX = (resized.shape[1] - textsize[0]) // 2
    textY = (textsize[1]) + pad

    # add text centered on image
    cv.putText(resized, text, (textX, textY), font, size, (0, 0, 0), thickness + 2)
    cv.putText(resized, text, (textX, textY), font, size, (255, 255, 255), thickness)

    n_across = 2

    if i % n_across == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

# vertically stacking rows to create final collage.
collage = rows[0]
collage = np.vstack([collage, cur_row])


print(collage.shape)
# collage = cv.resize(collage, (int(collage.shape[1] / 2), int(collage.shape[0] / 2)))
cv.imwrite('canteen-level3-text.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()


# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
for i in range(0, 69):
    filename = sorted_ce_img_list[i]

    image_path = gtruth_folder + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(1920 / 4)
    new_h = int(1080 / 4)
    resized = cv.resize(img, (new_w, new_h))
    og_h = 1080
    og_w = 1920

    n_across = 9

    if i % n_across == 0:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

n_filler = n_across - (69 % n_across)
print(n_filler)

if n_filler > 0:
    fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
    fill_img.fill(255)
    cur_row = np.hstack([cur_row, fill_img])
    rows.append(cur_row)

# vertically stacking rows to create final collage.
collage = rows[0]

for k in range(1, len(rows)):
    collage = np.vstack([collage, rows[k]])

print(collage.shape)
collage = cv.resize(collage, (2160, 1080))
cv.imwrite('canteen-ordered-close-enough-collage.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()



print('DONE')
