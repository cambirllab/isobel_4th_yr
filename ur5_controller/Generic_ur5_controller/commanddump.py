def cup_play(robot):
    # Playing taught movement for cup pick
    robot.open_hand()
    robot.teach_mode.play("cup.json")
    robot.close_hand()
    robot.home()
    return True


def plate_closed_loop(urnie):
    return True


def plate_play(robot):
    # Playing taught movement for cup pick
    robot.open_hand()
    robot.teach_mode.play("plate.json")
    robot.close_hand()
    robot.home()
    return True


def bplate_play(robot):
    # Playing taught movement for cup pick
    robot.open_hand()
    robot.teach_mode.play("bplate.json")
    robot.close_hand()
    robot.home()
    return True


def fork_play(robot):
    # Playing taught movement for cup pick
    robot.open_hand()
    robot.teach_mode.play("fork.json")
    robot.close_hand()
    robot.home()
    return True


def plate_pick(robot):
    robot.open_hand()
    robot.translatejl([0.22, -0.45, 0.12])
    robot.rotate_rel([0, 0, 0, 0, 0, np.pi/2])
    robot.movej(wp.urnie_platetempj)
    input()
    robot.translatejl_rel([-0.030, 0, -0.01])
    input()
    robot.close_hand()
    robot.translatejl_rel([0, 0, 0.2])
    robot.movej(wp.urnie_gripdownvhighperpj)
    # get over dishwasher
    # robot.teach_mode.record_end_pos()
    input()
    robot.movej(wp.urnie_platetemp1j)
    # get above a slot
    # robot.teach_mode.record_end_pos()
    input()
    robot.movej(wp.urnie_platetemp2j)
    # get into a slot
    # robot.teach_mode.record_end_pos()
    input()
    robot.movej(wp.urnie_platetemp3j)
    input()
    robot.translatejl_rel([-0.02,0,-0.04])
    robot.open_hand()
    input()
    robot.translatejl_rel([-0.08, 0, -0.01])
    input()
    robot.translatejl_rel([-0.02, 0, 0.03])
    robot.movej(wp.urnie_platetemp1j)

    # extract from under plate
    # robot.translatejl_rel([0.025, 0, 0])
    # robot.translatejl_rel([0.01, 0, 0.03])

    #robot.movej(wp.urnie_gripdownperpj)

    return True



def find_plates(img_path):
    plate_dicts = []

    img = cv2.imread(img_path, 0)
    img = cv2.medianBlur(img, 5)
    cv2.imshow('image', img)
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20,
                               param1=100, param2=100, minRadius=100, maxRadius=0)

    circles = np.uint16(np.around(circles))
    for i in circles[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)

        x = i[0] / wp.img_width
        y = i[1] / wp.img_height
        w = 2 * i[2] / wp.img_width
        h = w

        probab = 70
        plate_dict = {"type": "plate", "prob": probab, "box": {"x": x, "y": y, "w": w, "h": h}}
        plate_dicts.append(plate_dict)
        # will hopefully dump directly to json here?

    print(circles)
    print(plate_dicts)

    cv2.imshow('detected circles', cimg)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return plate_dicts


def pick_objects1(robot, filename):
    # eg filename = 'detected'
    #filename = filename + '.txt'
    robot.home()
    print(filename)
    objects = []
    with open(filename) as f:
        for line in f:
            objects.append(json.loads(line))

    # reversing so highest probability at start
    objects.reverse()

    for obj in objects:
        o_type = obj['type']
        print(o_type)
        o_box = convert_box_to_robot(obj['box'])
        input('Press enter to continue')
        if o_type == 'cup':
            cup_bounded(robot, o_box)
        elif o_type == 'knife' or o_type == 'spoon' or o_type == 'fork':
            cutlery_bounded(robot, o_box)
        # elif o_type == 'bowl':
        #     bowl_bounded(robot, o_box)
        # elif o_type == 'plate':
        #     plate_bounded(robot, o_box)
        else:
            print('Cannot deal with object type: ', o_type)

    return True


def cutlery_bounded1(robot, box_dict):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']

    l_off = wp.l_gtoTCP

    # input('check is above?')
    # robot.translatejl([x,y,0.15], vel=0.05)

    # angle from vertical, will do by feeding a rotated version into darknet
    angle = np.arctan(w / h)  # will be in radians
    print(angle)
    ipt = input('leaning left or right?')
    if ipt == 'l':
        angle = -angle
    elif ipt == 'r':
        angle = angle
    # do something with that
    # rotate joint 5(?) by angle

    x_off = l_off * np.cos(angle)
    y_off = l_off * np.sin(angle)
    print(x_off, y_off)

    x_pick = x - x_off
    y_pick = y + y_off

    from_plate_h = -0.024  # 0.10 - 0.024
    from_surf_h = -0.034

    robot.open_hand()
    robot.movej(wp.urnie_gripdownperpj)
    input('pic1')
    robot.rotate_rel([0,0,0,0,angle,0])
    input('pic2')
    robot.translatel([x_pick, y_pick, 0.10], vel=0.2)
    input('pic3')
    input('is it above?')
    robot.translatel_rel([0, 0, from_surf_h], vel=0.05)  # go down
    input('pic4')
    time.sleep(0.5)
    # robot.translatel_rel([0, -0.01, 0], vel=0.1)
    robot.close_hand()
    input('pic5')
    time.sleep(0.5)
    robot.translatel_rel([0, 0, 0.2], vel=0.1)
    input('pic6')

    # currently instead of putting in dishwasher
    robot.home()
    time.sleep(1)
    robot.open_hand()
    return True