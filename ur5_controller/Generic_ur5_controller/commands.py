import time
import serial
from math import pi
import numpy as np
import socket
import random
from matplotlib import pyplot as plt
import cv2
import json
from datetime import datetime
import os

import waypoints as wp
import kg_robot as kgr
import yolofuncs as yf

def main():
    print("------------Configuring Urnie-------------\r\n")
    urnie = kgr.kg_robot(port=30010, db_host="169.254.150.100", ee_port="COM6")
    #urnie = kgr.kg_robot(port=30010,db_host="169.254.50.100")
    darknet_path = "./darknet/build/darknet/x64/"
    #urnie = kgr.kg_robot(port=30010,ee_port="COM32",db_host="192.168.1.51")
    print("----------------Hi Urnie!-----------------\r\n\r\n")

    try:
        while 1:
            ipt = input("cmd: ")
            if ipt == 'close':
                break
            elif ipt == 'home':
                urnie.home()
            elif ipt == 'away':
                urnie.robotfuncs.move_away()
            elif ipt == 'cycle':
                for i in range(2):
                    urnie.open_hand()
                    time.sleep(1)
                    urnie.close_hand()
                    time.sleep(1)
            elif ipt == 'run':
                img_name, img_path, txt_path = get_initial_img(urnie)
                print(img_name, img_path, txt_path)
                # input('Get objs?')
                objects = get_objects_in_img(img_name, img_path, txt_path, show_imgs=True)
                input('Go on?')
                objects = get_picking_list(objects)
                print(objects)
                input('Pick objs?')
                pick_objects(urnie, objects)
            elif ipt == 'runf':
                tic = time.time()
                img_name, img_path, txt_path = get_initial_img(urnie)
                toc1 = time.time()
                print('TIME! Got image after:', toc1 - tic)
                print(img_name, img_path, txt_path)
                objects = get_objects_in_img(img_name, img_path, txt_path, show_imgs=False)
                toc2 = time.time()
                print('TIME! Finished running YOLO after:', toc2 - tic)
                objects = get_picking_list(objects)
                toc3 = time.time()
                print('TIME! Made plan after:', toc3 - tic)
                print(objects)
                pick_objects(urnie, objects)
                toc4 = time.time()
                print('TIME! Finished picking objects after:', toc4 - tic)
                final_name = img_name + '-final'
                imag_name, imag_path, text_path = get_final_img(urnie, image_name=final_name)
                print(imag_name, imag_path, text_path)
                toc5 = time.time()
                print('TIME! Got final image after:', toc5 - tic)
                print('TIME! Cycle time:', toc4 - tic)
                # objects_final = get_objects_in_img(imag_name, imag_path, text_path, show_imgs=False)
                # toc6 = time.time()
                # print('TIME! Finished running YOLO again after:', toc6 - tic)
                # print(objects_final)
            elif ipt == 'runfcyc':
                tic = time.time()
                # run for max 3 tests
                for i in range(3):
                    print('Look number', i+1)
                    print('***')
                    img_name, img_path, txt_path = get_initial_img(urnie)
                    toc1 = time.time()
                    print('TIME! Got image after:', toc1 - tic)
                    print(img_name, img_path, txt_path)
                    objects = get_objects_in_img(img_name, img_path, txt_path, show_imgs=False)
                    toc2 = time.time()
                    print('TIME! Finished running YOLO after:', toc2 - tic)
                    if len(objects) == 0:
                        print('Nothing seen in this image')
                        break
                    objects = get_picking_list(objects)
                    if len(objects) == 0:
                        print('Nothing pickable seen in this image')
                        break
                    toc3 = time.time()
                    print('TIME! Made plan after:', toc3 - tic)
                    print(objects)
                    pick_objects(urnie, objects)
                    toc4 = time.time()
                    print('TIME! Finished picking objects after:', toc4 - tic)
                toc7 = time.time()
                print('Finishing up')
                print('***')
                final_name = img_name + '-final'
                imag_name, imag_path, text_path = get_final_img(urnie, image_name=final_name)
                print(imag_name, imag_path, text_path)
                toc5 = time.time()
                print('TIME! Got final image after:', toc5 - tic)
                print('TIME! Cycle time:', toc7 - tic)
                # objects_final = get_objects_in_img(imag_name, imag_path, text_path, show_imgs=False)
                # toc6 = time.time()
                # print('TIME! Finished running YOLO again after:', toc6 - tic)
                # print(objects_final)
            elif ipt == 'runfs':
                tic = time.time()
                # run for max 3 tests
                for i in range(10):
                    print('Look number', i+1)
                    print('***')
                    img_name, img_path, txt_path = get_initial_img(urnie)
                    toc1 = time.time()
                    print('TIME! Got image after:', toc1 - tic)
                    print(img_name, img_path, txt_path)
                    objects = get_objects_in_img(img_name, img_path, txt_path, show_imgs=False)
                    toc2 = time.time()
                    print('TIME! Finished running YOLO after:', toc2 - tic)
                    if len(objects) == 0:
                        print('Nothing seen in this image')
                        break
                    objects = get_picking_list(objects)
                    if len(objects) == 0:
                        print('Nothing pickable seen in this image')
                        break
                    toc3 = time.time()
                    print('TIME! Made plan after:', toc3 - tic)
                    print(objects)
                    # only pick a single object
                    # TODO: CHange to general case
                    for obj in objects:
                        if obj['box']['tilt'] is not None:
                            pick_objects(urnie, [obj])
                            break
                    toc4 = time.time()
                    print('TIME! Finished picking objects after:', toc4 - tic)
                toc7 = time.time()
                print('Finishing up')
                print('***')
                final_name = img_name + '-final'
                imag_name, imag_path, text_path = get_final_img(urnie, image_name=final_name)
                print(imag_name, imag_path, text_path)
                toc5 = time.time()
                print('TIME! Got final image after:', toc5 - tic)
                print('TIME! Cycle time:', toc7 - tic)
                # objects_final = get_objects_in_img(imag_name, imag_path, text_path, show_imgs=False)
                # toc6 = time.time()
                # print('TIME! Finished running YOLO again after:', toc6 - tic)
                # print(objects_final)
            elif ipt == 'plates':
                img_name, img_path, txt_path = get_initial_img(urnie)
                plates = yf.get_plates(img_name, darknet_path + img_path, show_img=True)
                input('Pick objs?')
                pick_objects(urnie, plates)
            elif ipt == 'p1':
                objects = [{'type': 'plate', 'prob': 70.0, 'box': {'x': 0.453125, 'y': 0.44583333333333336, 'w': 0.45, 'h': 0.6, 'tilt': 'u'}}]
                pick_objects(urnie, objects)
            elif ipt == 'geti':
                img_name = datetime.now().strftime("%Y-%m-%d-%H%M")
                img_name = img_name + '-test'
                urnie.movej(wp.urnie_camj)
                urnie.camerafuncs.capture_image(name=img_name)
            elif ipt == 'frc':
                down=0.05
                urnie.movej(wp.urnie_gripdownperpj)
                urnie.force_move([0, 0, -down], min_time=1, force=20)
                time.sleep(1)
                urnie.home()
            elif ipt == 'cup':
                cup_put_away(urnie)
            elif ipt == 'cam':
                cam_pos_calib(urnie)
            elif ipt == 'pose':
                urnie.movej(wp.urnie_camj)
                # urnie.translatejl_rel([0,-0.1,0.1])
                print(urnie.getj())
            elif ipt == 'o':
                urnie.open_hand()
            elif ipt == 'c':
                urnie.close_hand()
            elif ipt == 'rcalib':
                robotcalib(urnie)
            elif ipt == 'check':
                # checking robot calibration
                urnie.translatejl(wp.robot_xyz[0], vel=0.15)
                urnie.translatejl_rel([0,0,0.02])
                input('next?')
                urnie.translatejl(wp.robot_xyz[1], vel=0.15)
                urnie.translatejl_rel([0, 0, 0.02])
                input('next?')
                urnie.translatejl(wp.robot_xyz[2], vel=0.15)
                urnie.translatejl_rel([0, 0, 0.02])
                input('next?')
                urnie.home()
            else:
                var = int(input("var: "))
                urnie.serial_send(ipt, var, True)
        
    finally:
        print("Goodbye")
        urnie.close()


# KEY FUNCTIONS
def get_initial_img(robot, darknet_path="./darknet/build/darknet/x64/"):
    # take initial image and save in darknet location
    im_name = datetime.now().strftime("%Y-%m-%d-%H%M")

    # going to save an image with this file name
    robot.movej(wp.urnie_camj)
    time.sleep(1)

    os.chdir(darknet_path)
    print('Changed dir to: ', os.getcwd())
    img, img_name, img_path = robot.camerafuncs.capture_image(name=im_name, subfolder='my_imgs')
    os.chdir("../../../..")
    print('Returned to: ', os.getcwd())

    txt_path = img_path[:-3] + 'txt'

    return img_name, img_path, txt_path


def get_final_img(robot, image_name, darknet_path="./darknet/build/darknet/x64/"):
    # take final image and save in darknet location

    # going to save an image with this file name
    robot.movej(wp.urnie_camj)
    time.sleep(1)

    os.chdir(darknet_path)
    print('Changed dir to: ', os.getcwd())
    img, img_name, img_path = robot.camerafuncs.capture_image(name=image_name, subfolder='my_imgs')
    os.chdir("../../../..")
    print('Returned to: ', os.getcwd())

    txt_path = img_path[:-3] + 'txt'

    return img_name, img_path, txt_path


def get_objects_in_img(img_name, img_path, txt_path, tilt=5, darknet_path="./darknet/build/darknet/x64/", show_imgs=True):
    os.chdir(darknet_path)
    print('Changed dir to: ', os.getcwd())
    # print(run_yolo('testimg', 'my_imgs/testimg.jpg'))
    objects = yf.get_lr_tilt(img_name, img_path, txt_path, tilt=tilt, show_imgs=show_imgs)
    os.chdir("../../../..")
    print('Returned to: ', os.getcwd())
    return objects


def get_picking_list(objects):
    # exclude objects not in our set
    print('All Objs:', objects)
    set = ['plate', 'spoon', 'fork', 'knife', 'cup']
    set_objects = [obj for obj in objects if (obj['type'] in set)]
    print('Set Objs:', set_objects)
    if len(set_objects) == 0:
        return []
    else:
        picking = yf.get_pick_order(set_objects)
        ordered_objects = [set_objects[i] for i in picking]
        return ordered_objects


def pick_objects(robot, objects):
    # Takes the list of objects and all details about them to pick them up
    for obj in objects:
        o_type = obj['type']
        print(o_type)
        # ipt3 = input('Press s to skip, enter to continue')
        ipt3 = 'pick'
        if ipt3 == 's':
            pass
        else:
            if o_type == 'cup':
                # print('skip cup')
                o_box = convert_box_to_robot(obj['box'], obj_h=0.12)
                cup_bounded(robot, o_box)
            elif o_type == 'knife' or o_type == 'spoon' or o_type == 'fork':
                o_box = convert_box_to_robot(obj['box'])
                cutlery_bounded(robot, o_box)
            # elif o_type == 'bowl':
            #     bowl_bounded(robot, o_box)
            elif o_type == 'plate':
                o_box = convert_box_to_robot(obj['box'])
                plate_bounded(robot, o_box)
            else:
                print('Cannot deal with object type: ', o_type)

    print('Run through objects in image one time')
    robot.home()

    return True


# METHODS FOR PICKING ITEMS

def cutlery_bounded(robot, box_dict):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']
    tilt = box_dict['tilt']

    l_off = wp.l_gtoTCP - 0.012
    # was -0.01, changed to -0.02 for spoon

    # angle from vertical
    # TODO: Account for case when cutlery is vertical and should have angle of 0
    angle = np.arctan(w / h)  # will be in radians
    print('angle:', angle)

    if tilt == 'l':
        angle = -angle
    elif tilt == 'r':
        angle = angle
    elif tilt == 'u':
        if angle <= 0.28:
            angle = 0
        else:  # Take a punt!
            sign = np.sign(np.random.rand() - 0.5)
            angle = sign * angle
    else:
        print('Cannot pick up object')
        return False

    x_off = l_off * np.cos(angle)
    y_off = l_off * np.sin(angle)
    # print(x_off, y_off)

    x_pick = x - x_off
    y_pick = y + y_off

    from_plate_z = 0.10 - 0.025  # 0.10 - 0.024
    from_surf_h = -0.034
    from_tray_h = -0.032  # just a guess

    robot.open_hand()
    robot.movej(wp.urnie_gripdownperpj)
    # print(robot.getl())
    robot.rotate_rel([0,0,0,0,angle,0])
    robot.translatejl([x_pick, y_pick, 0.20], vel=0.2)
    max_down = 0.15
    robot.force_move([0, 0, -max_down], force=40)
    time.sleep(0.5)
    robot.translatejl_rel([0, 0, 0.003], vel=0.1)  # go back up to avoid shifting surface it is on
    robot.close_hand()
    time.sleep(0.5)
    robot.translatejl_rel([0, 0, 0.2], vel=0.2)

    # # currently instead of putting in dishwasher
    # robot.home()
    # time.sleep(1)
    # robot.open_hand()

    # put in dishwasher
    cutlery_put_away(robot, angle=angle)

    return True


def cup_bounded(robot, box_dict):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']
    tilt = box_dict['tilt']
    # these are all in robot values

    l_off = wp.l_gtoTCP

    # angle from vertical

    angle = np.arctan(w / h)  # will be in radians
    print('angle:', angle)

    if tilt == 'l':
        angle = -angle
    elif tilt == 'r':
        angle = angle
    elif tilt == 'u' or tilt is None:
        angle = 0
    else:
        print('Cannot pick up object')
        return False

    # Set angle = 0 for now, update based on location in image / other objects around
    angle = 0

    x_off = l_off * np.cos(angle)
    y_off = l_off * np.sin(angle)
    # print(x_off, y_off)

    x_cent = x - x_off
    y_cent = y + y_off

    r = 0.5 * min(w,h) * 0.85
    print('radius:', r)

    x_pick = x_cent - r * np.cos(angle)
    y_pick = y_cent + r * np.sin(angle)

    # print(x_cent, y_cent)
    print('picking location:', x_pick, y_pick)

    # testing locations

    time.sleep(0.5)
    robot.open_hand()
    # move TCP to above middle of cup
    robot.movej(wp.urnie_gripdownperpj)
    robot.rotate_rel([0, 0, 0, 0, angle, 0])
    # input('move tcp to above centre')
    # robot.translatejl([x, y, 0.22], vel=0.1)
    # # move fingers to above centre
    # input('move fingers to centre')
    # robot.translatejl([x_cent, y_cent, 0.22], vel=0.1)
    # input('fingers to over rim')
    robot.translatejl([x_pick, y_pick, 0.22], vel=0.15)
    robot.translatejl_rel([0, 0, -0.05])  # go down
    robot.close_hand()
    time.sleep(0.5)
    robot.translatejl_rel([0, 0, 0.1], vel=0.15)

    cup_put_away(robot)
    # # need to move to drop off point
    return True


def plate_bounded(robot, box_dict):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']

    robot.open_hand()
    # Want to position gripper open a certain height above the cup edge
    robot.movej(wp.urnie_homej)
    # Going to pick from the left hand side, could code to pick at angle
    # offset = 0.10
    r = 0.5 * w
    angle = np.pi / 2  # Will make variable
    mid_point = 0.5 * (wp.robot_xyz[0][0] + wp.robot_xyz[1][0])
    print('midpoint: ', mid_point)

    if x > mid_point:
        angle = - np.pi / 2
        offset = 0.08
    else:
        angle = np.pi / 2
        offset = 0.09

    robot.rotate_rel([0, 0, 0, 0, 0, angle])

    x_off = x + np.sin(angle) * (r + offset)
    y_off = y - np.cos(angle) * (r + offset)

    x_edge = x + np.sin(angle) * (r + offset - 0.04)
    y_edge = y - np.cos(angle) * (r + offset - 0.04)
    # -0.035 works for small plastic plate
    # -0.045 for big plastic plate

    robot.translatel([x_off, y_off, 0.15])

    # input('Not going to hit plate?')
    plastic_tray_h = 0.065
    plastic_surf_h = 0.06
    # robot.translatejl([x_off, y_off, plastic_tray_h], vel=0.1)
    max_down = 0.12
    time.sleep(0.5)
    robot.force_move([0, 0, -max_down], force=50)
    time.sleep(0.5)
    robot.translatejl_rel([0, 0, 0.002], vel=0.1)
    slide_h = robot.getl()[2]
    print('picking height:', slide_h)
    robot.translatejl([x_edge, y_edge, slide_h], vel=0.05)
    time.sleep(0.5)
    robot.close_hand()
    time.sleep(0.5)

    plate_put_away(robot)
    # need bring to somewhere
    return True


def bowl_bounded(robot, box_dict):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']
    tilt = box_dict['tilt']
    # these are all in robot values

    l_off = wp.l_gtoTCP

    # angle from vertical
    # TODO: Account for case when cutlery is vertical and should have angle of 0
    angle = np.arctan(w / h)  # will be in radians
    print(angle)

    if tilt == 'l':
        angle = -angle
    elif tilt == 'r':
        angle = angle
    elif tilt == 'u' or tilt is None:
        angle = 0
    else:
        print('Cannot pick up object')
        return False

    x_off = l_off * np.cos(angle)
    y_off = l_off * np.sin(angle)
    print(x_off, y_off)

    x_cent = x - x_off
    y_cent = y + y_off

    r = 0.25 * (w + h) - 0.02

    x_pick = x_cent - r * np.cos(angle)
    y_pick = y_cent + r * np.sin(angle)

    print(x_cent, y_cent)
    print(x_pick, y_pick)

    time.sleep(2)
    robot.open_hand()
    # Want to position gripper open a certain height above the cup edge
    robot.movej(wp.urnie_gripdownperpj)
    robot.rotate_rel([0, 0, 0, 0, angle, 0])
    robot.translatel([x_cent, y_cent, 0.22])
    input('tcp over centre?')
    robot.translatel([x_pick, y_pick, 0.22])
    input('tcp over rim?')
    robot.translatel_rel([0, 0, -0.05])  # go down
    robot.close_hand()
    time.sleep(0.5)
    robot.translatel_rel([0, 0, 0.1], vel=0.1)

    input('continue?')
    robot.home()
    time.sleep(1)
    robot.open_hand()
    # need to move to drop off point
    return True


# PUTTING AWAY FUNCTIONS

def plate_put_away(robot):
    robot.translatel_rel([0, 0, 0.2], vel=0.1)
    robot.movej(wp.urnie_gripdownvhighperpj)
    robot.movej(wp.urnie_platetemp1j)
    robot.movej(wp.urnie_platetemp2j)
    robot.movej(wp.urnie_platetemp3j)
    robot.translatejl_rel([0, 0, -0.1])
    # print(robot.getj())
    robot.open_hand()
    time.sleep(0.5)
    robot.translatejl_rel([0,0,0.05])
    robot.movej(wp.urnie_gripdownvhighperpj)
    return True


def cutlery_put_away(robot, angle):
    # put in dishwasher
    robot.rotate_rel([0, 0, 0, 0, -angle, -np.pi / 2])  # rotate to upright
    robot.translatejl([0.39, -0.41, 0.35])  # move to above holder
    robot.translatel_rel([0, 0, -0.10], vel=0.1)  # move down
    robot.open_hand()
    time.sleep(0.5)
    robot.translatel_rel([0, 0, 0.10], vel=0.1)  # move up
    robot.translatejl_rel([-0.1, 0, 0])
    return True


def cup_put_away(robot):
    # input('continue to drop off?')
    # For put on counter
    # robot.translatejl([0.38, -0.55, 0.20], vel=0.1)
    # robot.translatejl_rel([0, 0, -0.02], vel=0.1)
    # time.sleep(1)
    # robot.open_hand()
    # robot.translatejl_rel([0, 0, 0.05], vel=0.1)
    robot.movej(wp.urnie_gripdownvhighperpj)
    robot.movej(wp.urnie_cuptemp1j)
    robot.movej(wp.urnie_cuptemp2j)
    robot.translatejl_rel([0.03, 0, 0])
    # print(robot.getj())
    robot.translatejl_rel([0, 0, -0.1])
    robot.open_hand()
    robot.translatejl_rel([0, 0, 0.1])
    robot.movej(wp.urnie_cuptemp1j)
    return True


# OTHER FUNCTIONS

def cup_closed_loop1(robot):
    # Will use centre and radius of mug to pick it up
    cup_height = 0.07
    cup_radius = 0.035
    cup_centre = [0.2078, -0.5024]

    # Want to position gripper open a certain height above the cup edge
    robot.translatel([cup_centre[0], cup_centre[1], cup_height + 0.15])
    robot.rotate_rel([0,0,0,np.pi/2,0,0])
    print(robot.getl())
    return True


def cup_closed_loop2(robot):
    # Will use centre and radius of mug to pick it up
    cup_height = 0.07
    cup_radius = 0.03
    cup_centre = [0.2609, -0.4734]  # tcp position corresponding to bottom finger of gripper being just below centre
    time.sleep(2)
    robot.open_hand()
    # Want to position gripper open a certain height above the cup edge
    robot.rotate_rel([0,0,0,np.pi/2,0,0])
    robot.translatel([0.2609, -0.4734, 0.15])
    robot.translatel_rel([0, -cup_radius, 0])
    robot.translatel_rel([0,0,-0.05])  # go down
    robot.close_hand()
    time.sleep(0.5)
    robot.translatel_rel([0,0,0.1], vel=0.1)
    return True


def cutlery_pick(robot):
    # robot.movej(wp.urnie_gripdownhighj)
    # robot.open_hand()
    # robot.rotate_rel([0, 0, 0, 0, np.pi / 2, 0])
    robot.open_hand()
    robot.movej(wp.urnie_gripdownperpj)
    robot.translatel([0, -0.4734, 0.10])
    input()
    robot.translatel_rel([0, 0, -0.024], vel=0.05)  # go down
    time.sleep(0.5)
    # robot.translatel_rel([0, -0.01, 0], vel=0.1)
    robot.close_hand()
    time.sleep(0.5)
    robot.translatel_rel([0, 0, 0.2], vel=0.1)
    # move to put in dishwasher
    robot.rotate_rel([0, 0, 0, 0, 0, -np.pi / 2])
    input()
    # robot.translatel([0.15, -0.2, 0.3], vel=0.05)
    # input()
    robot.translatejl([0.37, -0.4, 0.35])

    robot.translatel_rel([0, 0, -0.1], vel=0.1)
    robot.open_hand()
    time.sleep(0.5)
    robot.translatel_rel([0, 0, 0.1], vel=0.1)
    return True


# CALIBRATION + TRANSFORMATION COMMANDS

def calibration(robot):
    # Returns the coordinates of marker points in camera space and robot space
    # User will be prompted to move the robot, hit keys etc as part of the process
    # robot.camerafuncs.get_marker_coords()
    wp.cam_xy = robot.camerafuncs.get_marker_coords()
    wp.robot_xyz = robot.robotfuncs.get_marker_coords()
    print("[p1, p2, p3]: ", wp.cam_xy, wp.robot_xyz)
    return wp.cam_xy, wp.robot_xyz


# Broken calibration down into subfunctions for testing
def camcalib(robot):
    wp.cam_xy = robot.camerafuncs.get_marker_coords()
    return wp.cam_xy


def robotcalib(robot):
    wp.robot_xyz = robot.robotfuncs.get_marker_coords()
    return wp.robot_xyz


def scale(cam_coords, dist_to_table=0.685, obj_height=0.0):
    # account for projection of tall objects down to table
    z_scale = (dist_to_table - obj_height) / dist_to_table

    # Input a single pair of camera coordinates and get robot coordinates based on values from calibration
    cam_xy = wp.cam_xy
    robot_xyz = wp.robot_xyz
    cx = cam_coords[0]
    cy = cam_coords[1]

    # scale cam coords based on z_scale
    centred_x = cx - wp.img_width / 2
    centred_y = cy - wp.img_height / 2
    scaled_x = centred_x * z_scale
    scaled_y = centred_y * z_scale
    returned_x = wp.img_width / 2 + scaled_x
    returned_y = wp.img_height / 2 + scaled_y
    cx = returned_x
    cy = returned_y

    # Calculating scaling
    x_scale = (robot_xyz[1][0] - robot_xyz[0][0]) / (cam_xy[1][0] - cam_xy[0][0])
    y_scale = (robot_xyz[2][1] - robot_xyz[1][1]) / (cam_xy[2][1] - cam_xy[1][1])

    # Getting x and y coordinates in robot space
    rx = (x_scale * (cx - cam_xy[0][0])) + robot_xyz[0][0]
    ry = (y_scale * (cy - cam_xy[1][1])) + robot_xyz[1][1]

    robot_coords = np.array([rx, ry, wp.surface_avg])

    return robot_coords


# Separated to deal with conflict of single pair and pair of pair (both len = 2), can rewrite
def cam_to_robot(cam_coords_array):
    # Input an array of camera coordinate pairs and get the array of robot coordinates

    # User just must put in as [[... ...] [... ...]]

    # Use np.ravel to check is an array, if len is 2 after ravel put into scale

    robot_coords_array = []
    for i in range(len(cam_coords_array)):
        coord = np.array(cam_coords_array[i])
        entry = scale(coord)
        robot_coords_array.append(entry)

    return np.array(robot_coords_array)


def cam_pos_calib(robot, robocalib=False):
    # move to position where camera will be for gathering images
    robot.home()
    # do some movement to get to good position
    # robot.rotate_rel([0, 0, 0, 0, 0, np.pi / 2])
    # robot.translatejl_rel([0, 0, 0.40], vel=0.1)
    # print(robot.getj())  # put into urnie_camj

    robot.movej(wp.urnie_camj)
    # robot.translatejl_rel([0, 0.10, 0.05])
    # print(robot.getj())
    # find dot coords and their equivalent robot coords
    # calibration(robot)
    camcalib(robot)
    if robocalib:
        robotcalib(robot)
    robot.home()
    return True


def convert_box_to_robot(box_dict, obj_h=0.0):
    x = box_dict['x']
    y = box_dict['y']
    w = box_dict['w']
    h = box_dict['h']

    img_w = wp.img_width
    img_h = wp.img_height

    cam_xy_cent = [x*img_w, y*img_h]
    robot_xy_cent = scale(cam_xy_cent, obj_height=obj_h)

    z_scale = (wp.dist_to_table - obj_h) / wp.dist_to_table

    box_dict['x'] = robot_xy_cent[0]
    box_dict['y'] = robot_xy_cent[1]
    box_dict['w'] = w * img_w * abs(wp.x_scale) * z_scale
    box_dict['h'] = h * img_h * abs(wp.y_scale) * z_scale

    print(box_dict)

    return box_dict


# TEST functions


if __name__ == '__main__':
    main()
