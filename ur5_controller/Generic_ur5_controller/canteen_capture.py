import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf

darknet_path = "./darknet/build/darknet/x64/"
canteen_path = "./darknet/build/darknet/x64/canteen_imgs/"

# going to run through a video and save as tray1, tray2
# tray1_r, tray1_c, tray1_l

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name

cap = cv.VideoCapture('./videos/MAH00257a.MP4')

# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")

tray_count = 67
print(tray_count)

mid_x = 320
mid_y = 180

w = 430
h = 340

p1 = (int(mid_x - w / 2), int(mid_y - h / 2))
p2 = (int(mid_x + w / 2), int(mid_y + h / 2))

# # Read until video is completed
# while (cap.isOpened()):
#     # Capture frame-by-frame
#     ret, frame = cap.read()
#     if ret == True:
#         frameS = cv.resize(frame, (640, 360))
#         cv.circle(frameS, (320, 180), 4, color=(10, 10, 200))
#         cv.rectangle(frameS, p1, p2, color=(10, 10, 200))
#         cv.imshow('Frame', frameS)
#         # also add a rough rectangle to show where centre roughly is
#
#         key = cv.waitKey(0)
#         while key not in [ord('q'), ord('p'), ord('c'), ord('r'), ord('l'), ord('u'), ord('d')]:
#             key = cv.waitKey(0)
#         if key == ord('q'):
#             break
#         elif key == ord('u'):
#             # increment tray count
#             tray_count += 1
#             print(tray_count)
#         elif key == ord('d'):
#             # decrement tray count
#             tray_count -= 1
#             print(tray_count)
#         elif key == ord('r'):
#             img_name = './videos/' + 'tray' + str(tray_count) + '_r' + '.jpg'
#             cv.imwrite(img_name, frame)
#         elif key == ord('c'):
#             img_name = './videos/' + 'tray' + str(tray_count) + '_c' + '.jpg'
#             cv.imwrite(img_name, frame)
#         elif key == ord('l'):
#             img_name = './videos/' + 'tray' + str(tray_count) + '_l' + '.jpg'
#             cv.imwrite(img_name, frame)
#
#         # # Press Q on keyboard to  exit
#         # if cv.waitKey(25) & 0xFF == ord('q'):
#         #     break
#
#     # Break the loop
#     else:
#         break
#
# cap.release()
#
# cv.destroyAllWindows()

frame_count = 0
single_frames = False

# Read until video is completed
while (cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:
        frame_count += 1
        frameS = cv.resize(frame, (640, 360))
        cv.circle(frameS, (320, 180), 4, color=(10, 10, 200))
        cv.rectangle(frameS, p1, p2, color=(10, 10, 200))
        cv.putText(frameS, str(frame_count / 50), (50, 50), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))
        # also add a rough rectangle to show where centre roughly is

        if single_frames == False:
            if frame_count % 50 == 0:
                cv.imshow('Frame', frameS)

                key = cv.waitKey(0)

                while key not in [ord('q'), ord('p'), ord('s')]:
                    key = cv.waitKey(0)
                if key == ord('q'):
                    break
                elif key == ord('s'):
                    print('switching to single frame mode')
                    single_frames = True
        else:
            cv.imshow('Frame', frameS)

            key = cv.waitKey(0)
            while key not in [ord('q'), ord('p'), ord('c'), ord('r'), ord('l'), ord('u'), ord('d'), ord('s')]:
                key = cv.waitKey(0)
            if key == ord('q'):
                break
            elif key == ord('u'):
                # increment tray count
                tray_count += 1
                print(tray_count)
            elif key == ord('d'):
                # decrement tray count
                tray_count -= 1
                print(tray_count)
            elif key == ord('r'):
                img_name = './videos/' + 'tray' + str(tray_count) + '_r' + '.jpg'
                cv.imwrite(img_name, frame)
                print('saved r')
            elif key == ord('c'):
                img_name = './videos/' + 'tray' + str(tray_count) + '_c' + '.jpg'
                cv.imwrite(img_name, frame)
                print('saved c')
            elif key == ord('l'):
                img_name = './videos/' + 'tray' + str(tray_count) + '_l' + '.jpg'
                cv.imwrite(img_name, frame)
                print('saved l')
            elif key == ord('s'):
                print('switching to skip frame mode')
                single_frames = False

        # # Press Q on keyboard to  exit
        # if cv.waitKey(25) & 0xFF == ord('q'):
        #     break

    # Break the loop
    else:
        break

cap.release()

cv.destroyAllWindows()
