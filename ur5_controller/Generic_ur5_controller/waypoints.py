from math import pi
import numpy as np

# home position
# urnie_homej = [1.5722, -1.6675, 2.3422, 4.03652, -1.5716, 0.786752]
urnie_homej = [1.5722, -1.6675, 2.3422, -2.2467, -1.5716, 0.786752]
# dylan home pos
# urnie_homej = [-0.82030475, -1.83259571, -1.6406095, 3.4906585, -5.48033385, 0.75049158]

# urnie_awayj = [0.0000, -1.6675, 2.3422, 4.03652, -1.5716, 0.786752]
urnie_awayj = [0.0000, -1.6675, 2.3422, -2.24667, -1.5716, 0.786752]
urnie_squeezej = [-0.306607, -1.52971, 2.42763, 3.81537, -1.56747, 0.0]
# urnie_camj = [1.57205, -1.5308, 0.645033, 5.59668, -1.56851, -0.785378]
urnie_camj = [1.57205, -1.5308, 0.645033, -0.68651, -1.56851, -0.785378]
urnie_oldcamj = [1.57424, -1.93488, 1.3047, 5.3414, -1.56935, 2.35817]  # 70 cm away from surface
urnie_lowcamj = [1.57424, -1.93488, 1.3047, 5.3414, -1.56935, 2.35817]  # 35 cm away from surface

# urnie_gripdownj = [1.57218, -1.66757, 2.3422, 5.6072, -1.57165, 0.786812]
# urnie_gripdownhighj = [1.57126, -1.29238, 1.77061, 5.80347, -1.57159, 0.785958]
# urnie_gripdownvhighj = [1.57162, -1.39352, 1.49602, 6.17921, -1.57161, 0.784997]
urnie_gripdownj = [1.57218, -1.66757, 2.3422, -0.67600, -1.57165, 0.786812]
urnie_gripdownhighj = [1.57126, -1.29238, 1.77061, -0.47972, -1.57159, 0.785958]
urnie_gripdownvhighj = [1.57162, -1.39352, 1.49602, -0.10398, -1.57161, 0.784997]

# urnie_gripdownperpj = [1.57118, -1.29245, 1.77079, 5.80333, -0.000801865, 0.785922]
# urnie_gripdownvhighperpj = [1.57158, -1.3936, 1.49606, 6.17921, -0.000860993, 0.785021]
urnie_gripdownperpj = [1.57118, -1.29245, 1.77079, -0.47986, -0.000801865, 0.785922]
urnie_gripdownvhighperpj = [1.57158, -1.3936, 1.49606, -0.10398, -0.000860993, 0.785021]

# urnie_platetempj = [1.74298, -1.1626, 2.09558, 3.75306, -1.34041, 2.58884]
# # get over lip of dishwasher
# urnie_platetemp1j = [1.92083, -1.67183, 1.49575, 5.95566, 0.307239, 0.785406]
# # be above slot
# urnie_platetemp2j = [2.27011, -1.52362, 1.45976, 5.65427, 0.539205, 0.78555]
# # be in slot
# urnie_platetemp3j = [2.26965, -1.42715, 1.4713, 6.08517, 0.805854, 0.78549]

urnie_platetempj = [1.74298, -1.1626, 2.09558, -2.53013, -1.34041, 2.58884]
# get over lip of dishwasher
urnie_platetemp1j = [1.92083, -1.67183, 1.49575, -0.32753, 0.307239, 0.785406]
# be above slot
urnie_platetemp2j = [2.27011, -1.52362, 1.45976, -0.62892, 0.539205, 0.78555]
# be in slot
urnie_platetemp3j = [2.26965, -1.42715, 1.4713, -0.19802, 0.805854, 0.78549]

# urnie_cuptemp1j = [1.94192, -1.59549, 1.66358, 6.21602, 0.368989, 0.783256]
# urnie_cuptemp2j = [2.11918, -1.59881, 1.71804, 6.16512, 0.546083, 0.783436]
urnie_cuptemp1j = [1.94192, -1.59549, 1.66358, -0.06717, 0.368989, 0.783256]
urnie_cuptemp2j = [2.11918, -1.59881, 1.71804, -0.11807, 0.546083, 0.783436]
urnie_cuptemp3j = [2.18373, -1.54348, 1.66276, -0.11798, 0.610552, 0.783688]

# for squeezing - SPONGE
down_squeezej = [-0.306967, -1.30823, 2.48213, 3.53893, -1.57045, 0.000635164]
right_squeezej = [-0.335286, -1.29464, 2.49209, 3.51541, -1.47047, -0.02755]
left_squeezej = [-0.279126, -1.28581, 2.47637, 3.52198, -1.67079, 0.028497]
front_squeezej = [-0.306739, -1.24571, 2.43407, 3.62321, -1.57123, 0.00133025]
back_squeezej = [-0.307578, -1.25973, 2.53915, 3.33013, -1.57159, 0.0002277]

# for squeezing - CLOTH
# down_squeezej = [-0.306991, -1.27411, 2.48573, 3.49958, -1.57077, 0.000719053]
# right_squeezej = [-0.335238, -1.26751, 2.49545, 3.4759, -1.47074, -0.0274661]
# left_squeezej = [-0.279342, -1.25414, 2.47939, 3.48326, -1.67153, 0.0283052]

# for squeezing - TOWEL
# down_squeezej = [-0.307015, -1.31003, 2.4815, 3.54011, -1.5702, 0.000587227]
# right_squeezej = [-0.335262, -1.29652, 2.49139, 3.51662, -1.47002, -0.0276941]
# left_squeezej = [-0.279139, -1.28781, 2.47563, 3.52302, -1.67065, 0.0283532]

# gripper tool centre points (tcp)
pincher_tcp = [0, 0, 0.142, 0, 0, 0]
bluetack_tcp = [0, 0, -0.215, 0, 0, 0]

# points recorded from calibration, will be updated in main program
cam_xy = np.array([[27.233869552612305, 79.63788604736328], [587.9902954101562, 79.28242492675781], [597.6777954101562, 428.92620849609375]])
robot_xyz = np.array([[0.345167, -0.541933, 0.0652391], [-0.120665, -0.535317, 0.0597749],
                      [-0.131924, -0.2413, 0.0541688]])

x_scale = (robot_xyz[1][0] - robot_xyz[0][0]) / (cam_xy[1][0] - cam_xy[0][0])
y_scale = (robot_xyz[2][1] - robot_xyz[1][1]) / (cam_xy[2][1] - cam_xy[1][1])

# used to set height for sweep_contour
surface_avg = 0.08

# Creating some points in camera space for the robot to go to
points_c = np.array([[500, 300], [250, 250], [200, 350]])
points_r = np.array([[0.19, -0.62, 0.09], [0.19, -0.40, 0.09], [-0.30, -0.40, 0.09]])

dump_point = np.array(robot_xyz[2])

# Coordinates for cropping the frame
c_thresh = 20
r_thresh = 5

p1_r = int(np.floor(cam_xy[0][1]) + r_thresh - 3)
p1_col = int(np.ceil(cam_xy[0][0]) + c_thresh)
p2_col = int(np.floor(cam_xy[1][0]) - c_thresh - 10)
p3_r = int(np.ceil(cam_xy[2][1]) - r_thresh + 15)

test_num = 0

pixels_for_vol1 = 8700.0

gripdown_surface_height = 0.065

img_width = 640
img_height = 480

l_gtoTCP = 0.06  # distance from gripper finger to TCP

dist_to_table = 0.685
