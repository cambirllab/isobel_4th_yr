import cv2
import numpy as np
import waypoints as wp
from matplotlib import pyplot as plt
import time


# this class is now tied to the robot
# can I change this
class camerafuncs():
    def __init__(self, robot):
        self.robot = robot

        print("Opening video device...")
        self.cap = cv2.VideoCapture(1)
        if self.cap.isOpened():
            print("Video device has finally opened")

    def get_marker_coords(self):

        print("Press Esc when the 3 points are circled in red")

        while (1):

            # Take each frame
            _, frame = self.cap.read()

            # frame = cv2.flip(frame, -1) might be needed depending on camera mount

            # Make grayscale
            im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Setup SimpleBlobDetector parameters, will be optimised for detecting the calibration points
            params = cv2.SimpleBlobDetector_Params()

            # Change thresholds
            params.minThreshold = 10
            params.maxThreshold = 200

            # Filter by Colour
            params.filterByColor = True
            blobColor = 50

            # Filter by Area.
            params.filterByArea = True
            params.minArea = 150
            params.maxArea = 300

            # Filter by Circularity
            params.filterByCircularity = True
            params.minCircularity = 0.8  # 0.9 not detecting rectangular lego blocks but picking up tape circles

            # Filter by Convexity
            params.filterByConvexity = False
            params.minConvexity = 0.87

            # Filter by Inertia
            params.filterByInertia = False
            params.minInertiaRatio = 0.01

            # Create a detector with the parameters
            detector = cv2.SimpleBlobDetector_create(params)

            # Detect blobs.
            keypoints = detector.detect(im)

            # Draw detected blobs as red circles.
            # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
            im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                                  cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            # Converting to x y coordinates (camera space)
            points2f = cv2.KeyPoint_convert(keypoints)

            # Show keypoints
            cv2.imshow('frame', frame)
            cv2.imshow('keypoints', im_with_keypoints)

            k = cv2.waitKey(5) & 0xFF
            if k == 27 and len(points2f) == 3:

                # Assigning coordinates of keypoints to calibration markers
                # p1 = top left
                # p2 = top right
                # p3 = bottom right

                # Looping to find point with smallest x value (p1) and point with largest y value (p3)
                initial_x = points2f[0][0]
                initial_y = 0  # points2f[0][1]
                for i in range(3):
                    if points2f[i][0] <= initial_x:
                        p1_idx = i
                        initial_x = points2f[i][0]
                    if points2f[i][1] >= initial_y:
                        p3_idx = i
                        initial_y = points2f[i][1]
                p1 = points2f[p1_idx]
                p3 = points2f[p3_idx]
                p2_idx = (3 - (p1_idx + p3_idx))  # Gets remaining index out of 0, 1, 2
                p2 = points2f[p2_idx]

                ordered_points = np.array([p1, p2, p3])

                print(keypoints)
                print(points2f)
                print(p1_idx, p3_idx)
                print("Coordinates of p1: {}".format(p1))
                print("Coordinates of p2: {}".format(p2))
                print("Coordinates of p3: {}".format(p3))
                print(ordered_points)
                print("[[{0}, {1}], [{2}, {3}], [{4}, {5}]]".format(p1[0], p1[1], p2[0], p2[1], p3[0], p3[1]))
                break

        cv2.destroyAllWindows()
        return ordered_points

    def capture_image(self, name, subfolder='.'):
        time.sleep(1)
        _, frame = self.cap.read()
        img = frame
        img_path = subfolder + '/' + name + '.jpg'
        cv2.imwrite(img_path, img)
        img_name = name
        return img, img_name, img_path

    def capture_close_image(self, name):
        self.robot.movej(wp.urnie_lowcamj)
        time.sleep(1)
        _, frame = self.cap.read()
        img = frame
        img_path = './' + name + '.jpg'
        cv2.imwrite(img_path, img)
        return img, img_path

    def cluster_spots(contours, areas, centroids):
        # Something to group any small spots of liquid
        spots_areas = []
        spots_contours = []
        spots_centroids = []
        spots_x = []
        spots_y = []

        for i in range(len(areas)):
            if areas[i] <= 500:
                spots_areas.append(areas[i])
                spots_contours.append(contours[i])
                spots_centroids.append(centroids[i])
                spots_x.append(centroids[i][0])
                spots_y.append(centroids[i][1])

        z = np.vstack((spots_y, spots_x))
        print(z)
        z = z.reshape((len(spots_x), 2))
        # convert to np.float32
        z = np.float32(z)
        print(z)

        # define criteria and apply kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 20, 1.0)
        ret, label, center = cv2.kmeans(z, 2, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

        # Now separate the data, Note the flatten()
        a = z[label.ravel() == 0]
        b = z[label.ravel() == 1]
        print(a)
        print(b)

        # Plot the data
        plt.scatter(a[:, 0], a[:, 1])
        plt.scatter(b[:, 0], b[:, 1], c='r')
        plt.scatter(center[:, 0], center[:, 1], s=80, c='y', marker='s')
        plt.axis('equal')
        plt.show()

    def detect_liquid(self, before):
        # Code to find regions of continuous mess, likely liquid
        # Returns contours with non zero area, areas, perimeters, centroids

        print("Will show contours in pink, hulls in yellow")

        if before:
            min_count = 200
        else:
            min_count = 350

        # setting up potential averaging of frames
        while(1):
            pet, frame1 = self.cap.read()
            if pet:
                frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
                frame2 = frame1
                frame3 = frame2
                frame4 = frame3
                break

        count = 0
        stored_contours = []

        if before:
            cont_vid_outpath = './vidcontoursbefore.avi'
            orig_vid_outpath = './vidoriginalbefore.avi'
        else:
            cont_vid_outpath = './vidcontoursafter.avi'
            orig_vid_outpath = './vidoriginalafter.avi'

        # setting up video writers
        cont_vid = cv2.VideoWriter(cont_vid_outpath, cv2.VideoWriter_fourcc(*'MJPG'), 20, (258, 204))
        orig_vid = cv2.VideoWriter(orig_vid_outpath, cv2.VideoWriter_fourcc(*'MJPG'), 20, (258, 204))

        flac = time.time()

        while (1):

            # Take each frame
            ret, frame = self.cap.read()

            if ret:
                count += 1
                # averaging to remove noise perhaps
                frame4 = frame3
                frame3 = frame2
                frame2 = frame1
                frame1 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Crop frame to exclude the stickers, go from just right of p1 to just left of p2 and down to level w p3
                p1_r = wp.p1_r
                p1_col = wp.p1_col
                p2_col = wp.p2_col
                p3_r = wp.p3_r

                im1 = frame1[p1_r:p3_r, p1_col:p2_col]
                im2 = frame2[p1_r:p3_r, p1_col:p2_col]
                im3 = frame3[p1_r:p3_r, p1_col:p2_col]
                im4 = frame4[p1_r:p3_r, p1_col:p2_col]

                ims = [im1, im2, im3, im4]

                # trying clahe
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
                cls = [clahe.apply(im) for im in ims]

                # averaging clahes
                avg1 = cv2.addWeighted(cls[0], 0.5, cls[1], 0.5, 0)
                avg2 = cv2.addWeighted(cls[2], 0.5, cls[3], 0.5, 0)
                avg3 = cv2.addWeighted(avg1, 0.5, avg2, 0.5, 0)

                cv2.imshow('avg clahe', avg3)
                imag = avg3

                kernel = np.ones((5, 5), np.uint8)

                gauss_mask = camerafuncs.get_liquid_mask(imag)
                closed_gauss_mask = cv2.morphologyEx(gauss_mask, cv2.MORPH_CLOSE, kernel, iterations=2)
                # closed_gauss_mask = cv2.morphologyEx(closed_gauss_mask1, cv2.MORPH_CLOSE, kernel)

                cv2.imshow('closed gauss mask', closed_gauss_mask)
                cv2.imshow('gauss mask', gauss_mask)

                thresh = closed_gauss_mask
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                img = cv2.cvtColor(im1, cv2.COLOR_GRAY2BGR)

                # removing any contours with area smaller than 5
                my_contours = []
                my_hulls = []
                for cont in contours:
                    cont_area = cv2.contourArea(cont)
                    if cont_area >= 40.0:  # might need to be reduced to pick up droplets
                        hull = cv2.convexHull(cont)
                        hull_area = cv2.contourArea(hull)
                        # checking solidity, using hulls to replace ones with broken edge
                        if float(cont_area) / hull_area >= 0.5:
                            my_contours.append(cont)
                        else:
                            my_contours.append(cont)
                            # my_hulls.append(hull)
                # combining contours and hulls for the final contour list, drawing them in different colours
                # contours = my_contours + my_hulls
                contours = my_contours

                cv2.drawContours(img, my_contours, -1, (255, 0, 255), 2)
                cv2.drawContours(img, my_hulls, -1, (0, 255, 255), 2)

                # to be deleted
                # colour_cropped = frame[p1_r:p3_r, p1_col:p2_col]

                cv2.imshow('frame', frame)
                cv2.imshow('cropped w contours', img)

                change = False
                total_area = 0

                # Selecting the biggest contour by area observed so far
                if len(contours) >= 1:
                    areas = [cv2.contourArea(cnt) for cnt in contours]
                    total_area = np.sum(areas)
                    if len(stored_contours) == 0:
                        change = True
                    else:
                        stored_areas = [cv2.contourArea(cnt) for cnt in stored_contours]
                        total_stored_area = np.sum(stored_areas)
                        if total_area >= total_stored_area:
                            change = True

                # only changing contour image and bw image recorded if change has occurred
                if change:
                    stored_contours = contours
                    print(count, 'CHANGE', total_area)
                    cont_img = img
                    bw_img = avg3

                    # code to record images for vision graphic
                    # cv2.imwrite('./vis-entireframe.jpg', frame)
                    # cv2.imwrite('./vis-colourimage.jpg', colour_cropped)
                    # cv2.imwrite('./vis-claheimage.jpg', avg3)
                    # cv2.imwrite('./vis-bandwwconts.jpg', img)
                    # cv2.imwrite('./vis-maskopen.jpg', gauss_mask)
                    # cv2.imwrite('./vis-maskclosed.jpg', closed_gauss_mask)

                contours = stored_contours

                # writing frames to videos
                cont_vid.write(img)
                colour_cropped = frame[p1_r:p3_r, p1_col:p2_col]
                orig_vid.write(colour_cropped)

                k = cv2.waitKey(5) & 0xFF
                if count > min_count:
                    flic = time.time()
                    print('OBS (s) = ', flic - flac)
                    if len(contours) >= 1:
                        areas = [cv2.contourArea(cnt) for cnt in contours]
                        perimeters = [cv2.arcLength(cnt, True) for cnt in contours]
                        print('a, p', areas, perimeters)

                        # Getting centroids in uncropped camera coordinates
                        centroids = []
                        for i in range(len(contours)):
                            moms = cv2.moments(contours[i])
                            cx = moms['m10'] / moms['m00']
                            cy = moms['m01'] / moms['m00']
                            centroids.append([cx + p1_col, cy + p1_r])
                        print('centroids', centroids)

                        # attempt to do clustering on small droplets of liquid, not working yet
                        # camerafuncs.cluster_spots(uncropped_contours, areas, centroids)

                        cv2.destroyAllWindows()
                        return contours, areas, perimeters, centroids, cont_img, bw_img
                    else:
                        cont_img = img
                        bw_img = avg3
                        cv2.destroyAllWindows()
                        return [], [], [], [], cont_img, bw_img  # or return None

                    break

    def get_cont_as_path(self, contour):
        # Contour comes into function in form [ [[... ...]] [[... ...]] ]
        # And goes out as [ [... ...] [... ...] ] in uncropped camera coords
        # This is then in right form to be converted to robot coords etc
        cont = np.array([np.ravel(pair) for pair in contour])
        uncropped_cont = cont + [wp.p1_col, wp.p1_r]
        return uncropped_cont

    def get_major_axis(self, contour):
        # get major axis of contour, return the angle of major axis
        rect = cv2.minAreaRect(contour)
        angle = rect[2]
        if rect[1][0] > rect[1][1]:
            angle += 90
        return angle

    def get_inertia(self, contour):
        # get inertia of contour, to record as a parameter
        return

    def get_liquid_mask(image):
        if len(image.shape) == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        img = cv2.medianBlur(image, 7)

        ret, th1 = cv2.threshold(img, 180, 255, cv2.THRESH_BINARY)
        th2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 17, 2)
        th3 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 37, 4)

        titles = ['Original Image', 'Global Thresholding (v = 180)',
                  'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
        images = [img, th1, th2, th3]

        # for i in range(4):
        #     plt.subplot(2, 2, i + 1), plt.imshow(images[i], 'gray')
        #     plt.title(titles[i])
        #     plt.xticks([]), plt.yticks([])
        # plt.show()
        return th3

    def get_contour_videos(self, before):
        # Code to find regions of continuous mess, likely liquid
        # Returns contours with non zero area, areas, perimeters, centroids

        print("Will show contours in pink, hulls in yellow")

        if before:
            min_count = 200
        else:
            min_count = 350

        # setting up potential averaging of frames
        while(1):
            pet, frame1 = self.cap.read()
            if pet:
                frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
                frame2 = frame1
                frame3 = frame2
                frame4 = frame3
                break

        count = 0
        stored_contours = []

        # setting up video writers
        cont_vid = cv2.VideoWriter('./videocontours.avi', cv2.VideoWriter_fourcc(*'XVID'), 20, (258, 204))
        orig_vid = cv2.VideoWriter('./videooriginal.avi', cv2.VideoWriter_fourcc(*'XVID'), 20, (258, 204))

        while (1):

            # Take each frame
            ret, frame = self.cap.read()

            if ret:
                count += 1
                # averaging to remove noise perhaps
                frame4 = frame3
                frame3 = frame2
                frame2 = frame1
                frame1 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Crop frame to exclude the stickers, go from just right of p1 to just left of p2 and down to level w p3
                p1_r = wp.p1_r
                p1_col = wp.p1_col
                p2_col = wp.p2_col
                p3_r = wp.p3_r

                im1 = frame1[p1_r:p3_r, p1_col:p2_col]
                im2 = frame2[p1_r:p3_r, p1_col:p2_col]
                im3 = frame3[p1_r:p3_r, p1_col:p2_col]
                im4 = frame4[p1_r:p3_r, p1_col:p2_col]

                ims = [im1, im2, im3, im4]

                # trying clahe
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
                cls = [clahe.apply(im) for im in ims]

                # averaging clahes
                avg1 = cv2.addWeighted(cls[0], 0.5, cls[1], 0.5, 0)
                avg2 = cv2.addWeighted(cls[2], 0.5, cls[3], 0.5, 0)
                avg3 = cv2.addWeighted(avg1, 0.5, avg2, 0.5, 0)

                cv2.imshow('avg clahe', avg3)
                imag = avg3

                kernel = np.ones((5, 5), np.uint8)

                gauss_mask = camerafuncs.get_liquid_mask(imag)
                closed_gauss_mask = cv2.morphologyEx(gauss_mask, cv2.MORPH_CLOSE, kernel, iterations=2)
                # closed_gauss_mask = cv2.morphologyEx(closed_gauss_mask1, cv2.MORPH_CLOSE, kernel)

                cv2.imshow('closed gauss mask', closed_gauss_mask)
                cv2.imshow('gauss mask', gauss_mask)

                thresh = closed_gauss_mask
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                img = cv2.cvtColor(im1, cv2.COLOR_GRAY2BGR)

                # removing any contours with area smaller than 5
                my_contours = []
                my_hulls = []
                for cont in contours:
                    cont_area = cv2.contourArea(cont)
                    if cont_area >= 40.0:  # might need to be reduced to pick up droplets
                        hull = cv2.convexHull(cont)
                        hull_area = cv2.contourArea(hull)
                        # checking solidity, using hulls to replace ones with broken edge
                        if float(cont_area) / hull_area >= 0.5:
                            my_contours.append(cont)
                        else:
                            my_contours.append(cont)
                            # my_hulls.append(hull)
                # combining contours and hulls for the final contour list, drawing them in different colours
                # contours = my_contours + my_hulls
                contours = my_contours

                cv2.drawContours(img, my_contours, -1, (255, 0, 255), 2)
                cv2.drawContours(img, my_hulls, -1, (0, 255, 255), 2)

                cv2.imshow('frame', frame)
                cv2.imshow('cropped w contours', img)

                change = False
                total_area = 0

                # Selecting the biggest contour by area observed so far
                if len(contours) >= 1:
                    areas = [cv2.contourArea(cnt) for cnt in contours]
                    total_area = np.sum(areas)
                    if len(stored_contours) == 0:
                        change = True
                    else:
                        stored_areas = [cv2.contourArea(cnt) for cnt in stored_contours]
                        total_stored_area = np.sum(stored_areas)
                        if total_area >= total_stored_area:
                            change = True

                # only changing contour image and bw image recorded if change has occurred
                if change:
                    stored_contours = contours
                    print(count, 'CHANGE', total_area)
                    cont_img = img
                    bw_img = avg3

                    # code to record images for vision graphic
                    # cv2.imwrite('./vis-entireframe.jpg', frame)
                    # cv2.imwrite('./vis-colourimage.jpg', colour_cropped)
                    # cv2.imwrite('./vis-claheimage.jpg', avg3)
                    # cv2.imwrite('./vis-bandwwconts.jpg', img)
                    # cv2.imwrite('./vis-maskopen.jpg', gauss_mask)
                    # cv2.imwrite('./vis-maskclosed.jpg', closed_gauss_mask)

                contours = stored_contours

                # writing frames to videos
                cont_vid.write(img)
                colour_cropped = frame[p1_r:p3_r, p1_col:p2_col]
                orig_vid.write(colour_cropped)

                k = cv2.waitKey(5) & 0xFF
                if count > min_count and k == 27:
                    if len(contours) >= 1:
                        areas = [cv2.contourArea(cnt) for cnt in contours]
                        perimeters = [cv2.arcLength(cnt, True) for cnt in contours]
                        print('a, p', areas, perimeters)

                        # Getting centroids in uncropped camera coordinates
                        centroids = []
                        for i in range(len(contours)):
                            moms = cv2.moments(contours[i])
                            cx = moms['m10'] / moms['m00']
                            cy = moms['m01'] / moms['m00']
                            centroids.append([cx + p1_col, cy + p1_r])
                        print('centroids', centroids)

                        cont_vid.release()
                        orig_vid.release()
                        cv2.destroyAllWindows()
                        return contours, areas, perimeters, centroids, cont_img, bw_img
                    else:
                        cont_img = img
                        bw_img = avg3
                        cont_vid.release()
                        orig_vid.release()
                        cv2.destroyAllWindows()
                        return [], [], [], [], cont_img, bw_img  # or return None

                    break

# Unused functions, retained because aspects may be useful later

    def unused_detect_liquid(self):
        # Code to find regions of continuous mess, likely liquid
        # Returns contours with non zero area, areas, perimeters, centroids

        print("Press Esc when contours shown in green")

        # setting up potential averaging of frames
        _, frame1 = self.cap.read()
        frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
        frame2 = frame1
        frame3 = frame2

        count = 0

        while (1):

            # Take each frame
            ret, frame = self.cap.read()

            if ret:
                count += 1
                # averaging to remove noise perhaps
                frame3 = frame2
                frame2 = frame1
                frame1 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Crop frame to exclude the stickers, go from just right of p1 to just left of p2 and down to level w p3

                c_thresh = 20
                r_thresh = 10

                p1_r = int(np.floor(wp.cam_xy[0][1]) - r_thresh)
                p1_col = int(np.ceil(wp.cam_xy[0][0]) + c_thresh)
                p2_col = int(np.floor(wp.cam_xy[1][0]) - c_thresh)
                p3_r = int(np.ceil(wp.cam_xy[2][1]) + r_thresh)

                im1 = frame1[p1_r:p3_r, p1_col:p2_col]
                im2 = frame2[p1_r:p3_r, p1_col:p2_col]
                im3 = frame3[p1_r:p3_r, p1_col:p2_col]

                ims = [im1, im2, im3]

                kernel = np.ones((7, 7), np.uint8)

                masks = ims

                for i in range(len(ims)):
                    gauss_mask = camerafuncs.get_liquid_mask(ims[i])
                    closed_gauss_mask = cv2.morphologyEx(gauss_mask, cv2.MORPH_CLOSE, kernel)
                    masks[i] = closed_gauss_mask

                # when count is small, do average
                if count <= 10:
                    avg1 = cv2.bitwise_and(masks[0], masks[1], masks[0])
                    avg2 = cv2.bitwise_and(avg1, masks[2], avg1)
                # when count is medium, compound with previous averages
                elif count <= 150:
                    avg2 = cv2.bitwise_and(masks[0], previous_avg, masks[0])
                # when count is big, stop compounding perhaps
                else:
                    avg2 = previous_avg

                previous_avg = avg2

                cv2.imshow('anded masks', avg2)

                thresh = avg2
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                img = cv2.cvtColor(im1, cv2.COLOR_GRAY2BGR)

                # removing any contours with area smaller than 5
                my_contours = []
                for cont in contours:
                    if cv2.contourArea(cont) >= 5.0:  # might need to be reduced to pick up droplets
                        my_contours.append(cont)
                contours = my_contours

                cv2.drawContours(img, contours, -1, (0, 255, 0), 3)

                cv2.imshow('frame', frame)
                cv2.imshow('cropped w contours', img)

                k = cv2.waitKey(5) & 0xFF
                if count > 200 and len(contours) >= 1 and k == 27:

                    areas = [cv2.contourArea(cnt) for cnt in contours]
                    perimeters = [cv2.arcLength(cnt, True) for cnt in contours]
                    moments = [cv2.moments(cnt) for cnt in contours]
                    print('a, p', areas, perimeters)

                    # Getting centroids in uncropped camera coordinates
                    centroids = []
                    for i in range(len(contours)):
                        moms = cv2.moments(contours[i])
                        cx = moms['m10'] / moms['m00']
                        cy = moms['m01'] / moms['m00']
                        centroids.append([cx + p1_col, cy + p1_r])
                    print('centroids', centroids)

                    uncropped_contours = []

                    for i in range(len(contours)):
                        cont = camerafuncs.get_cont_as_path(contours[i])
                        uncropped_cont = cont + [p1_col, p1_r]
                        uncropped_contours.append(uncropped_cont)
                    uncropped_contours = np.array(uncropped_contours)

                    # attempt to do clustering on small droplets of liquid, not working yet
                    # camerafuncs.cluster_spots(uncropped_contours, areas, centroids)

                    break

        cv2.destroyAllWindows()
        return uncropped_contours, areas, perimeters, centroids

    def unused_get_liquid_mask(image):
        if len(image.shape) == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        img = cv2.medianBlur(image, 5)

        # trying clahe
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        cl1 = clahe.apply(img)
        cv2.imshow('clahe', cl1)
        img = cl1

        ret, th1 = cv2.threshold(img, 180, 255, cv2.THRESH_BINARY)
        th2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 17, 2)
        th3 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 37, 2)

        titles = ['Original Image', 'Global Thresholding (v = 180)',
                  'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
        images = [img, th1, th2, th3]

        # for i in range(4):
        #     plt.subplot(2, 2, i + 1), plt.imshow(images[i], 'gray')
        #     plt.title(titles[i])
        #     plt.xticks([]), plt.yticks([])
        # plt.show()
        return th3

    def get_big_blob_coords(self):
        # Some code to find the biggest blob detected and return its coordinates in camera space

        print("Press Esc when points are circled in red")

        while (1):

            # Take each frame
            _, frame = self.cap.read()

            # frame = cv2.flip(frame, -1) might be needed depending on camera mount

            # Make grayscale
            im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Setup SimpleBlobDetector parameters, will be optimised for detecting the calibration points
            params = cv2.SimpleBlobDetector_Params()

            # Change thresholds
            params.minThreshold = 10
            params.maxThreshold = 200

            # Filter by Colour
            params.filterByColor = True
            blobColor = 10

            # Filter by Area.
            params.filterByArea = True
            params.minArea = 100
            params.maxArea = 900

            # Filter by Circularity
            params.filterByCircularity = True
            params.minCircularity = 0.5  # 0.9 not detecting rectangular lego blocks but picking up tape circles

            # Filter by Convexity
            params.filterByConvexity = False
            params.minConvexity = 0.87

            # Filter by Inertia
            params.filterByInertia = True
            params.minInertiaRatio = 0.1

            # Create a detector with the parameters
            detector = cv2.SimpleBlobDetector_create(params)

            # Detect blobs.
            keypoints = detector.detect(im)

            # Draw detected blobs as red circles.
            # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
            im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                                  cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            # Show keypoints
            cv2.imshow('frame', frame)
            cv2.imshow('keypoints', im_with_keypoints)

            k = cv2.waitKey(5) & 0xFF
            if k == 27 and len(keypoints) >= 1:
                # Find biggest keypoint
                keypoints_size = [keypoint.size for keypoint in keypoints]
                biggest_keypoint = keypoints[np.argmax(keypoints_size)]
                biggest_keypoint_coords = cv2.KeyPoint_convert([biggest_keypoint])

                print(biggest_keypoint)
                print(biggest_keypoint_coords)


                break

        cv2.destroyAllWindows()
        return biggest_keypoint_coords[0] # convert returns in form of [[... ...]]

    def get_blob_coords(self):
        # Editing code to get blob coords and sort by furthest from dump point

        print("Press Esc when points are circled in red")

        while (1):

            # Take each frame
            _, frame = self.cap.read()

            # frame = cv2.flip(frame, -1) might be needed depending on camera mount

            # Need to make it not black and white due to reflections, having problems w camera
            # im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            im = frame

            # Setup SimpleBlobDetector parameters, will be optimised for detecting the calibration points
            params = cv2.SimpleBlobDetector_Params()

            # Change thresholds
            params.minThreshold = 10
            params.maxThreshold = 200

            # Filter by Colour
            params.filterByColor = True
            blobColor = 10

            # Filter by Area.
            params.filterByArea = True
            params.minArea = 100
            params.maxArea = 900

            # Filter by Circularity
            params.filterByCircularity = True
            params.minCircularity = 0.5  # 0.9 not detecting rectangular lego blocks but picking up tape circles

            # Filter by Convexity
            params.filterByConvexity = False
            params.minConvexity = 0.87

            # Filter by Inertia
            params.filterByInertia = True
            params.minInertiaRatio = 0.1

            # Create a detector with the parameters
            detector = cv2.SimpleBlobDetector_create(params)

            # Detect blobs.
            keypoints = detector.detect(im)

            # Draw detected blobs as red circles.
            # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
            im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                                  cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            # Show keypoints
            cv2.imshow('frame', frame)
            cv2.imshow('keypoints', im_with_keypoints)

            k = cv2.waitKey(5) & 0xFF
            if k == 27 and len(keypoints) >= 1:
                def dist_to_dump(keypoint):
                    keypoint = [keypoint]
                    coord = cv2.KeyPoint_convert(keypoint)  # convert returns in form of [[... ...]]
                    dump = wp.cam_xy[2] # location needs to be in cam coords!
                    squar_dist = (coord[0][0] - dump[0]) ** 2 + (coord[0][1] - dump[1]) ** 2
                    return squar_dist

                ordered_keypoints = sorted(keypoints, key=dist_to_dump, reverse=True)
                ordered_points = cv2.KeyPoint_convert(ordered_keypoints)

                print(ordered_points)
                dist_list = [dist_to_dump(keypoint) for keypoint in ordered_keypoints]
                print(dist_list)

                break

        cv2.destroyAllWindows()
        return ordered_points