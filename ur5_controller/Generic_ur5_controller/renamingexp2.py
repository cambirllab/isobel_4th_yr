import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
import shutil

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
test_folder1 = "test_imgs/openloop/"
test_folder2 = "test_imgs/closedloop/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []

y_m = '2020-03-'
start_day_time = ['09-1239', '09-1247', '09-1253', '09-1258', '11-1203', '11-1212', '11-1220', '11-1228', '11-1234',
                  '11-1241', '11-1740', '11-1747', '11-1757', '11-1804', '11-1811', '11-1822', '11-1828', '11-1834',
                  '11-1840', '11-1850', '11-1857', '11-1904', '11-1913', '11-1918', '11-1927', '13-1400']
start_str = [y_m + day_time for day_time in start_day_time]
print(start_str)
addendum = ['-a', '-b', '-c', '-d', '-e', '-f', '-g', '-h', '-i', '-j', '-k', '-l']

os.chdir(darknet_path + 'test_imgs/closedloop')
for start_string in start_str[:-1]:
    print(start_string)
    img = cv.imread('yolo/' + start_string + '-a.jpg')
    cv.imwrite('gtruth/' + start_string + '.jpg', img)

input('wait')
# # make list of all images
# os.chdir(darknet_path + test_folder2)
# for i in range(len(start_str)-1):
#     date1 = start_str[i]
#     date2 = start_str[i+1]
#     print('hi', date1)
#
#     dict = {}
#
#     j = 0
#     for image in glob.glob('*.jpg'):
#         img = cv.imread(image)
#         date_time = image[:15]
#
#         if date1 <= date_time < date2:
#             if date_time in dict.keys():
#                 new_time = dict[date_time]
#             else:
#                 dict[date_time] = date1 + addendum[j]
#                 new_time = dict[date_time]
#                 j += 1
#             # print(date_time, new_time)
#
#             new_name = new_time + image[15:]
#             print(image, new_name)
#             # save image into yolo/
#             cv.imwrite('yolo/' + new_name, img)


# # make list of all text files
# os.chdir(darknet_path + test_folder2)
# for i in range(len(start_str)-1):
#     date1 = start_str[i]
#     date2 = start_str[i+1]
#     print('hi', date1)
#
#     dict = {}
#
#     j = 0
#     for image in glob.glob('*.txt'):
#         # img = cv.imread(image)
#         date_time = image[:15]
#         if image[-10:-4] == 'output':
#             print('wahey')
#         else:
#             if date1 <= date_time < date2:
#                 if date_time in dict.keys():
#                     new_time = dict[date_time]
#                 else:
#                     dict[date_time] = date1 + addendum[j]
#                     new_time = dict[date_time]
#                     j += 1
#                 # print(date_time, new_time)
#
#                 new_name = new_time + image[15:]
#                 print(image, new_name)
#                 # save image into yolo/
#                 # cv.imwrite('yolo/' + new_name, img)
#                 # save into yolo folder as well
#                 shutil.copyfile(image, 'yolo/' + new_name)

num_cycle_times = 0
total_cycle_time = 0
y_m = '2020-03-'
start_day_time = ['09-1239', '09-1247', '09-1253', '09-1258', '11-1203', '11-1212', '11-1220', '11-1228', '11-1235',
                  '11-1242', '11-1739', '11-1747', '11-1757', '11-1804', '11-1811', '11-1822', '11-1828', '11-1834',
                  '11-1840', '11-1850', '11-1857', '11-1908', '11-1913', '11-1918', '11-1927', '13-1400']
start_str = [y_m + day_time for day_time in start_day_time]
print(start_str)

yolo_times = []

os.chdir(darknet_path + test_folder2 + 'output/')
for date in start_str[:-1]:
    filename = date + '-output.txt'
    print(date)
    with open(filename) as file:
        flag = False
        for line in file:
            # if line.startswith('TIME! Made plan'):
            #     print(line[:-1])
            #     for i in range(3):
            #         print(next(file)[:-1])
            if line.startswith('TIME! Cycle time'):
                num_cycle_times += 1
                cyc_time = float(line[18:-1])
                total_cycle_time += cyc_time
            if not flag and line.startswith('TIME! Got image'):
                img_time = float(line[23:-1])
                print(img_time)
                flag = True
            if flag and line.startswith('TIME! Finished running'):
                yolo_time = float(line[35:-1])
                print(yolo_time)
                print(yolo_time - img_time)
                yolo_times.append(yolo_time - img_time)
                flag = False
    print('                 *****                   ')

print(yolo_times)
print(np.mean(yolo_times), np.std(yolo_times))

yolo_time = 10
num_yolos = 145
print(num_cycle_times)
print(total_cycle_time)
print(total_cycle_time / num_cycle_times)
print((total_cycle_time - (yolo_time*num_yolos)) / num_cycle_times)