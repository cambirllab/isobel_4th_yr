import time
import waypoints as wp
import numpy as np


class robotfuncs():
    # do i need init
    def __init__(self, robot):
        self.robot = robot

    def move_away(self):
        # Code to to get Urnie to twist away once in the home position to avoid obscuring camera
        # Getting into start position at away position
        if self.robot.getj() != wp.urnie_awayj:
            if self.robot.getl()[2] >= 0.0:
                # Not going to hit back board so just move to away
                self.robot.movej(wp.urnie_awayj)
            else:
                # Go to home and then away
                self.robot.home()
                self.robot.movej(wp.urnie_awayj)

        print('Out of the way')

    def get_marker_coords(self):
        # Going to prompt for calibration here using teach_mode
        # Return coordinates for p1, p2 and p3 in robot space???
        print("Get ready to go to r1")
        r1 = self.robot.teach_mode.record_end_pos()
        print(r1)
        print(self.robot.getl())

        # Repeat for r2 and r3
        print("Get ready to go to r2")
        r2 = self.robot.teach_mode.record_end_pos()
        print(r2)

        print("Get ready to go to r3")
        r3 = self.robot.teach_mode.record_end_pos()
        print(r3)

        # Get some confirmation of calibration after, i.e. move to r1, r2, r3, and back to home

        time.sleep(1)
        self.robot.home()
        time.sleep(1)
        self.robot.movel(r1)
        time.sleep(1)

        self.robot.movel(r2)
        time.sleep(1)
        self.robot.movel(r3)
        time.sleep(1)

        self.robot.home()
        print(self.robot.getl())

        wp.surface_avg = (r1[2] + r2[2] + r3[2]) / 3  # average of z heights

        ordered_points = np.array([r1[:3], r2[:3], r3[:3]])

        return ordered_points  # Returning x,y,z as array

    def get_alpha(points):
        # Given points a and b, what is the angle alpha of the tool head to be perpendicular to line ab
        a = points[0]
        b = points[1]

        if b[0] - a[0] == 0:  # different way to deal with exception?
            if b[1] - a[1] >= 0:
                alpha = -np.pi / 2
            else:
                alpha = np.pi / 2
        elif b[1] - a[1] == 0:
            if b[0] - a[0] >= 0:
                alpha = 0
            else:
                alpha = np.pi
        else:
            theta = np.arctan(abs((b[1] - a[1]) / (b[0] - a[0])))
            if b[0] - a[0] > 0:  # i.e. robot is moving from left to right
                if b[1] - a[1] >= 0:  # i.e. robot is moving from far to near, alpha between 0 and 90
                    alpha = (np.pi / 2 - theta)
                else:  # i.e. robot moving from near to far, alpha between 90 and 180
                    alpha = (np.pi / 2 + theta)
            else:  # i.e. robot is moving from right to left, alpha between 0 and -90
                if b[1] - a[1] >= 0:  # i.e. robot is moving from far to near
                    alpha = -(np.pi / 2 - theta)
                else:  # i.e. robot moving from near to far, alpha between -90 and -180
                    alpha = -(np.pi / 2 + theta)

        return alpha

    def sweep_single_blob(self, blob_coords_r):
        # This function takes the coordinates of a single blob in robot space and sweeps it to p3 in a straight line
        # The tool starts behind the blob and with the tool head rotated to be perpendicular to the sweep line

        p3_xy = wp.robot_xyz[2][:2]  # [:2] selects xy coords from robot_xyz

        # Trig to set start pos in line with blob and p3 but further away
        # and to set angle of head to perpendicular to that line
        start_pos_xy = [0, 0]
        start_pos_xy[0] = p3_xy[0] - 1.3 * (p3_xy[0] - blob_coords_r[0])  # 30% further away from p3 than the blob
        start_pos_xy[1] = p3_xy[1] - 1.3 * (p3_xy[1] - blob_coords_r[1])

        start_pos_angle = [0, np.pi, 0]  # pointing down and forwards

        start_pos_up = start_pos_xy + [0.25] + start_pos_angle
        self.robot.movel(start_pos_up)
        time.sleep(0.5)

        # Rotating tool to be perpendicular to sweep line
        theta = np.arctan((p3_xy[0] - blob_coords_r[0])/(p3_xy[1] - blob_coords_r[1]))
        # Dealing with case where blob is 'below' p3
        if blob_coords_r[1] - p3_xy[1] > 0:
            self.robot.movel_tool([0, 0, 0, 0, 0, -(np.pi/2 + theta)])
        else:
            self.robot.movel_tool([0, 0, 0, 0, 0, theta])

        start_pos_down = self.robot.getl()
        start_pos_down[2] = wp.surface_avg
        self.robot.movel(start_pos_down)
        time.sleep(0.5)

        # p3_xyz = p3_xy + [wp.surface_avg]

        self.robot.translatel(wp.robot_xyz[2][:3])  # Moving without rotating head to p3
        print("I tried my best!")

    def sweep_a_to_b(self, points):
        # This function will start just behind point a and sweep along the surface to point b, then lift up
        # Points in form [[ax ay az] [bx by bz]]

        a = points[0]
        b = points[1]

        start_pos_xy = [0, 0]
        start_pos_xy[0] = b[0] - 1.3 * (b[0] - a[0])  # 30% further away from b than a, could do diff method
        start_pos_xy[1] = b[1] - 1.3 * (b[1] - a[1])

        start_pos_angle = [0, np.pi, 0]

        start_pos_up = start_pos_xy + [0.10] + start_pos_angle
        self.robot.movel(start_pos_up)

        # Rotating tool to be perpendicular to sweep line
        if (b[0] - a[0]) == 0:  # different way to deal with exception?
            theta = 0
        else:
            theta = np.arctan(abs((b[1] - a[1]) / (b[0] - a[0])))
        # Dealing with case where a is 'below' b
        if b[0] - a[0] >= 0:  # i.e. robot is moving from left to right
            if b[1] - a[1] >= 0:  # i.e. robot is moving from far to near
                self.robot.movel_tool([0, 0, 0, 0, 0, (np.pi / 2 - theta)])
            else:  # i.e. robot moving from near to far
                self.robot.movel_tool([0, 0, 0, 0, 0, (np.pi / 2 + theta)])
        else:  # i.e. robot is moving from right to left
            if b[1] - a[1] >= 0:  # i.e. robot is moving from far to near
                self.robot.movel_tool([0, 0, 0, 0, 0, -(np.pi / 2 - theta)])
            else:  # i.e. robot moving from near to far
                self.robot.movel_tool([0, 0, 0, 0, 0, -(np.pi / 2 + theta)])

        start_pos_down = self.robot.getl()
        start_pos_down[2] = 0.025  # = wp.surface_avg
        self.robot.movel(start_pos_down)

        end_pos_down = start_pos_down
        end_pos_down[:2] = b[:2]
        self.robot.movel(end_pos_down)  # Moving without rotating head to b

        end_pos_up = self.robot.getl()
        end_pos_up[2] = 0.10
        self.robot.movel(end_pos_up)  # Lifting up the tool head

        print("I tried my best!")

    def sweep_contour(self, points, vel=0.3):
        # This function will sweep around the contour (above the surface rn) keeping the tool head roughly perpendicular
        # Points in form [[ax ay] [bx by]]

        # need to start with head at horizontal
        initial_pos_xy = points[0][:2]
        initial_pos_angle = np.array([0, np.pi, 0])
        initial_pos = np.concatenate((initial_pos_xy, [wp.surface_avg], initial_pos_angle), axis=None)
        self.robot.movel(initial_pos, vel=0.2)
        alpha1 = 0

        for i in range(len(points) - 1):
            a = points[i]
            b = points[i+1]
            # Could use mod to get to loop around to first element b = points[(i+1)%len(points)]

            start_pos_xy = [0, 0]
            start_pos_xy[0] = a[0]
            start_pos_xy[1] = a[1]

            start_pos_xyz = np.concatenate((start_pos_xy, [wp.surface_avg]), axis=None)  # May also be problem

            alpha2 = robotfuncs.get_alpha([a, b])
            phi = alpha2 - alpha1
            # self.robot.movel_tool([0, 0, 0, 0, 0, phi])

            # Moving without rotating head to b
            end_pos_xyz = start_pos_xyz
            end_pos_xyz[:2] = b[:2]
            self.robot.translatel(end_pos_xyz, vel=vel)  # change vel to desired velocity

            alpha1 = alpha2

        self.robot.home()

    def sweep_curve(self, points, vel, height, angle):
        # Going to try to get smooth motion along points defining the curve
        if height < -0.425:
            print('tool head will hit surface, resetting to -0.41')
            height = -0.41

        angle = np.deg2rad(angle)

        num_points = len(points)

        ds_array = []
        for i in range(len(points) - 1):
            ds = np.sqrt((points[i+1][0] - points[i][0])**2 + (points[i+1][1] - points[i][1])**2)
            ds_array.append(ds)
        ds_array.append(ds_array[-1])
        dt_array = [ds/0.05 for ds in ds_array]

        start_pos = np.concatenate((points[0][:2], [height + 0.05]), axis=None)
        self.robot.translatel(start_pos, acc=0.2, vel=0.2)
        self.robot.rotate_rel([0, 0, 0, 0, 0, angle])

        pos_angle = np.array(self.robot.getl()[3:])
        for i in range(len(points)):
            pos_xy = points[i][:2]
            pos = np.concatenate((pos_xy, [height], pos_angle), axis=None)
            points[i] = pos

        self.robot.movel(points[0])
        toc = time.time()

        dt_initial = 0.5
        dt_minimum = 0.005
        time_gain = 0.015
        for i in range(num_points - 1):
            if i < num_points / 2:
                dt = dt_initial - (i * time_gain) ** 0.5
            else:
                dt = dt_initial - (num_points - i) * time_gain
            if dt < dt_array[i]:
                dt = dt_array[i]
            dt_array[i] = dt
            self.robot.servoj(points[i], control_time=0.02, lookahead_time=0.008, gain=300)

        self.robot.movel(points[-1])
        tic = time.time()
        self.robot.home()

        path_time = tic - toc
        path_length = np.sum(ds_array)
        avg_vel = path_length / path_time

        return avg_vel

    def sweep_path(self, points, vel, height, angle):
        # What is used for liquid cleaning
        # Going to do linear motion between points
        if height < -0.42:
            print('tool head will hit surface, resetting to -0.42')
            height = -0.42

        angle = np.deg2rad(angle)

        num_points = len(points)

        start_pos = np.concatenate((points[0][:2], [height + 0.05]), axis=None)
        self.robot.translatel(start_pos, acc=0.2, vel=0.2)
        self.robot.rotate_rel([0, 0, 0, 0, 0, angle])

        pos_angle = np.array(self.robot.getl()[3:])
        for i in range(len(points)):
            pos_xy = points[i][:2]
            pos = np.concatenate((pos_xy, [height], pos_angle), axis=None)
            points[i] = pos

        self.robot.movel(points[0])
        toc = time.time()

        for i in range(num_points):
            self.robot.translatel(points[i], vel=vel)

        tic = time.time()
        print('time elapsed: ', tic-toc)

        self.robot.home()

    def squeeze(self):
        # Code to squeeze out the liquid

        # Getting into start position at away position
        if self.robot.getj() != wp.urnie_awayj:
            if self.robot.getl()[2] >= 0.0:
                # Not going to hit back board so just move to away
                self.robot.movej(wp.urnie_awayj)
            else:
                # Go to home and then away
                self.robot.robotfuncs.move_away()

        # # Code to define the squeezing positions, not necessary each time but may be helpful if setup changes
        # # If used, please copy values into waypoints.py

        # self.robot.movej(wp.urnie_squeezej)
        # time.sleep(1)
        # self.robot.force_move([0, 0, -0.20], force=150)
        # pushj = self.robot.getj()
        # print('down', pushj)
        # time.sleep(1.0)
        #
        # self.robot.movej(pushj)
        # self.robot.movel_tool([0.01, 0, 0.005, 0, 0, 0])
        # self.robot.rotate_rel([0, 0, 0, 0, 0.1, 0])
        # right_squeezej = self.robot.getj()
        # print('r', right_squeezej)
        # time.sleep(1)
        #
        # self.robot.movej(pushj)
        # self.robot.movel_tool([-0.01, 0, 0.005, 0, 0, 0])
        # self.robot.rotate_rel([0, 0, 0, 0, -0.1, 0])
        # left_squeezej = self.robot.getj()
        # print('l', left_squeezej)
        # time.sleep(1)
        #
        # self.robot.movej(pushj)
        # self.robot.movel_tool([0, -0.01, 0.01, 0, 0, 0])
        # self.robot.rotate_rel([0, 0, 0, 0.1, 0, 0])
        # front_squeezej = self.robot.getj()
        # print('f', front_squeezej)
        # time.sleep(1)
        # #
        # self.robot.movej(pushj)
        # self.robot.movel_tool([0, 0.01, 0.01, 0, 0, 0])
        # self.robot.rotate_rel([0, 0, 0, -0.1, 0, 0])
        # back_squeezej = self.robot.getj()
        # print('b', back_squeezej)
        # time.sleep(1)
        # self.robot.movej(pushj)

        # Code to execute squeezing motion

        self.robot.movej(wp.urnie_squeezej)
        self.robot.movej(wp.down_squeezej)
        time.sleep(2.0)
        # self.robot.movej(wp.right_squeezej)
        # time.sleep(0.5)
        # self.robot.movej(wp.down_squeezej)
        # self.robot.movej(wp.left_squeezej)
        # time.sleep(0.5)
        # self.robot.movej(wp.down_squeezej)
        # time.sleep(0.5)
        # self.robot.movej(wp.urnie_squeezej)
        # self.robot.movej(wp.down_squeezej)
        # time.sleep(1.0)
        self.robot.movej(wp.front_squeezej)
        time.sleep(1.0)
        self.robot.movej(wp.down_squeezej)
        self.robot.movej(wp.front_squeezej)
        time.sleep(1.0)
        # self.robot.movej(wp.back_squeezej)
        # time.sleep(0.5)
        self.robot.movej(wp.down_squeezej)

        self.robot.translatel_rel([0, 0, -0.02])
        # self.robot.force_move([0, 0, -0.02], force=200)
        time.sleep(0.8)
        self.robot.movej(wp.urnie_squeezej)

        return
