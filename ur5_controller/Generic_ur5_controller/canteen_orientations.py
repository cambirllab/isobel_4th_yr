import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
import matplotlib.pyplot as plt

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder


def drawline(img,pt1,pt2,color,thickness=1,style='dotted',gap=15):
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv.circle(img,p,thickness,color,-1)
    else:
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv.line(img,s,e,color,thickness)
            i+=1


def drawpoly(img,pts,color,thickness=1,style='dotted',):
    s=pts[0]
    e=pts[0]
    pts.append(pts.pop(0))
    for p in pts:
        s=e
        e=p
        drawline(img,s,e,color,thickness,style)


def drawrect(img,pt1,pt2,color,thickness=1,style='dotted'):
    pts = [pt1,(pt2[0],pt1[1]),pt2,(pt1[0],pt2[1])]
    drawpoly(img,pts,color,thickness,style)

    # cv.imshow('im',im)
    # cv.waitKey()


def draw_cv_rect(image, obj_dict, colour, draw_l_diagonal=False, draw_r_diagonal=False, order=0, gtruth=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    top_right_x = int((x + (w / 2)) * width)
    top_right_y = int((y - (h / 2)) * height)
    bot_left_x = int((x - (w / 2)) * width)
    bot_left_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    pt3 = (top_right_x, top_right_y)
    pt4 = (bot_left_x, bot_left_y)

    line_thickness = 10
    if gtruth:
        drawrect(image, pt1, pt2, colour, line_thickness, 'dotted')
    else:
        cv.rectangle(image, pt1, pt2, colour, line_thickness)
        if obj_dict['type'] == 'fork':
            if obj_dict['prob'] <80:
                location = (bot_left_x + 40, bot_left_y + 80)
            # elif obj_dict['prob'] <97:
            #     location = (top_left_x - 125, top_left_y - 15)
            else:
                # location = (top_left_x - 100, top_left_y - 15)
                location = (bot_left_x - 40, bot_left_y + 80)
        else:
            location = (top_left_x +20, top_left_y - 20)
        cv.putText(image, text=obj_dict['type'] + ' ' + str(int(obj_dict['prob'])) + '%',
                   org=location, fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=2.5,
                   thickness=8)

    if draw_l_diagonal:
        cv.line(image, pt1, pt2, colour, line_thickness)
    if draw_r_diagonal:
        cv.line(image, pt3, pt4, colour, line_thickness)

    if order != 0:
        order_label = str(order)
        y_off = 120
        x_off = 30
        th = 10
        if draw_r_diagonal:
            cv.putText(image, text=order_label, org=(top_left_x+x_off, top_left_y+y_off), fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)
        elif draw_l_diagonal:
            cv.putText(image, text=order_label, org=(top_right_x - 4 * x_off, top_right_y + y_off),
                       fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)
        else:
            cv.putText(image, text=order_label, org=(top_left_x+x_off, top_left_y+y_off),
                       fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)


    return image


os.chdir(canteen_yolo_path)

cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']

for i in range(1, 70):
    filename = 'tray' + str(i) + '_c'
    objects = np.load(filename + '_objs.npy', allow_pickle=True)
    # print(objects)

    img = cv.imread(filename + '.jpg')
    img1 = cv.imread(filename + '.jpg')

    for obj in objects:
        if obj['type'] in cutlery_list:
            print('oi')
            tilt = obj['box']['tilt']
            if tilt == 'l':
                draw_cv_rect(img, obj, (0, 255, 255), draw_l_diagonal=True)
            elif tilt == 'r':
                draw_cv_rect(img, obj, (255, 255, 0), draw_r_diagonal=True)
            elif tilt == 'u':
                draw_cv_rect(img, obj, (255, 0, 255))
            elif tilt is None:
                print('HELP')

    resized = cv.resize(img, (480, 270))
    resized1 = cv.resize(img1, (480, 270))
    cv.imshow(filename, resized)
    # cv.imshow(filename + 'clean', resized1)
    cv.waitKey(0)
    cv.destroyAllWindows()