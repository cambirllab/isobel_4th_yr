import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []


# for image in glob.glob("*.jpg"):
#     # image gives me name
#     print(image)
#     if image[-5] == 'c':
#         img_list.append(image)
#         img = cv.imread(image)
#         cv.imwrite('../gtruth/' + image, img)


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def get_canteen_plates(image_path, show_img=True):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # # for small plastic
    # p1 = 80
    # p2 = 50
    # min_r = 120
    # max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    # for big canteen plates
    p1 = 80
    p2 = 60
    min_r = 355
    max_r = 370

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    # for small canteen plates
    p1 = 80
    p2 = 50
    min_r = 230
    max_r = 240

    circles_s = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    print(circles_b)
    print(circles_s)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if circles_s is not None:
        circles = np.uint16(np.around(circles_s))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0,
                                  "box": {"x": i[0] / wid, "y": i[1] / hei, "w": 2 * i[2] / wid,
                                          "h": 2 * i[2] / hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    print(plate_objects)
    return plate_objects


# for txtfile in glob.glob("*.txt"):
#     if txtfile[0] == "t":
#         print(txtfile)
#         print(get_objs_from_true_txt(txtfile))
#         input('next file?')


# os.chdir(canteen_yolo_path)
# for image in glob.glob("*.jpg"):
#     if image[-5] == 'c':
#         img_list.append(image)
# print(img_list)
#
# os.chdir("../../")
# print(os.getcwd())
#
# # print(np.load(canteen_folder + yolo_folder + 'tray10_c_objs.npy', allow_pickle=True))
#
# for i in range(len(img_list)):
#     image_name = img_list[i]
#     print(i, image_name)
#     objects = yf.get_lr_tilt(image_name[:-4], canteen_folder + yolo_folder + image_name,
#                              canteen_folder + yolo_folder + image_name[:-4] + '.txt', tilt=5, show_imgs=False)
#
#     # do plate (big + small)? and bowl detection through circles, probably in yolofuncs, and append to text document
#     print('my obbies', objects)
#     np.save(canteen_folder + yolo_folder + image_name[:-4] + '_objs.npy', objects)
#
# print('Been through all the images!')


# setup counts
predict_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
iou_lists = {'fork': [], 'spoon': [], 'knife': [], 'cup': [], 'plate': [], 'bowl': [], 'cutlery': []}
temp_close_enough = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
temp_tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image


os.chdir(canteen_path)


for i in range(1,70):
    filename = 'tray' + str(i) + '_c'

    print(filename)
    try:
        objects = np.load('yolo/' + filename + '_objs.npy', allow_pickle=True)
        plates = get_canteen_plates('yolo/' + filename + '.jpg', show_img=False)
        objects = np.concatenate((objects, plates))
        print(objects)
        # RUN GET PLATES

        true_objects = get_objs_from_true_txt('gtruth/' + filename + '.txt')
        print(true_objects)

        old_ground_counts = ground_counts
        old_predict_counts = predict_counts
        old_tp_counts = tp_counts

        for true_obj in true_objects:
            obj_type = true_obj['type']
            print(obj_type)
            for obj in objects:
                if obj['type'] == true_obj['type']:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    iou_thresh = 0.5
                    if iou >= iou_thresh:
                        tp_counts[true_obj['type']] = tp_counts[true_obj['type']] + 1
                        to_app_list = iou_lists[true_obj['type']]
                        iou_lists[true_obj['type']] = np.concatenate((to_app_list, [iou]))
                    else:
                        print('didnt pass: ', true_obj['type'])

                # adding to cutlery true count if fork and fork or fork and spoon or fork and cutlery etc
                cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']
                if obj['type'] in cutlery_list and true_obj['type'] == 'cutlery':
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    iou_thresh = 0.5
                    if iou >= iou_thresh:
                        tp_counts['cutlery'] = tp_counts['cutlery'] + 1
                        to_app_list = iou_lists['cutlery']
                        iou_lists['cutlery'] = np.concatenate((to_app_list, [iou]))
                    else:
                        print('didnt pass: ', true_obj['type'])

                    print('done single comp')

        # TODO: Fix this, consider refactoring you have a list of cutlery objects predicted, try all out at once, look if value greater than thresh exists in list
        cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']
        cutlery_predictions = []
        for obj in objects:
            if obj['type'] in cutlery_list:
                cutlery_predictions.append(obj)
        # print(true_objects)
        # print(objects)
        # print(cutlery_predictions)
        if len(cutlery_predictions) > 0:
            for true_obj in true_objects:
                # print('here1', true_obj['type'])
                if true_obj['type'] in cutlery_list:
                    print('seen cutlery!')
                    empty_ious = np.zeros(len(cutlery_predictions))
                    empty_threshs = np.zeros(len(cutlery_predictions))
                    empty_matches = np.zeros(len(cutlery_predictions))
                    # print('here2')
                    # find one that
                    for c in range(len(cutlery_predictions)):
                        cut_obj = cutlery_predictions[c]

                        if true_obj['type'] == cut_obj['type']:
                            thresh = 0.5
                            match = True
                        else:
                            thresh = 0.5
                            match = False
                        # print('here3')

                        x1 = cut_obj['box']['x']
                        y1 = cut_obj['box']['y']
                        w1 = cut_obj['box']['w']
                        h1 = cut_obj['box']['h']

                        x2 = true_obj['box']['x']
                        y2 = true_obj['box']['y']
                        w2 = true_obj['box']['w']
                        h2 = true_obj['box']['h']

                        # print(x1, y1, w1, h1, x2, y2, w2, h2)

                        inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                        # print([inter, union, iou])
                        empty_ious[c] = iou
                        empty_threshs[c] = thresh
                        empty_matches[c] = match
                    # print(empty_ious)
                    for z in range(len(empty_ious)):
                        iou = empty_ious[z]
                        thresh = empty_threshs[z]
                        match = empty_matches[z]
                        if iou >= thresh:
                            temp_close_enough[true_obj['type']] += 1
                            print('breaking?', iou, thresh, match)
                            if match:
                                temp_tp_counts[true_obj['type']] += 1
                            break  # (to only get if there is one or not)

        # add to the counts and draw rectangles
        for true_obj in true_objects:
            # if true_obj['type'] == 'knife':
            #     print('knife in', str(filename))
            ground_counts[true_obj['type']] = ground_counts[true_obj['type']] + 1
            # draw rectangle
            # draw_cv_rect(img, true_obj, isGround=True)
        for obj in objects:
            try:
                predict_counts[obj['type']] = predict_counts[obj['type']] + 1
            except KeyError:
                print(obj['type'], 'not one of object set')
            # draw rectangle
            # draw_cv_rect(img, obj, isGround=False)

        print(old_ground_counts, old_predict_counts, old_tp_counts)
        print(ground_counts, predict_counts, tp_counts, iou_lists)

        # print(temp_close_enough)

        # NUMPY SAVE SOME SHIT
        np.save('ground_counts.npy', ground_counts)
        np.save('predict_counts.npy', predict_counts)
        np.save('tp_counts.npy', tp_counts)
        np.save('iou_lists.npy', iou_lists)
        np.save('temp_tp_counts.npy', temp_tp_counts)
        np.save('temp_close_enough.npy', temp_close_enough)

        # overlaps1 = yf.get_overlap_matrix(true_objects)
        # overlaps2 = yf.get_overlap_matrix(objects)
        # print(yf.get_pick_order(true_objects, overlaps1))
        # print(yf.get_pick_order(objects, overlaps2))

    except IOError:
        print('File not accessible')

print(ground_counts)
print(predict_counts)
print(tp_counts)
print(temp_tp_counts)
print(temp_close_enough)


print('DONE')


#
#
# # print(img_list)
# # objects = yf.get_lr_tilt('MAH00251_Moment0', 'canteen_imgs/MAH00251_Moment0.jpg', 'canteen_imgs/MAH00251_Moment0.txt', tilt=5, show_imgs=True)
# # input()
# #
# # objects = yf.get_lr_tilt('2020-03-03-1225', 'canteen_imgs/2020-03-03-1225.jpg', 'canteen_imgs/2020-03-03-1225.txt', tilt=5, show_imgs=True)
# objects = yf.get_objs_from_txt('canteen_imgs/2020-03-03-1215.txt')
# plates = yf.get_plates('2020-03-03-1225', 'canteen_imgs/2020-03-03-1215.jpg', show_img=False)
# objects += plates
#
# print('All Objs:', objects)
# set = ['plate', 'spoon', 'fork', 'knife', 'cup']
# set_objects = [obj for obj in objects if (obj['type'] in set)]
# print('Set Objs:', set_objects)
# imagy = yf.draw_cv_rect('canteen_imgs/2020-03-03-1215.jpg', set_objects, isGround=False, show=True, pick_order=None, draw_percents=True)
# cv.imwrite('canteen_imgs/labelled2.jpg', imagy)
#
# picking = yf.get_pick_order(set_objects)
#
# objects = yf.get_objs_from_txt('canteen_imgs/2020-03-03-1215_tilt5.txt')
# plates = yf.get_plates('2020-03-03-1215_tilt5', 'canteen_imgs/2020-03-03-1215_tilt5.jpg', show_img=False)
# objects += plates
#
# print('All Objs:', objects)
# set = ['plate', 'spoon', 'fork', 'knife', 'cup']
# set_objects = [obj for obj in objects if (obj['type'] in set)]
# print('Set Objs:', set_objects)
# imagy = yf.draw_cv_rect('canteen_imgs/2020-03-03-1215_tilt5.jpg', set_objects, isGround=False, show=True, pick_order=None)
# cv.imwrite('canteen_imgs/labelled1.jpg', imagy)
#
#
#
#
# # for img in img_list:
# #     path = 'canteen_imgs/' + img
# #     print(path)
# #     yf.run_yolo(img, path, show_img=True)