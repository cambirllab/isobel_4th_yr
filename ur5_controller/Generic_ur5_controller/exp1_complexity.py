import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
import matplotlib.pyplot as plt

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def get_canteen_plates(image_path, show_img=True):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # # for small plastic
    # p1 = 80
    # p2 = 50
    # min_r = 120
    # max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    # for big canteen plates
    p1 = 80
    p2 = 60
    min_r = 355
    max_r = 370

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    # for small canteen plates
    p1 = 80
    p2 = 50
    min_r = 230
    max_r = 240

    circles_s = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    print(circles_b)
    print(circles_s)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if circles_s is not None:
        circles = np.uint16(np.around(circles_s))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0,
                                  "box": {"x": i[0] / wid, "y": i[1] / hei, "w": 2 * i[2] / wid,
                                          "h": 2 * i[2] / hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    print(plate_objects)
    return plate_objects


# setup counts
predict_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
iou_lists = {'fork': [], 'spoon': [], 'knife': [], 'cup': [], 'plate': [], 'bowl': [], 'cutlery': []}
temp_close_enough = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
temp_tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image



close_enough = True
cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']

overall_true_ious = []
overall_ce_ious = []

ground_cups = []
true_predicted_cups = []

exp1_names = ['2020-03-09-1041.jpg', '2020-03-09-1046.jpg', '2020-03-09-1051.jpg', '2020-03-09-1056.jpg', '2020-03-09-1102.jpg', '2020-03-09-1107.jpg', '2020-03-09-1120.jpg', '2020-03-09-1126.jpg', '2020-03-09-1130.jpg', '2020-03-09-1135.jpg', '2020-03-09-1201.jpg', '2020-03-09-1210.jpg', '2020-03-09-1215.jpg', '2020-03-09-1218.jpg', '2020-03-09-1222.jpg', '2020-03-11-1055.jpg', '2020-03-11-1059.jpg', '2020-03-11-1105.jpg', '2020-03-11-1112.jpg', '2020-03-11-1118.jpg', '2020-03-11-1124.jpg', '2020-03-11-1137.jpg', '2020-03-11-1145.jpg', '2020-03-11-1149.jpg', '2020-03-11-1153.jpg']

exp1_start_list = '3445433444544444445556575'
exp1_start_nos = [int(exp1_start_list[i]) for i in range(len(exp1_start_list))]
print(exp1_start_nos)
exp1_picked_list = '2421320443233320332423234'
exp1_picked_nos = [100 * int(exp1_picked_list[i]) for i in range(len(exp1_picked_list))]
print(exp1_picked_nos)

exp1_dates = [x[:-4] for x in exp1_names]
print(exp1_dates)

os.chdir(darknet_path + 'test_imgs/openloop/')

for i in range(len(exp1_dates)):
    filename = exp1_dates[i]

    print(filename)
    try:
        tray_true_ious = []
        tray_ce_ious = []

        objects = np.load('yolo/' + filename + '-yobjs.npy', allow_pickle=True)
        # plates = get_canteen_plates('yolo/' + filename + '.jpg', show_img=False)
        # objects = np.concatenate((objects, plates))
        print(objects)
        # RUN GET PLATES

        true_objects = get_objs_from_true_txt('gtruth/' + filename + '.txt')
        print(true_objects)

        old_ground_counts = ground_counts
        old_predict_counts = predict_counts
        old_tp_counts = tp_counts

        for true_obj in true_objects:
            object_true_ious = [0]
            object_ce_ious = [0]
            obj_type = true_obj['type']
            print(obj_type)
            if true_obj['type'] == 'cup':
                ground_cups.append((true_obj['box']['x'], true_obj['box']['y']))

            for obj in objects:
                if (obj['type'] == true_obj['type']) or (obj['type'] in cutlery_list and true_obj['type'] in cutlery_list):
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    object_ce_ious.append(iou)
                if obj['type'] == true_obj['type']:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    object_true_ious.append(iou)

                    if obj['type'] == 'cup' and iou >= 0.5:
                        true_predicted_cups.append((true_obj['box']['x'], true_obj['box']['y']))

            # find max
            max_ce_iou = max(object_ce_ious)
            max_true_iou = max(object_true_ious)
            print(max_ce_iou, max_true_iou)

            tray_ce_ious.append(max_ce_iou)
            tray_true_ious.append(max_true_iou)

        print(len(true_objects), len(tray_ce_ious), len(tray_true_ious))

        avg_ce_iou = sum(tray_ce_ious) / len(tray_ce_ious)
        avg_true_iou = sum(tray_true_ious) / len(tray_true_ious)

        overall_true_ious.append(avg_true_iou)
        overall_ce_ious.append(avg_ce_iou)

    except IOError:
        print('File not accessible')




# # cups
# print(ground_cups)
# print(true_predicted_cups)
# ground_cup_xs = [x[0] for x in ground_cups]
# ground_cup_ys = [1-x[1] for x in ground_cups]
# ground_dist_to_cent = [((a-0.5)**2 + (b-0.5)**2)**0.5 for a, b in zip(ground_cup_xs, ground_cup_ys)]
# predicted_xs = [x[0] for x in true_predicted_cups]
# predicted_ys = [1-x[1] for x in true_predicted_cups]
# predicted_dist_to_cent = [((a-0.5)**2 + (b-0.5)**2)**0.5 for a, b in zip(predicted_xs, predicted_ys)]
# print(np.mean(ground_dist_to_cent), np.mean(predicted_dist_to_cent))
# print((np.sum(ground_dist_to_cent) - np.sum(predicted_dist_to_cent)) / (len(ground_dist_to_cent) - len(predicted_dist_to_cent)))
# plt.figure(figsize=(5.76, 3.24))
# plt.scatter(ground_cup_xs, ground_cup_ys, c='red')
# plt.scatter(predicted_xs, predicted_ys, c='green', marker='.')
# plt.xlim([0,1])
# plt.ylim([0,1])
# plt.show()
clear_rates = [a / b for a, b in zip(exp1_picked_nos, exp1_start_nos)]
print(clear_rates)
level_clear_rates = [np.sum(exp1_picked_nos[0:10]) / np.sum(exp1_start_nos[0:10]), np.sum(exp1_picked_nos[10:20]) / np.sum(exp1_start_nos[10:20]), np.sum(exp1_picked_nos[20:25]) / np.sum(exp1_start_nos[20:25])]

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6,4))
level_avg_ious = [np.mean(overall_true_ious[0:10]), np.mean(overall_true_ious[10:20]), np.mean(overall_true_ious[20:25])]
# ax.scatter(overall_true_ious[0:], clear_rates[0:], c='k', marker='.', clip_on=False)
ax.scatter(overall_true_ious[0:10], clear_rates[0:10], c='b', marker='.', clip_on=False)
ax.scatter(overall_true_ious[10:20], clear_rates[10:20], c='r', marker='.', clip_on=False)
ax.scatter(overall_true_ious[20:25], clear_rates[20:25], c='g', marker='.', clip_on=False)
# plt.scatter(overall_true_ious[10:20], clear_rates[10:20], c='g', marker='.')
# plt.scatter(overall_true_ious[20:25], clear_rates[20:25], c='b', marker='.')
ax.scatter(level_avg_ious[0], level_clear_rates[0], c='b', marker='x', label='Level 1 Average')
ax.scatter(level_avg_ious[1], level_clear_rates[1], c='r', marker='x', label='Level 2 Average')
ax.scatter(level_avg_ious[2], level_clear_rates[2], c='g', marker='x', label='Level 3 Average')
pts = 50
xs = np.linspace(0,1,pts)
ys = np.linspace(0,100, pts)
# plt.plot(xs, ys, linestyle='dashed')

plt.legend(loc='upper left')
# plt.title('Closed-loop')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylim([0,100])
plt.xlim([0,1.0])
plt.xlabel('Average IOU for initial object detection')
plt.ylabel('Clearing success rate (%)')
plt.tight_layout()
plt.show()

print(overall_true_ious)
print(overall_ce_ious)

print(np.mean(overall_true_ious[0:10]), np.mean(overall_true_ious[10:20]), np.mean(overall_true_ious[20:25]))
for z in range(len(overall_true_ious) // 5):
    separation = len(overall_true_ious) // 5
    start = z * separation
    end = (z+1) * separation
    print(start, end)
    print(np.mean(overall_true_ious[start:end]))

print(sorted(overall_ce_ious, reverse=True))

img_list = [i+1 for i in range(len(exp1_dates))]
print(img_list)


sorted_true_img_list = [x for _, x in sorted(zip(overall_true_ious, img_list), reverse=True)]
sorted_ce_img_list = [x for _, x in sorted(zip(overall_ce_ious, img_list), reverse=True)]
print(sorted_true_img_list)
print(sorted_ce_img_list)

print(np.sum(img_list[0:10]), np.sum(img_list[10:20]), np.sum(img_list[20:25]))
print(np.sum(sorted_true_img_list[0:10]), np.sum(sorted_true_img_list[10:20]), np.sum(sorted_true_img_list[20:25]))
print(np.sum(sorted_ce_img_list[0:10]), np.sum(sorted_ce_img_list[10:20]), np.sum(sorted_ce_img_list[20:25]))


input('hows this?')

averaged_ious = [(a + b) / 2 for a, b in zip(overall_true_ious, overall_ce_ious)]
print('avg', averaged_ious)

corresponding_ce_ious = [overall_ce_ious[int(x[4:-2]) - 1] for x in sorted_true_img_list]
print(corresponding_ce_ious)

level1_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if a >= 0.6]
print(level1_imgs)
print(sorted(overall_true_ious, reverse=True)[:len(level1_imgs)])
print(len(level1_imgs))
level2_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.6 > a >= 0.4]
level3_imgs = [x for a, x in sorted(zip(overall_true_ious, img_list), reverse=True) if 0.4 > a]
print(len(level1_imgs), len(level2_imgs), len(level3_imgs))
print(level1_imgs[7], level2_imgs[4], level2_imgs[13], level3_imgs[1])

plt.figure()
plt.plot(np.arange(1, 70), sorted(overall_true_ious, reverse=True))
# plt.plot(np.arange(0, 69), corresponding_ce_ious)
# plt.plot(np.arange(0, 69), sorted(overall_ce_ious, reverse=True))
# plt.plot(np.arange(0, 69), sorted(averaged_ious, reverse=True))
plt.xlim([0,70])
plt.ylim([0, 1])
plt.xlabel('ranking')
plt.ylabel('average iou across objects on tray')
# plt.show()
# input('do not continue')

print(sorted(overall_true_ious)[0])
print(sorted(overall_true_ious)[-1])

best_iou = sorted_true_img_list[0]
image_path = gtruth_folder + best_iou + '.jpg'
best = cv.imread(image_path)
cv.imwrite('canteen-best.jpg', best[:, 200:-200])
cv.imshow('best', best[:, 200:-200])
cv.waitKey(0)
cv.destroyAllWindows()


print('DONE')
