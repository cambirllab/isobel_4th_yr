import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf

# iterate over images in folder
# get the yolo results
# get the ground truth results in same form
# handle file not existing or being empty
# for each object compare with others of same type from other file
# record intersection, union, iou, if no union

# for image in /imgwYOLOlabels:
#   get filename (without jpg)
#   get yolo objects

# os.chdir("./mytrain")
# for image in glob.glob("*.jpg"):
#     # image gives me name
#     filename = "C:\\Users\\isobe\\Desktop\\darknet\\build\\darknet\\x64\\data\\coco\\images\\mytrain\\" + image
#
#     print(filename)  # /imgwYOLOlabels/filename.txt
#
# input()

# setup counts
predict_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'tray': 0}
ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'tray': 0}
tp_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'tray': 0}
iou_lists = {'fork': [], 'spoon': [], 'knife': [], 'cup': [], 'plate': [], 'tray': []}


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    print('b, l:', b, l)

    if b < 0 or l < 0:
        print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image


os.chdir("./imgwYOLOlabels")
# print('Calling....')
# subprocess.check_call(["ls", "-l"], shell=True)

for image in glob.glob("*.jpg"):
    img = cv.imread(image)
    # image gives me name
    filename = image[:-3] + 'txt'

    print(filename)  # /imgwYOLOlabels/filename.txt
    objects = []
    try:
        with open(filename) as f:
            for line in f:
                objects.append(json.loads(line))

        # reversing so highest probability at start
        objects.reverse()
        print(objects)

        next_filename = '../imgwgroundlabels/' + filename
        # print(next_filename)  # ../imgwgroundlabels/filename.txt
        true_lsts = []
        true_objects = []
        with open(next_filename) as f:
            for line in f:
                lin = str(line)
                true_lsts.append(lin.split())
        print(true_lsts)

        types_conv = {'41': "cup", '42': "fork", '43': "knife", '44': "spoon", '45': "bowl", '80': "plate", '81': "tray"}
        for lst in true_lsts:
            typ = types_conv[lst[0]]
            tx = float(lst[1])
            ty = float(lst[2])
            tw = float(lst[3])
            th = float(lst[4])

            obj_dct = {"type": typ, "prob": 100, "box": {"x": tx, "y": ty, "w": tw, "h": th}}

            true_objects.append(obj_dct)
        print(true_objects)

        old_ground_counts = ground_counts
        old_predict_counts = predict_counts
        old_tp_counts = tp_counts

        for true_obj in true_objects:
            obj_type = true_obj['type']
            print(obj_type)
            for obj in objects:
                if obj['type'] == true_obj['type']:
                    x1 = obj['box']['x']
                    y1 = obj['box']['y']
                    w1 = obj['box']['w']
                    h1 = obj['box']['h']

                    x2 = true_obj['box']['x']
                    y2 = true_obj['box']['y']
                    w2 = true_obj['box']['w']
                    h2 = true_obj['box']['h']

                    print(x1, y1, w1, h1, x2, y2, w2, h2)

                    inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
                    print([inter, union, iou])

                    to_app_list = iou_lists[true_obj['type']]
                    iou_lists[true_obj['type']] = np.concatenate((to_app_list, [iou]))
                    iou_thresh = 0.5
                    if iou >= iou_thresh:
                        tp_counts[true_obj['type']] = tp_counts[true_obj['type']] + 1
                    else:
                        print('didnt pass: ', true_obj['type'])

                    print('done single comp')


        # add to the counts and draw rectangles
        for true_obj in true_objects:
            if true_obj['type'] == 'knife':
                print('knife in', str(image))
            ground_counts[true_obj['type']] = ground_counts[true_obj['type']] + 1
            # draw rectangle
            draw_cv_rect(img, true_obj, isGround=True)
        for obj in objects:
            try:
                predict_counts[obj['type']] = predict_counts[obj['type']] + 1
            except KeyError:
                print(obj['type'], 'not one of object set')
            # draw rectangle
            draw_cv_rect(img, obj, isGround=False)

        print(old_ground_counts, old_predict_counts, old_tp_counts)
        print(ground_counts, predict_counts, tp_counts, iou_lists)

        overlaps1 = yf.get_overlap_matrix(true_objects)
        overlaps2 = yf.get_overlap_matrix(objects)
        print(yf.get_pick_order(true_objects, overlaps1))
        # print(yf.get_pick_order(objects, overlaps2))

        cv.imshow('Image', img)
        cv.waitKey(0)
        cv.destroyAllWindows()


        # save and show the image w boxes
        # saved in imgwYOLOlabels folder
        if image == '2019-12-11-1539.jpg' or image == '2019-12-11-1542.jpg':
            img_name = 'ha-' + image[:-4] + '.jpg'
            crop_ha = img[100:350, 120:400]
            cv.imwrite(img_name, crop_ha)

            # cv.imshow('Image', crop_ha)
            # cv.waitKey(0)
            # cv.destroyAllWindows()


    except IOError:
        print('File not accessible')

print(ground_counts)
print(predict_counts)
print(tp_counts)


import matplotlib
import matplotlib.pyplot as plt
import numpy as np


labels = ['fork', 'spoon', 'knife', 'cup']
precisions = [tp_counts[otype]/predict_counts[otype] for otype in labels]
recalls = [tp_counts[otype]/ground_counts[otype] for otype in labels]
iou_means = [np.average(iou_lists[otype]) for otype in labels]
iou_stddevs = [np.std(iou_lists[otype]) for otype in labels]
iou_stderr = [scipy.stats.sem(iou_lists[otype]) for otype in labels]
print(iou_means, iou_stddevs, iou_stderr)

x = np.arange(len(labels))  # the label locations
width = 0.30  # the width of the bars

fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, sharex=True, figsize=(8,3))
rects1 = ax2.bar(x - width / 2, precisions, width, label='Precision', color=[0, 0.4470, 0.7410])
        # plt.plot(t, trajs[:,1], label=r'$\theta_2$', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,2], label=r'$\theta_3$', color=[0.6350, 0.0780, 0.1840])
rects2 = ax2.bar(x + width / 2, recalls, width, label='Recall', color=[0.6350, 0.0780, 0.1840])

# Add some text for labels, title and custom x-axis tick labels, etc.
ax2.set_ylabel('Precision and Recall')
ax1.set_ylabel('Average Intersection-Over-Union')
ax2.set_ylim([0,1])
ax2.set_xticks(x)
ax2.set_xticklabels(labels)
ax2.legend(fontsize=8, loc='upper left', bbox_to_anchor=(0.45, 1))
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
#ax2.set_title('Precision and Recall', fontsize=10)


rects3 = ax1.bar(x, iou_means, width*2, yerr=iou_stderr, capsize=4, label='IOU', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,1], label=r'$\theta_2$', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,2], label=r'$\theta_3$', color=[0.6350, 0.0780, 0.1840])


# Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Performance')
ax1.set_ylim([0,1])
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
#ax1.set_title('Average Intersection-Over-Union', fontsize=10)


# def autolabel(rects):
#     """Attach a text label above each bar in *rects*, displaying its height."""
#     for rect in rects:
#         height = rect.get_height()
#         ax.annotate('{}'.format(height),
#                     xy=(rect.get_x() + rect.get_width() / 2, height),
#                     xytext=(0, 3),  # 3 points vertical offset
#                     textcoords="offset points",
#                     ha='center', va='bottom')
#
#
# autolabel(rects1)
# autolabel(rects2)

#fig.tight_layout()


not_id = [2, 0, 6, 4]
id_not_pick = [2, 2, 2, 1]
id_and_pick = [6, 8, 2, 5]
all_id = [sum(i) for i in zip(id_not_pick, id_and_pick)]
#print(all_id)

fig1, ax3 = plt.subplots(figsize=(4,3))

p3 = ax3.bar(x, not_id, width*2, bottom=all_id, capsize=4, label='Not identified correctly', color=[0.6350, 0.0780, 0.1840])
p2 = ax3.bar(x, id_not_pick, width*2, bottom=id_and_pick, capsize=4, label='Identified correctly but not picked successfully', color=[0, 0.4470, 0.7410])
p1 = ax3.bar(x, id_and_pick, width*2, capsize=4, label='Identified correctly and picked successfully', color=[0.4660, 0.6740, 0.1880])


ax3.set_ylim([0,10])
ax3.set_xticks(x)
ax3.set_xticklabels(labels)
ax3.set_ylabel('Number of tests')
ax3.legend()
ax3.legend(fontsize=8, loc='lower center', bbox_to_anchor=(0.5, 1))
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
#ax2.set_title('Precision and Recall', fontsize=10)
fig1.tight_layout()
plt.show()
