import serial
import time
import serial.tools.list_ports

def serial_send(cmd, wait=False):
    ipt = ""
    ee.reset_input_buffer()
    ee.write(str.encode(cmd + "\n"))
    # wait for cmd acknowledgement
    while True:
        ipt = bytes.decode(ee.readline())
        print(ipt)
        break
    return ipt

available_ports = [i.device for i in serial.tools.list_ports.comports()]
print(available_ports)
ee = serial.Serial(available_ports[0], 9600)  # open serial port
ee.send_break()
time.sleep(1)  # This is needed to allow MBED to send back command in time!
print('Moving on...')
try:
    while 1:
        ipt = input('command for gripper?: ')
        t1,t2,t3 = ipt.split(',')
        t1 = "{0:.2f}".format(float(t1))
        t2 = "{0:.2f}".format(float(t2))
        t3 = "{0:.2f}".format(float(t3))
        zero = "0"
        t1 = "0"*(6-len(t1))+t1
        t2 = "0" * (6 - len(t2)) + t2
        t3 = "0" * (6 - len(t3)) + t3
        serial_send("{}{}{}".format(t1,t2,t3), False)
except:
    ee.close()