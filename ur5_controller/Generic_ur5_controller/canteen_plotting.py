import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
from tqdm import tqdm

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

ground_counts = np.load(canteen_path + 'ground_counts.npy', allow_pickle=True).item()
predict_counts = np.load(canteen_path + 'predict_counts.npy', allow_pickle=True).item()
tp_counts = np.load(canteen_path + 'tp_counts.npy', allow_pickle=True).item()
iou_lists = np.load(canteen_path + 'iou_lists.npy', allow_pickle=True).item()
temp_tp_counts = np.load(canteen_path + 'temp_tp_counts.npy', allow_pickle=True).item()
temp_close_enough = np.load(canteen_path + 'temp_close_enough.npy', allow_pickle=True).item()

ground_counts['cutlery'] = ground_counts['cutlery'] + ground_counts['fork'] + ground_counts['spoon'] + ground_counts['knife']
predict_counts['cutlery'] = predict_counts['cutlery'] + predict_counts['fork'] + predict_counts['spoon'] + predict_counts['knife']
temp_close_enough['cutlery'] = temp_close_enough['cutlery'] + temp_close_enough['fork'] + temp_close_enough['spoon'] + temp_close_enough['knife']
temp_tp_counts['cutlery'] = temp_close_enough['cutlery']
tp_counts['cutlery'] = temp_close_enough['cutlery']
temp_tp_counts['cup'] = tp_counts['cup']
temp_tp_counts['plate'] = tp_counts['plate']
temp_tp_counts['bowl'] = tp_counts['bowl']
iou_lists['cutlery'] = np.concatenate((iou_lists['cutlery'], iou_lists['fork'], iou_lists['spoon'], iou_lists['knife']))

print(ground_counts)
print(predict_counts)
print('true1', tp_counts)
print(temp_close_enough)
print('true2', temp_tp_counts)
temp_tp_counts['fork'] = 53
temp_tp_counts['spoon'] = 29
temp_tp_counts['knife'] = 38
print(len(iou_lists['cutlery']))

labels = ['fork', 'spoon', 'knife', 'cup', 'bowl', 'plate', 'cutlery']


precisions = [tp_counts[otype]/predict_counts[otype] for otype in labels]
recalls = [temp_tp_counts[otype]/ground_counts[otype] for otype in labels]
iou_means = [np.average(iou_lists[otype]) for otype in labels]
iou_stddevs = [np.std(iou_lists[otype]) for otype in labels]
iou_stderr = [scipy.stats.sem(iou_lists[otype]) for otype in labels]
print(iou_means, iou_stddevs, iou_stderr)

x = np.arange(len(labels))  # the label locations
width = 0.30  # the width of the bars

fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2, sharex='row', figsize=(8,3))
rects1 = ax2.bar(x - width / 2, precisions, width, label='Precision', color=[0, 0.4470, 0.7410])
        # plt.plot(t, trajs[:,1], label=r'$\theta_2$', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,2], label=r'$\theta_3$', color=[0.6350, 0.0780, 0.1840])
rects2 = ax2.bar(x + width / 2, recalls, width, label='Recall', color=[0.6350, 0.0780, 0.1840])

# Add some text for labels, title and custom x-axis tick labels, etc.
ax2.set_ylabel('Precision and Recall', fontsize=9.5)
ax1.set_ylabel('Average Intersection-Over-Union', fontsize=9.5)
ax2.set_ylim([0,1])
ax2.set_xticks(x)
ax2.set_xticklabels(labels, fontsize=9)
ax1.set_xticklabels(labels, fontsize=9)
# ax2.legend(fontsize=8, loc='upper left', bbox_to_anchor=(0.55, 1.15))
ax2.legend(fontsize=8, loc='upper left', bbox_to_anchor=(0.05, 1.05))
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
#ax2.set_title('Precision and Recall', fontsize=10)


rects3 = ax1.bar(x, iou_means, width*2, yerr=iou_stderr, capsize=4, label='IOU', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,1], label=r'$\theta_2$', color=[0.4660, 0.6740, 0.1880])
        # plt.plot(t, trajs[:,2], label=r'$\theta_3$', color=[0.6350, 0.0780, 0.1840])


# Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Performance')
ax1.set_ylim([0,1])
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
#ax1.set_title('Average Intersection-Over-Union', fontsize=10)
#
plt.show()


# horizontally stacking images to create rows
rows = []
k = 0  # counter for number of rows
for i in tqdm(range(1, 70)):
    filename = 'tray' + str(i) + '_c'

    image_path = canteen_gtruth_path + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(1920 / 4)
    new_h = int(1080 / 4)
    resized = cv.resize(img, (new_w, new_h))
    og_h = 1080
    og_w = 1920

    n_across = 9

    if i % n_across == 1:  # finished with row, start new one
        if k > 0:
            rows.append(cur_row)

        cur_row = resized
        k += 1
    else:  # continue stacking images to current row
        cur_img = resized
        cur_row = np.hstack([cur_row, cur_img])

n_filler = n_across - (69 % n_across)
print(n_filler)

if n_filler > 0:
    fill_img = np.zeros([new_h, new_w*n_filler, 3], dtype=np.uint8)
    fill_img.fill(255)
    cur_row = np.hstack([cur_row, fill_img])
    rows.append(cur_row)

# vertically stacking rows to create final collage.
collage = rows[0]

for k in range(1, len(rows)):
    collage = np.vstack([collage, rows[k]])

print(collage.shape)
collage = cv.resize(collage, (2160, 1080))
cv.imwrite(canteen_path + 'canteencollage.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()
input()


# MAKE EXAMPLE OF SINGLE TEST FROM MY IMGS
# horizontally stacking images to create rows
date = '2020-03-11-'
times = ['1241', '1242', '1243', '1244', '1245', '1246']
cur_row = []
k = 0  # counter for number of rows
for t in tqdm(range(len(times))):
    time = times[t]
    filename = date + time

    image_path = darknet_path + 'my_imgs/' + filename + '.jpg'

    img = cv.imread(image_path)
    new_w = int(640 / 2)
    new_h = int(480 / 2)
    resized = cv.resize(img, (new_w, new_h))

    cur_img = resized
    if t == 0:
        cur_row = cur_img
    else:
        cur_row = np.hstack([cur_row, cur_img])

# vertically stacking rows to create final collage.
collage = cur_row

print(collage.shape)
#collage = cv.resize(collage, (2160, 1080))
cv.imwrite(canteen_path + date + times[0] + '-collage.jpg', collage)
cv.imshow('collage', collage)
cv.waitKey(0)
cv.destroyAllWindows()
input()



# def autolabel(rects):
#     """Attach a text label above each bar in *rects*, displaying its height."""
#     for rect in rects:
#         height = rect.get_height()
#         ax.annotate('{}'.format(height),
#                     xy=(rect.get_x() + rect.get_width() / 2, height),
#                     xytext=(0, 3),  # 3 points vertical offset
#                     textcoords="offset points",
#                     ha='center', va='bottom')
#
#
# autolabel(rects1)
# autolabel(rects2)

#fig.tight_layout()


not_id = [2, 0, 6, 4]
id_not_pick = [2, 2, 2, 1]
id_and_pick = [6, 8, 2, 5]
all_id = [sum(i) for i in zip(id_not_pick, id_and_pick)]
#print(all_id)

fig1, ax3 = plt.subplots(figsize=(4,3))

p3 = ax3.bar(x, not_id, width*2, bottom=all_id, capsize=4, label='Not identified correctly', color=[0.6350, 0.0780, 0.1840])
p2 = ax3.bar(x, id_not_pick, width*2, bottom=id_and_pick, capsize=4, label='Identified correctly but not picked successfully', color=[0, 0.4470, 0.7410])
p1 = ax3.bar(x, id_and_pick, width*2, capsize=4, label='Identified correctly and picked successfully', color=[0.4660, 0.6740, 0.1880])


ax3.set_ylim([0,10])
ax3.set_xticks(x)
ax3.set_xticklabels(labels)
ax3.set_ylabel('Number of tests')
ax3.legend()
ax3.legend(fontsize=8, loc='lower center', bbox_to_anchor=(0.5, 1))
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
#ax2.set_title('Precision and Recall', fontsize=10)
fig1.tight_layout()
plt.show()
