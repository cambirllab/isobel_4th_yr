import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
from tqdm import tqdm

import matplotlib.pyplot as plt
def get_canteen_plates(image_path, show_img=False):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # # for small plastic
    # p1 = 80
    # p2 = 50
    # min_r = 120
    # max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    # for big canteen plates
    p1 = 80
    p2 = 60
    min_r = 355
    max_r = 370

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    # for small canteen plates
    p1 = 80
    p2 = 50
    min_r = 230
    max_r = 240

    circles_s = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    print(circles_b)
    print(circles_s)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if circles_s is not None:
        circles = np.uint16(np.around(circles_s))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0,
                                  "box": {"x": i[0] / wid, "y": i[1] / hei, "w": 2 * i[2] / wid,
                                          "h": 2 * i[2] / hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    print(plate_objects)
    return plate_objects


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def drawline(img,pt1,pt2,color,thickness=1,style='dotted',gap=15):
    dist =((pt1[0]-pt2[0])**2+(pt1[1]-pt2[1])**2)**.5
    pts= []
    for i in  np.arange(0,dist,gap):
        r=i/dist
        x=int((pt1[0]*(1-r)+pt2[0]*r)+.5)
        y=int((pt1[1]*(1-r)+pt2[1]*r)+.5)
        p = (x,y)
        pts.append(p)

    if style=='dotted':
        for p in pts:
            cv.circle(img,p,thickness,color,-1)
    else:
        s=pts[0]
        e=pts[0]
        i=0
        for p in pts:
            s=e
            e=p
            if i%2==1:
                cv.line(img,s,e,color,thickness)
            i+=1


def drawpoly(img,pts,color,thickness=1,style='dotted',):
    s=pts[0]
    e=pts[0]
    pts.append(pts.pop(0))
    for p in pts:
        s=e
        e=p
        drawline(img,s,e,color,thickness,style)


def drawrect(img,pt1,pt2,color,thickness=1,style='dotted'):
    pts = [pt1,(pt2[0],pt1[1]),pt2,(pt1[0],pt2[1])]
    drawpoly(img,pts,color,thickness,style)

    # cv.imshow('im',im)
    # cv.waitKey()


def draw_cv_rect(image, obj_dict, colour, draw_l_diagonal=False, draw_r_diagonal=False, order=0, gtruth=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    top_right_x = int((x + (w / 2)) * width)
    top_right_y = int((y - (h / 2)) * height)
    bot_left_x = int((x - (w / 2)) * width)
    bot_left_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    pt3 = (top_right_x, top_right_y)
    pt4 = (bot_left_x, bot_left_y)

    line_thickness = 10
    if gtruth:
        drawrect(image, pt1, pt2, colour, line_thickness, 'dotted')
    else:
        cv.rectangle(image, pt1, pt2, colour, line_thickness)
        if obj_dict['type'] == 'fork':
            if obj_dict['prob'] <80:
                location = (bot_left_x + 40, bot_left_y + 80)
            # elif obj_dict['prob'] <97:
            #     location = (top_left_x - 125, top_left_y - 15)
            else:
                # location = (top_left_x - 100, top_left_y - 15)
                location = (bot_left_x - 40, bot_left_y + 80)
        else:
            location = (top_left_x +20, top_left_y - 20)
        cv.putText(image, text=obj_dict['type'] + ' ' + str(int(obj_dict['prob'])) + '%',
                   org=location, fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=2.5,
                   thickness=8)

    if draw_l_diagonal:
        cv.line(image, pt1, pt2, colour, line_thickness)
    if draw_r_diagonal:
        cv.line(image, pt3, pt4, colour, line_thickness)

    if order != 0:
        order_label = str(order)
        y_off = 120
        x_off = 30
        th = 10
        if draw_r_diagonal:
            cv.putText(image, text=order_label, org=(top_left_x+x_off, top_left_y+y_off), fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)
        elif draw_l_diagonal:
            cv.putText(image, text=order_label, org=(top_right_x - 4 * x_off, top_right_y + y_off),
                       fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)
        else:
            cv.putText(image, text=order_label, org=(top_left_x+x_off, top_left_y+y_off),
                       fontFace=cv.FONT_HERSHEY_SIMPLEX, color=colour, fontScale=4,
                       thickness=th)


    return image


darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"


# for i in range(1,70):
#     tray_name = 'tray' + str(i)
#
#     img1_path = tray_name + '_c.jpg'
#     img2_path = tray_name + '_c_tilt5.jpg'
#     img1 = cv.imread(img1_path)
#     img2 = cv.imread(img2_path)
#     txt1_path = tray_name + '_c.txt'
#     txt2_path = tray_name + '_c_tilt5.txt'
#
#     obj1 = yf.get_objs_from_txt(txt1_path) + get_canteen_plates(tray_name + '_c.jpg')
#
#     types = ['fork', 'spoon', 'knife', 'cup', 'plate', 'bowl']
#
#     objects1 = [obj for obj in obj1 if obj['type'] in types]
#     print(objects1)
#
#     obj2 = yf.get_objs_from_txt(txt2_path) + get_canteen_plates(tray_name + '_c_tilt5.jpg')
#     objects2 = [obj for obj in obj2 if obj['type'] in types]
#     print(objects2)
#
#     colours = [(0, 255, 0), (0, 0, 255), (255, 0, 0)]
#     colour_codes = {'fork': (0, 255, 0), 'spoon': (0, 255, 255), 'knife': (255, 255, 0), 'cup': (255, 0, 255), 'plate': (0, 0, 255), 'bowl': (255, 0, 0)}
#
#     img_new1 = img1
#     for o in range(len(objects1)):
#         obj = objects1[o]
#         colour = colour_codes[obj['type']]
#         img_new1 = draw_cv_rect(img_new1, obj, colour)
#
#     scale = 1 / 3
#     new_size = (int(1920 * scale), int(1080 * scale))
#
#     resized1 = cv.resize(img_new1, new_size)
#     # cv.imshow('new image ' + str(i), resized1)
#
#     img_new2 = img2
#     for o in range(len(objects2)):
#         obj = objects2[o]
#         colour = colour_codes[obj['type']]
#         img_new2 = draw_cv_rect(img_new2, obj, colour)
#
#     resized2 = cv.resize(img_new2, new_size)
#     # cv.imshow('new image tilted ' + str(i), resized2)
#
#     joint = np.hstack([resized1, resized2])
#     cv.imshow('combined - tray' + str(i), joint)
#     cv.waitKey(0)
#     cv.destroyAllWindows()
#
# input('stop right here')

os.chdir(darknet_path + canteen_folder)

tray_egs = [47]
angles47 = ['r', 'n', 'l', 'd']
order47 = [1, 0, 2, 3]
# tray_egs = [47, 26, 16, 25, 7, 13, 21]

for i in range(len(tray_egs)):
    tray_name = 'tray' + str(tray_egs[i])
    true_objs = get_objs_from_true_txt(gtruth_folder + tray_name + '_c.txt')
    print(true_objs)

    trayp_name = yolo_folder + tray_name

    img1_path = trayp_name + '_c.jpg'
    img2_path = trayp_name + '_c_tilt5.jpg'
    img1 = cv.imread(img1_path)
    img2 = cv.imread(img2_path)
    txt1_path = trayp_name + '_c.txt'
    txt2_path = trayp_name + '_c_tilt5.txt'

    obj1 = yf.get_objs_from_txt(txt1_path) + get_canteen_plates(trayp_name + '_c.jpg')

    types = ['fork', 'spoon', 'knife', 'cup', 'plate', 'bowl']

    objects1 = [obj for obj in obj1 if obj['type'] in types]
    print(objects1)

    obj2 = yf.get_objs_from_txt(txt2_path) + get_canteen_plates(trayp_name + '_c_tilt5.jpg')
    objects2 = [obj for obj in obj2 if obj['type'] in types]
    print(objects2)

    colours = [(0, 255, 0), (0, 0, 255), (255, 0, 0)]
    colour_codes = {'fork': (0, 255, 0), 'spoon': (0, 255, 255), 'knife': (255, 255, 0), 'cup': (255, 0, 255), 'plate': (0, 0, 255), 'bowl': (255, 0, 0)}

    img_new1 = img1
    img_new3 = cv.imread(img1_path)
    img_new4 = cv.imread(img1_path)
    img_new0 = cv.imread(img1_path)

    scale = 1 / 3
    new_size = (int(1920 * scale), int(1080 * scale))

    scaledimg1 = cv.resize(img1, new_size)
    cv.imwrite('figure-' + tray_name + '-0deg-clean.jpg', scaledimg1[:, 100:-100])
    for o in range(len(objects1)):
        obj = objects1[o]
        colour = colour_codes[obj['type']]
        img_new1 = draw_cv_rect(img_new1, obj, colour)

    for o2 in range(len(true_objs)):
        obj = true_objs[o2]
        colour = colour_codes[obj['type']]
        # img_new1 = draw_cv_rect(img_new1, obj, colour)
        img_new0 = draw_cv_rect(img_new0, obj, colour, gtruth=True)
    resized0 = cv.resize(img_new0, new_size)
    cv.imshow('new image ' + str(i), resized0)
    cv.waitKey()
    cv.destroyAllWindows()

    resized1 = cv.resize(img_new1, new_size)
    # cv.imshow('new image ' + str(i), resized1)
    cv.imwrite('figure-' + tray_name + '-0deg-yoloed.jpg', resized1[:, 100:-100])

    img_new2 = img2

    scaledimg2 = cv.resize(img2, new_size)
    cv.imwrite('figure-' + tray_name + '-5deg-clean.jpg', scaledimg2[:, 100:-100])

    for o in range(len(objects2)):
        obj = objects2[o]
        colour = colour_codes[obj['type']]
        img_new2 = draw_cv_rect(img_new2, obj, colour)

    resized2 = cv.resize(img_new2, new_size)
    # cv.imshow('new image tilted ' + str(i), resized2)
    cv.imwrite('figure-' + tray_name + '-5deg-yoloed.jpg', resized2[:, 100:-100])

    for o in range(len(objects1)):
        obj = objects1[o]
        colour = colour_codes[obj['type']]
        print(obj)
        ipt = angles47[o]
        ipt2 = order47[o]
        # ipt = 'e'
        # ipt = input('left or right?')
        if ipt == 'l':
            img_new3 = draw_cv_rect(img_new3, obj, colour, draw_l_diagonal=True)
            img_new4 = draw_cv_rect(img_new4, obj, colour, draw_l_diagonal=True, order=ipt2)
        elif ipt == 'r':
            img_new3 = draw_cv_rect(img_new3, obj, colour, draw_r_diagonal=True)
            img_new4 = draw_cv_rect(img_new4, obj, colour, draw_r_diagonal=True, order=ipt2)
        elif ipt == 'd':
            img_new3 = draw_cv_rect(img_new3, obj, colour)
            img_new4 = draw_cv_rect(img_new4, obj, colour, order=ipt2)
        elif ipt == 'n':
            print('not drawing')

    resized4 = cv.resize(img_new4, new_size)
    resized3 = cv.resize(img_new3, new_size)
    cv.imshow('diagonals', resized4)

    cv.imwrite('figure-' + tray_name + '-diagonaled.jpg', resized3[:, 100:-100])
    cv.imwrite('figure-' + tray_name + '-planned.jpg', resized4[:, 100:-100])

    joint = np.hstack([resized1, resized2, resized3])
    cv.imshow('combined - tray' + str(tray_egs[i]), joint)
    cv.waitKey(0)
    cv.destroyAllWindows()

input('stop right here')

tray_name = 'tray25'

img1_path = tray_name + '_c.jpg'
img2_path = tray_name + '_c_tilt5.jpg'
img1 = cv.imread(img1_path)
img2 = cv.imread(img2_path)
txt1_path = tray_name + '_c.txt'
txt2_path = tray_name + '_c_tilt5.txt'

obj1 = yf.get_objs_from_txt(txt1_path) + get_canteen_plates(tray_name + '_c.jpg')

types = ['fork', 'spoon', 'knife', 'cup', 'plate', 'bowl']

objects1 = [obj for obj in obj1 if obj['type'] in types]
print(objects1)

obj2 = yf.get_objs_from_txt(txt2_path) + get_canteen_plates(tray_name + '_c_tilt5.jpg')
objects2 = [obj for obj in obj2 if obj['type'] in types]
print(objects2)

colours = [(0, 255, 0), (0, 0, 255), (255, 0, 0)]
colour_codes = {'fork': (0, 255, 0), 'spoon': (0, 255, 255), 'knife': (255, 255, 0), 'cup': (255, 0, 255), 'plate': (0, 0, 255), 'bowl': (255, 0, 0)}

img_new1 = img1
for o in range(len(objects1)):
    obj = objects1[o]
    colour = colour_codes[obj['type']]
    img_new1 = draw_cv_rect(img_new1, obj, colour)

resized1 = cv.resize(img_new1, (960, 540))
cv.imshow('new image ', resized1)
cv.imwrite('labelled25straight.jpg', resized1)

img_new2 = img2
for o in range(len(objects2)):
    obj = objects2[o]
    colour = colour_codes[obj['type']]
    img_new2 = draw_cv_rect(img_new2, obj, colour)

resized2 = cv.resize(img_new2, (960, 540))
cv.imshow('new image tilted ', resized2)
cv.imwrite('labelled25tilt.jpg', resized2)
cv.waitKey(0)
cv.destroyAllWindows()