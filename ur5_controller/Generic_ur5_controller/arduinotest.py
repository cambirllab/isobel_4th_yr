import serial
import time

port = 30010
ee_port = "COM7"

if ee_port!=False:
    ee = serial.Serial(ee_port, 9600)  # open serial port

    def serial_send(cmd, var, wait):
        ipt = ""
        ee.reset_input_buffer()
        ee.write(str.encode(cmd + chr(var + 48) + "\n"))
        # wait for cmd acknowledgement
        while True:
            ipt = bytes.decode(ee.readline())
            # print("gripper data: ", ipt)
            if ipt == "received\r\n":
                break
        # wait for cmd completion
        if wait == True:
            while True:
                ipt = bytes.decode(ee.readline())
                # print("gripper data: ", ipt)
                if ipt == "done\r\n":
                    # print("Completed gripper CMD")
                    break
        return ipt

    while ee.isOpen()==False:
        print("Waiting for hand")
    print("Serial port opened :)")

    ee.send_break()
    time.sleep(1)  # This is needed to allow MBED to send back command in time!
    ipt = bytes.decode(ee.readline())
    print("Connected to",ipt)

    if ipt=="Rotary Gripper\r\n":
        print("Pump recognised")
    elif ipt=="Basic Gripper\r\n":
        print("Gripper recognised")
    else:
        print("NO GRIPPER DETECTED")

    print('Moving on...')
    while 1:
        ipt = input('command for gripper?: ')
        if ipt == 'o':
            serial_send("G", 0, True)
        elif ipt == 'c':
            serial_send("G", 1, True)
        else:
            break