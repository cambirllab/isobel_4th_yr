import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats
import subprocess
import yolofuncs as yf
import matplotlib.pyplot as plt

darknet_path = "./darknet/build/darknet/x64/"

canteen_folder = "canteen_imgs/"
yolo_folder = "yolo/"
gtruth_folder = "gtruth/"

canteen_path = darknet_path + canteen_folder
canteen_yolo_path = canteen_path + yolo_folder
canteen_gtruth_path = canteen_path + gtruth_folder

img_list = []


def get_objs_from_true_txt(file_path):
    # file_path = file_path + '.txt'
    # return list of object dictionaries ordered in highest probability first

    classes = ["fork", "spoon", "knife", "plate", "cup", "bowl", "cutlery"]

    # print(file_path)
    objects = []
    with open(file_path) as f:
        for line in f:
            box_list = line.split()
            type = classes[int(box_list[0])]
            x = float(box_list[1])
            y = float(box_list[2])
            w = float(box_list[3])
            h = float(box_list[4])
            object_dict = {"type": type, "prob": 100, "box": {"x": x, "y": y, "w": w, "h": h}}
            # something about tilt later?
            objects.append(object_dict)

    return objects


def get_canteen_plates(image_path, show_img=True):
    plate_objects = []
    img = cv.imread(image_path, 0)
    cimg = cv.imread(image_path)
    wid = img.shape[1]
    hei = img.shape[0]

    # # for large white
    # p1 = 80
    # p2=50
    # min_r = 130
    # max_r = 160

    # # for small plastic
    # p1 = 80
    # p2 = 50
    # min_r = 120
    # max_r = 140

    # # for big plastic
    # p1 = 80
    # p2 = 50
    # min_r = 150
    # max_r = 170

    # for big canteen plates
    p1 = 80
    p2 = 60
    min_r = 355
    max_r = 370

    circles_b = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                              param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    # for small canteen plates
    p1 = 80
    p2 = 50
    min_r = 230
    max_r = 240

    circles_s = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 20,
                               param1=p1, param2=p2, minRadius=min_r, maxRadius=max_r)

    print(circles_b)
    print(circles_s)

    if circles_b is not None:
        circles = np.uint16(np.around(circles_b))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0, "box": {"x": i[0]/wid, "y": i[1]/hei, "w": 2*i[2]/wid, "h": 2*i[2]/hei, "tilt": 'u'}})

    if circles_s is not None:
        circles = np.uint16(np.around(circles_s))
        print(circles)
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 2)
            # draw the center of the circle
            cv.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
            plate_objects.append({"type": "plate", "prob": 100.0,
                                  "box": {"x": i[0] / wid, "y": i[1] / hei, "w": 2 * i[2] / wid,
                                          "h": 2 * i[2] / hei, "tilt": 'u'}})

    if show_img:
        #cv.imshow('image', img)
        resized = cv.resize(cimg, (960, 540))
        cv.imshow('detected circles', resized)
        cv.waitKey(0)
        cv.destroyAllWindows()

    print(plate_objects)
    return plate_objects


def get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2):
    # return intersection, union, iou
    signx = np.sign(x2 - x1)
    signy = np.sign(y2 - y1)

    if signx == 0:
        signx = 1
    if signy == 0:
        signy = 1

    b = (x1 + signx * (w1 / 2) - x2 + signx * (w2 / 2)) * signx

    l = (y1 + signy * (h1 / 2) - y2 + signy * (h2 / 2)) * signy

    # for case of incasement, take smallest value
    b = np.min([b, w1, w2])
    l = np.min([l, h1, h2])
    # print('b, l:', b, l)

    if b < 0 or l < 0:
        # print('No intersection')
        union = w1 * h1 + w2 * h2
        intersection = 0
        iou = intersection / union
        return intersection, union, iou
    else:
        intersection = b * l
        union = w1 * h1 + w2 * h2 - b * l
        iou = intersection / union
        return intersection, union, iou


def draw_cv_rect(image, obj_dict, isGround=False):
    height = image.shape[0]
    width = image.shape[1]

    x = obj_dict['box']['x']
    y = obj_dict['box']['y']
    w = obj_dict['box']['w']
    h = obj_dict['box']['h']

    top_left_x = int((x - (w / 2)) * width)
    top_left_y = int((y - (h / 2)) * height)
    bot_right_x = int((x + (w / 2)) * width)
    bot_right_y = int((y + (h / 2)) * height)

    pt1 = (top_left_x, top_left_y)
    pt2 = (bot_right_x, bot_right_y)

    if isGround:
        # green for ground truth
        color = (0, 255, 0)
    else:
        # red for predicted
        color = (0, 0, 255)

    cv.rectangle(image, pt1, pt2, color, 2)

    return image


# os.chdir(canteen_path)
# close_enough = True
# cutlery_list = ['fork', 'spoon', 'knife', 'cutlery']
#
# ground_counts = {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
# duplicate_counts = {'fork': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'spoon': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'knife': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cup': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'plate': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'bowl': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cutlery': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
#                  }
#
# single_counts = {'fork': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'spoon': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'knife': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cup': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'plate': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'bowl': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cutlery': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
#                  }
#
# confi_counts = {'fork': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'spoon': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'knife': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cup': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'plate': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'bowl': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0},
#                  'cutlery': {'fork': 0, 'spoon': 0, 'knife': 0, 'cup': 0, 'plate': 0, 'bowl': 0, 'cutlery': 0}
#                  }
#
#
# random_predictions = []
#
# for i in range(1,70):
#     filename = 'tray' + str(i) + '_c'
#
#     print(filename)
#     try:
#         tray_true_ious = []
#         tray_ce_ious = []
#
#         objects = np.load('yolo/' + filename + '_objs.npy', allow_pickle=True)
#         plates = get_canteen_plates('yolo/' + filename + '.jpg', show_img=False)
#         objects = np.concatenate((objects, plates))
#         # print(objects)
#         # RUN GET PLATES
#
#         true_objects = get_objs_from_true_txt('gtruth/' + filename + '.txt')
#         # print(true_objects)
#
#         for true_obj in true_objects:
#             object_true_ious = [0]
#             object_ce_ious = [0]
#             obj_type = true_obj['type']
#             # print(obj_type)
#             ground_counts[true_obj['type']] += 1
#             iou_predictions = []
#             object_predictions = []
#             confi_predictions = []
#             for obj in objects:
#                 x1 = obj['box']['x']
#                 y1 = obj['box']['y']
#                 w1 = obj['box']['w']
#                 h1 = obj['box']['h']
#
#                 x2 = true_obj['box']['x']
#                 y2 = true_obj['box']['y']
#                 w2 = true_obj['box']['w']
#                 h2 = true_obj['box']['h']
#
#                 # print(x1, y1, w1, h1, x2, y2, w2, h2)
#
#                 inter, union, iou = get_int_uni_iou(x1, y1, w1, h1, x2, y2, w2, h2)
#                 # print([inter, union, iou])
#                 iou_predictions.append(iou)
#                 object_predictions.append(obj['type'])
#                 confi_predictions.append(obj['prob'])
#
#                 iou_thresh = 0.50
#                 if iou >= iou_thresh:
#                     try:
#                         duplicate_counts[true_obj['type']][obj['type']] += 1
#                         # if true_obj['type'] == 'spoon' and obj['type'] == 'bowl':
#                         #     img = cv.imread(yolo_folder + filename + '.jpg')
#                         #     cv.imshow('image', img)
#                         #     cv.waitKey(0)
#                         #     cv.destroyAllWindows()
#
#                     except KeyError:
#                         print(obj['type'], 'not in object set')
#                         random_predictions.append(obj['type'])
#
#             # iou_predictions = np.array(iou_predictions)
#             sorted_iou_predictions = [x for x, _ in sorted(zip(iou_predictions, object_predictions), reverse=True)]
#             sorted_object_predictions = [x for _, x in sorted(zip(iou_predictions, object_predictions), reverse=True)]
#             csorted_iou_predictions = [x for _, x in sorted(zip(confi_predictions, iou_predictions), reverse=True)]
#             csorted_object_predictions = [x for _, x in sorted(zip(confi_predictions, object_predictions), reverse=True)]
#             csorted_confi_predictions = [x for x, _ in sorted(zip(confi_predictions, object_predictions), reverse=True)]
#
#             # have something that finds any correct predictions
#             iou_thresh = 0.50
#             try:
#                 argmax = sorted_object_predictions.index(true_obj['type'])
#                 max_iou = sorted_iou_predictions[argmax]
#                 if max_iou >= iou_thresh:
#                     max_obj_type = sorted_object_predictions[argmax]
#                 else:
#                     raise ValueError
#             # if none then just use maximum
#             except ValueError:
#                 iou_predictions = np.array(iou_predictions)
#                 argmax = np.argmax(iou_predictions)
#                 max_iou = iou_predictions[argmax]
#                 max_obj_type = object_predictions[int(argmax)]
#
#             c = 0
#             while c in range(len(csorted_confi_predictions)):
#                 if csorted_iou_predictions[c] >= iou_thresh:
#                     max_iou = csorted_iou_predictions[c]
#                     max_obj_type = csorted_object_predictions[c]
#                     break
#                 c += 1
#                 if c == len(csorted_confi_predictions):
#                     try:
#                         argmax = sorted_object_predictions.index(true_obj['type'])
#                         max_iou = sorted_iou_predictions[argmax]
#                         if max_iou >= iou_thresh:
#                             max_obj_type = sorted_object_predictions[argmax]
#                         else:
#                             raise ValueError
#                     # if none then just use maximum
#                     except ValueError:
#                         iou_predictions = np.array(iou_predictions)
#                         argmax = np.argmax(iou_predictions)
#                         max_iou = iou_predictions[argmax]
#                         max_obj_type = object_predictions[int(argmax)]
#                     break
#
#             if max_iou >= iou_thresh:
#                 try:
#                     single_counts[true_obj['type']][max_obj_type] += 1
#                     print('matched', true_obj['type'], 'with', max_obj_type, 'IOU=', max_iou)
#                     # if true_obj['type'] == 'spoon' and obj['type'] == 'bowl':
#                     #     img = cv.imread(yolo_folder + filename + '.jpg')
#                     #     cv.imshow('image', img)
#                     #     cv.waitKey(0)
#                     #     cv.destroyAllWindows()
#
#                 except KeyError:
#                     print(max_obj_type, 'not in object set')
#                     random_predictions.append(max_obj_type)
#
#     except IOError:
#         print('File not accessible')
#
#
# print(ground_counts)
# print(duplicate_counts)
# print(single_counts)
# print(random_predictions)
#
# np.save('conf_ground_counts.npy', ground_counts)
# np.save('conf_confidence_counts.npy', single_counts)
#
# input()

os.chdir(canteen_path)
ground_counts = np.load('conf_ground_counts.npy', allow_pickle=True).item()
single_counts = np.load('conf_single_counts.npy', allow_pickle=True).item()
matched_counts = np.load('conf_match_counts.npy', allow_pickle=True).item()


classes = ["fork", "spoon", "knife", "cup", "bowl", "plate", "cutlery"]

confusion_totals = np.array([ground_counts[x] for x in classes])
confusion_matrix = np.array([[single_counts[y][x] for x in classes] for y in classes])
confusion_matrix_1 = np.array([[matched_counts[y][x] for x in classes] for y in classes])
print(confusion_matrix)
print(confusion_matrix_1)


import numpy as np


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()


def my_plot_confusion_matrix(cm,
                          target_names,
                          overall_totals,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):

    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    int_cm = cm

    cm = np.array([[float(cm[i, j] / overall_totals[i]) for j in range(len(cm[i]))] for i in range(len(overall_totals))])

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    # plt.title(title)
    cbr = plt.colorbar()
    cbr.set_label('Fraction of class occurrences')
    plt.clim(0, 1)

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            old_string = "{:0.4f}".format(cm[i, j])
            new_string = "{:,}".format(cm[i, j])
            plt.text(j, i, new_string,
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            old_string = "{:,}".format(int_cm[i, j])
            new_string = str(cm[i, j]) + '/' + str(overall_totals[i])
            print(new_string)
            plt.text(j, i, old_string,
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    # plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.xlabel('Predicted label with best IOU')
    plt.tight_layout()
    plt.show()


def my_plot_confusion_matrix_1(cm,
                          target_names,
                          predict_names,
                          overall_totals,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):

    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    for k in range(cm.shape[0]):
        cm[k, -1] = overall_totals[k] - np.sum(cm[k])
    int_cm = cm

    print(int_cm)

    cm = np.array([[float(cm[i, j] / overall_totals[i]) for j in range(len(cm[i]))] for i in range(len(overall_totals))])

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    cbr = plt.colorbar()
    cbr.set_label('Fraction of class occurrences')
    plt.clim(0, 1)

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, predict_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            old_string = "{:0.4f}".format(cm[i, j])
            new_string = "{:,}".format(cm[i, j])
            plt.text(j, i, new_string,
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            old_string = "{:,}".format(int_cm[i, j])
            new_string = str(cm[i, j]) + '/' + str(overall_totals[i])
            print(new_string)
            plt.text(j, i, old_string,
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.show()


predicted = classes[:-1]
predicted.append('none')
print(predicted)

# my_plot_confusion_matrix_1(cm=confusion_matrix,
#                          overall_totals=confusion_totals,
#                          normalize=False,
#                          target_names=classes,
#                          predict_names=predicted,
#                          title="Confusion Matrix")

my_plot_confusion_matrix(cm=confusion_matrix_1[:-1, :-1],
                         overall_totals=confusion_totals[:-1],
                         normalize=False,
                         target_names=classes[:-1],
                         title="Confusion Matrix")
my_plot_confusion_matrix(cm=confusion_matrix[:-1, :-1],
                         overall_totals=confusion_totals[:-1],
                         normalize=False,
                         target_names=classes[:-1],
                         title="Confusion Matrix")

plot_confusion_matrix(cm=confusion_matrix,
                      normalize=False,
                      target_names=classes,
                      title="Confusion Matrix")



