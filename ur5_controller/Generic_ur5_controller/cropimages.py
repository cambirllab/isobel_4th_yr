import numpy as np
import json
import glob, os
import cv2 as cv
import scipy.stats

# iterate over images in folder
# get the yolo results
# get the ground truth results in same form
# handle file not existing or being empty
# for each object compare with others of same type from other file
# record intersection, union, iou, if no union

# for image in /imgwYOLOlabels:
#   get filename (without jpg)
#   get yolo objects

# os.chdir("./mytrain")
# for image in glob.glob("*.jpg"):
#     # image gives me name
#     filename = "C:\\Users\\isobe\\Desktop\\darknet\\build\\darknet\\x64\\data\\coco\\images\\mytrain\\" + image
#
#     print(filename)  # /imgwYOLOlabels/filename.txt
#
# input()

i = 1

os.chdir("./gripimages")
for image in glob.glob("*.jpg"):
    img = cv.imread(image)

    cur_h = img.shape[0]
    cur_w = img.shape[1]

    cv.namedWindow('image', cv.WINDOW_NORMAL)
    cv.resizeWindow('image', cur_w, cur_h)
    cv.imshow('image', img)


    print(cur_h, cur_w)

    y1 = 500
    y2 = 2500
    x1 = 1000
    x2 = 3000

    crop_w = x2 - x1
    crop_h = y2 - y1

    crop_img = img[y1:y2, x1:x2]
    cv.namedWindow('cropped', cv.WINDOW_NORMAL)
    cv.resizeWindow('cropped', crop_w, crop_h)
    cv.imshow('cropped', crop_img)
    cv.waitKey(0)

    filename = 'grip' + str(i) + '.jpg'
    print(filename)

    cv.imwrite(filename, crop_img)

    i += 1